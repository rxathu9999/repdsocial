//
//  CustomTextField.swift
//  DuringBoss
//
//  Created by buisinam on 5/26/15.
//  Copyright (c) 2015 buisinam. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        var width = CGFloat(1.0)
        var border = CALayer()
        border.borderColor = UIColor.whiteColor().CGColor
//        border.frame = CGRect(x: 0, y:
//            self.frame.size.height - width, width:
//            self.frame.size.width, height: self.frame.size.height)
//        
        border.borderWidth = width
//        self.layer.addSublayer(border)
        
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
    }
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 5);
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }

    private func newBounds(bounds: CGRect) -> CGRect {
        
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= (padding.top * 2) - padding.bottom
        newBounds.size.width -= (padding.left * 2) - padding.right
        return newBounds
    }
    
    

}
