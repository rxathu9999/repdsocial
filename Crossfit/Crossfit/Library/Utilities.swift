//
//  Utilities.swift
//  Carseats
//
//  Created by Trung Luan on 3/17/15.
//  Copyright (c) 2015 Trung Luan. All rights reserved.
//
import UIKit

class Utilities {
    
    // MARK: UITextField
    /* Create right view for UITextField */
    class func rightImageViewForTextField(textField: UITextField, top: CGFloat, right: CGFloat, bottom: CGFloat, imageName: String) -> UIView {
        let textFieldSize = textField.frame.size
        let imageViewSize = textFieldSize.height - top - bottom
        let adjustRightView: UIView = UIView(frame: CGRectMake(textFieldSize.width - imageViewSize - right,
            top,
            imageViewSize + right,
            imageViewSize))
        let rightImageView = UIImageView(image: UIImage(named: imageName))
        rightImageView.frame = CGRectMake(0, 0, imageViewSize, imageViewSize)
        rightImageView.contentMode = UIViewContentMode.ScaleAspectFit
        adjustRightView.addSubview(rightImageView)
        
        return adjustRightView;
    }
    
    /* Create left view for UITextField */
    class func leftImageViewForTextField(textField: UITextField, left: CGFloat, width: CGFloat, imageName: String) -> UIView {
        let adjustView: UIView = UIView(frame: CGRectMake(left, 0, width, textField.frame.size.height))
        let imageView = UIImageView(image: UIImage(named: imageName))
        imageView.frame = CGRectMake(0, 0, adjustView.frame.size.width, adjustView.frame.size.height)
        imageView.contentMode = UIViewContentMode.Left
        adjustView.addSubview(imageView)
        
        return adjustView;
    }
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(advance(cString.startIndex, 1))
        }
        
        if (count(cString) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    
    
    // MARK: Custom Popup View Controller
    
    class func showPopUp(viewController: UIViewController, completionHandler: () -> ()) {
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if ((UIDevice.currentDevice().systemVersion as NSString).floatValue  >= 8.0)
        {
            viewController.providesPresentationContextTransitionStyle = true
            viewController.definesPresentationContext = true
            viewController.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        } else {
            delegate.window?.rootViewController?.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        }
        
        delegate.window?.rootViewController?.presentViewController(viewController, animated: false
            , completion: { () -> Void in
                completionHandler()
        })
    }
    
    class func showPopUp(viewController: UIViewController, rootViewController: UIViewController) {
        
        if ((UIDevice.currentDevice().systemVersion as NSString).floatValue  >= 8.0)
        {
            viewController.providesPresentationContextTransitionStyle = true
            viewController.definesPresentationContext = true
            viewController.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        } else {
            rootViewController.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
            rootViewController.navigationController?.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        }
        
        rootViewController.presentViewController(viewController, animated: false
            , completion: { () -> Void in
                //
        })
    }
    
    class func hidePopUp(completionHandler: () -> ()) {
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.window?.rootViewController?.dismissViewControllerAnimated(false, completion: { () -> Void in
            completionHandler()
        })
    }
    
    // MARK: Load View Controller from Story Board

    class func viewController(name: String, onStoryboard storyboardName: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewControllerWithIdentifier(name) as! UIViewController
    }
    
    class func viewTableController(name: String, onStoryboard storyboardName: String) -> UITableViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewControllerWithIdentifier(name) as! UITableViewController
    }
    
    // MARK: Load View from XIB file
    
    class func view(name: String, owner: AnyObject) -> UIView {
        let view = NSBundle.mainBundle().loadNibNamed(name, owner: owner, options: nil).first as! UIView
        return view
    }
    
    // MARK: Fix attributed String on Storyboard
    class func fixAttributedString(label: UILabel) {
        let attributes = label.attributedText.attributesAtIndex(0, effectiveRange: nil)
        label.attributedText = NSAttributedString(string: label.text!, attributes: attributes)
    }
    
    // MARK: Timestamp
    
    /* UTC timestamp from local time */
    class func timestampSince1970FromDate(date: NSDate) -> Double {
        
        let localTimeZoneInSeconds = Double(NSTimeZone.localTimeZone().secondsFromGMT)
        let utcTime = date.dateByAddingTimeInterval(-localTimeZoneInSeconds)
        return utcTime.timeIntervalSince1970
    }
    /* UTC timestamp from local time */
    class func timestampInDayFromDate(date: NSDate) -> Double {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
        let stringDate = dateFormatter.stringFromDate(date)
        let components = stringDate.componentsSeparatedByString(":")
        let timestamp = (components[0] as NSString).doubleValue * 3600 + (components[1] as NSString).doubleValue * 60 + (components[2] as NSString).doubleValue
        return timestamp
    }
    
    /* Local date from UTC timestamp */
    class func localDateFromUTC(timestamp: Int64) -> NSDate {
        
        let utcDate = NSDate(timeIntervalSince1970: Double(timestamp))
        let localTimeZoneInSeconds = Double(NSTimeZone.localTimeZone().secondsFromGMT)
        let localDate = utcDate.dateByAddingTimeInterval(localTimeZoneInSeconds)
        
        return localDate
        
        
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss"
//        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
//        let localDate = NSDate()
//        let stringDate = dateFormatter.stringFromDate(localDate)
//        println("utc: \(stringDate)")
//        
//        let localTimestamp: Double = localDate.timeIntervalSince1970
//        
//        
//        let timeZoneSecond: Double = Double(NSTimeZone.localTimeZone().secondsFromGMT)
//        let utcTimestamp: Double = localTimestamp as Double - timeZoneSecond
//        let uctTime = NSDate(timeIntervalSince1970: utcTimestamp)
//        let dateF = NSDateFormatter()
//        dateF.dateFormat = "dd/MM/yyyy hh:mm:ss"
//        println("utc: \(dateF.stringFromDate(uctTime))")
    }
}

