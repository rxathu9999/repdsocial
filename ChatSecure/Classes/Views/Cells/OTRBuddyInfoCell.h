//
//  OTRBuddyInfoCell.h
//  Off the Record
//
//  Created by David Chiles on 3/4/14.
//  Copyright (c) 2014 Chris Ballinger. All rights reserved.
//

#import "OTRBuddyImageCell.h"

@interface OTRBuddyInfoCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *identifierLabel;
@property (nonatomic, strong) IBOutlet UILabel *accountLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imgCheck;
@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;

- (void)setBuddy:(OTRBuddy *)buddy withAccountName:(NSString *)accountName;
- (void)setBuddy:(OTRBuddy *)buddy;
@end
