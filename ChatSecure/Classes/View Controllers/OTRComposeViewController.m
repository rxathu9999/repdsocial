//
//  OTRComposeViewController.m
//  Off the Record
//
//  Created by David Chiles on 3/4/14.
//  Copyright (c) 2014 Chris Ballinger. All rights reserved.
//

#import "OTRComposeViewController.h"

#import "OTRBuddy.h"
#import "OTRAccount.h"
#import "OTRDatabaseView.h"
#import "OTRLog.h"
#import "OTRDatabaseManager.h"
#import "OTRDatabaseView.h"
#import "OTRAccountsManager.h"
#import "YapDatabaseFullTextSearchTransaction.h"
#import "Strings.h"
#import "OTRBuddyInfoCell.h"
#import "OTRNewBuddyViewController.h"
#import "OTRChooseAccountViewController.h"
#import "OTRProtocolManager.h"

static CGFloat OTRBuddyInfoCellHeight = 80.0;

@interface OTRComposeViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    NSMutableArray *arrIndex;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSLayoutConstraint *  tableViewBottomConstraint;
@property (nonatomic, strong) YapDatabaseConnection *databaseConnection;
@property (nonatomic, strong) YapDatabaseViewMappings *mappings;




@end

@implementation OTRComposeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrIndex = [[NSMutableArray alloc] init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    /////////// Navigation Bar ///////////
    self.title = @"Chat";
//    UIBarButtonItem * cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed:)];
//    self.navigationItem.rightBarButtonItem = cancelBarButtonItem;
    
    if (self.isGroup == YES)
    {
    UIBarButtonItem * doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonNaviPressed:)];
    self.navigationItem.leftBarButtonItem = doneBarButtonItem;
    }
    
    
    NSArray *accounts = [OTRAccountsManager allAccountsAbleToAddBuddies];
    OTRAccount *account = [accounts firstObject];
    
    [[[OTRProtocolManager sharedInstance] protocolForAccount:account] updateAvatar:nil];

    
    /////////// TableView ///////////
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = OTRBuddyInfoCellHeight;
    [self.view addSubview:self.tableView];
    
    [self.tableView registerClass:[OTRBuddyInfoCell class] forCellReuseIdentifier:@"OTRBuddyInfoCell2"];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:0 views:@{@"tableView":self.tableView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[topLayoutGuide][tableView]" options:0 metrics:0 views:@{@"tableView":self.tableView,@"topLayoutGuide":self.topLayoutGuide}]];
    self.tableViewBottomConstraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self.view addConstraint:self.tableViewBottomConstraint];
    
    //////// YapDatabase Connection /////////
    self.databaseConnection = [[OTRDatabaseManager sharedInstance] newConnection];
    self.databaseConnection.name = NSStringFromClass([self class]);
    [self.databaseConnection beginLongLivedReadTransaction];
    
    self.mappings = [[YapDatabaseViewMappings alloc] initWithGroups:@[OTRBuddyGroup] view:OTRAllBuddiesDatabaseViewExtensionName];
    
    [self.databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
        [self.mappings updateWithTransaction:transaction];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(yapDatabaseDidUpdate:)
                                                 name:YapDatabaseModifiedNotification
                                               object:nil];
    
    }

    


- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)doneButtonNaviPressed:(id)sender
{
    
    UIAlertView *alertViewChangeName=[[UIAlertView alloc]initWithTitle:@"" message:@"Input room name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    alertViewChangeName.alertViewStyle=UIAlertViewStylePlainTextInput;
    alertViewChangeName.delegate = self;
    [alertViewChangeName show];
    
   
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==1 )
    {
        NSString *roomName = [[alertView textFieldAtIndex:0] text];
        NSMutableArray *arrBuddy = [[NSMutableArray alloc] init];
        for (int i=0;i<arrIndex.count;i++)
        {
            NSIndexPath *viewIndexPath = [NSIndexPath indexPathForItem:[[arrIndex objectAtIndex:i] intValue] inSection:0];
            
            OTRBuddy * buddy = [self buddyAtIndexPath:viewIndexPath];
            [arrBuddy addObject:buddy];
            
        }
        
        //    [[[OTRProtocolManager sharedInstance] protocolForAccount:self.account] removeBuddies:@[self.buddy]];
        NSArray *accounts = [OTRAccountsManager allAccountsAbleToAddBuddies];
        OTRAccount *account = [accounts firstObject];
        
        [[[OTRProtocolManager sharedInstance] protocolForAccount:account] createGroup:roomName arrInvited:arrBuddy];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)cancelButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)canAddBuddies
{
    if([OTRAccountsManager allAccountsAbleToAddBuddies]) {
        return YES;
    }
    return NO;
}

- (OTRBuddy *)buddyAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *viewIndexPath = [NSIndexPath indexPathForItem:indexPath.row inSection:0];
    
            __block OTRBuddy *buddy;
        [self.databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            buddy = [[transaction ext:OTRAllBuddiesDatabaseViewExtensionName] objectAtIndexPath:viewIndexPath withMappings:self.mappings];
        }];
        return buddy;
    
    

}



#pragma - mark keyBoardAnimation Methods
- (void)keyboardWillShow:(NSNotification *)notification
{
    [self animateTableViewWithKeyboardNotification:notification];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    [self animateTableViewWithKeyboardNotification:notification];
}

- (void)animateTableViewWithKeyboardNotification:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    //
    // Get keyboard size.
    NSValue *endFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [self.view convertRect:endFrameValue.CGRectValue fromView:nil];
    
    //
    // Get keyboard animation.
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    CGFloat height = keyboardEndFrame.size.height;
    if ([notification.name isEqualToString:UIKeyboardWillHideNotification]) {
        height = 0;
    }
    
    [self animateTableViewToKeyboardHeight:height animationCurve:animationCurve animationDuration:animationDuration];
}

- (void)animateTableViewToKeyboardHeight:(CGFloat)keyBoardHeight animationCurve:(UIViewAnimationCurve)animationCurve animationDuration:(NSTimeInterval)animationDuration
{
    self.tableViewBottomConstraint.constant = -keyBoardHeight;
    void (^animations)() = ^() {
        [self.view layoutIfNeeded];
    };
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:(animationCurve << 16)
                     animations:animations
                     completion:nil];
    
}

#pragma - mark UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    BOOL canAddBuddies = [self canAddBuddies];
    NSInteger sections = 0;
           sections = [self.mappings numberOfSections];
    
    
//    if (canAddBuddies) {
//        sections += 1;
//    }
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
//    if (section == 0 && [self canAddBuddies]) {
//        numberOfRows = 1;
//    }
//    else {
    
        
            numberOfRows = [self.mappings numberOfItemsInSection:0];
        
//    }
   
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if(indexPath.section == 0 && [self canAddBuddies]) {
//        // add new buddy cell
//        static NSString *addCellIdentifier = @"addCellIdentifier";
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addCellIdentifier];
//        if (!cell) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCellIdentifier];
//        }
//        cell.textLabel.text = ADD_BUDDY_STRING;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        
//        return cell;
//    }
//    else {
//        OTRBuddyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[OTRBuddyInfoCell reuseIdentifier] forIndexPath:indexPath];
        
        static NSString *CellIdentifier = @"OTRBuddyInfoCell222";
        OTRBuddyInfoCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        
        if (cell == nil) {//nil -- create a new cell
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OTRBuddyInfoCell" owner:nil options:nil];
            for (id oneObject in nib)
                if ([oneObject isKindOfClass:[OTRBuddyInfoCell class]])
                    cell = (OTRBuddyInfoCell *)oneObject;
            
        }

        cell.tag = indexPath.row;
        
        OTRBuddy * buddy = [self buddyAtIndexPath:indexPath];
        
        __block NSString *buddyAccountName = nil;
        [self.databaseConnection readWithBlock:^(YapDatabaseReadTransaction *transaction) {
            buddyAccountName = [OTRAccount fetchObjectWithUniqueID:buddy.accountUniqueId transaction:transaction].username;
        }];
        [cell setBuddy:buddy];
        [cell setBuddy:buddy withAccountName:buddyAccountName];
        [cell.imgBackground setBackgroundColor:[UIColor whiteColor]];
        [cell.avatarImageView.layer setCornerRadius:cell.avatarImageView.frame.size.width /2];
        if ([buddy.username containsString:@"conference"])
        {
            [cell.avatarImageView setImage:[UIImage imageNamed:@"icon_group"]];
            
        }
        else
        {
            [cell.avatarImageView setImage:[UIImage imageWithData:buddy.avatarData]];
            
        }
        if(self.isGroup)
        {
            [cell.imgCheck setHidden:NO];
            if ([arrIndex containsObject:[NSString stringWithFormat:@"%ld",(long)cell.tag]])
            {
                cell.imgCheck.highlighted = YES;
            }
            else
            {
                cell.imgCheck.highlighted = NO;
            }
        }
        else
        {
            [cell.imgCheck setHidden:YES];
        }
        return cell;
    
    
    
}

#pragma - mark UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return OTRBuddyInfoCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return OTRBuddyInfoCellHeight;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *accounts = [OTRAccountsManager allAccountsAbleToAddBuddies];
//    if(indexPath.section == 0 && [accounts count])
//    {
//        
//        //add buddy cell
//        UIViewController *viewController = nil;
//        if([accounts count] > 1) {
//            // pick wich account
//            viewController = [[OTRChooseAccountViewController alloc] init];
//            
//        }
//        else {
//            OTRAccount *account = [accounts firstObject];
//            viewController = [[OTRNewBuddyViewController alloc] initWithAccountId:account.uniqueId];
//        }
//        [self.navigationController pushViewController:viewController animated:YES];
//        
//    }
//    else if ([self.delegate respondsToSelector:@selector(controller:didSelectBuddy:)]) {
    
        if (self.isGroup)
        {
            OTRBuddyInfoCell *cell = (OTRBuddyInfoCell*)[tableView cellForRowAtIndexPath:indexPath];
            if ([arrIndex containsObject:[NSString stringWithFormat:@"%ld",(long)cell.tag]])
            {
                [arrIndex removeObject:[NSString stringWithFormat:@"%ld",(long)cell.tag]];
                cell.imgCheck.highlighted = NO;
            }
            else
            {
                [arrIndex addObject:[NSString stringWithFormat:@"%ld",(long)cell.tag]] ;
                cell.imgCheck.highlighted = NO;
            }
            [tableView reloadData];
        }
        else
        {
            OTRBuddy * buddy = [self buddyAtIndexPath:indexPath];
            ///check group
            if([buddy.username containsString:@"conference.peerbits.in"])
            {
                NSArray *accounts = [OTRAccountsManager allAccountsAbleToAddBuddies];
                OTRAccount *account = [accounts firstObject];
                
                [[[OTRProtocolManager sharedInstance] protocolForAccount:account] joinRoom:buddy.username];
                buddy.isGroup = YES;
                

            }
            
            
            
            [self.delegate controller:self didSelectBuddy:buddy];
        }
//    }
}

#pragma - mark UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma - mark YapDatabaseViewUpdate

- (void)yapDatabaseDidUpdate:(NSNotification *)notification;
{
    // Process the notification(s),
    // and get the change-set(s) as applies to my view and mappings configuration.
    NSArray *notifications = [self.databaseConnection beginLongLivedReadTransaction];
    
    NSArray *sectionChanges = nil;
    NSArray *rowChanges = nil;
    
      
    [[self.databaseConnection ext:OTRAllBuddiesDatabaseViewExtensionName] getSectionChanges:&sectionChanges
                                                                                 rowChanges:&rowChanges
                                                                           forNotifications:notifications
                                                                               withMappings:self.mappings];
    
    // No need to update mappings.
    // The above method did it automatically.
    
    if ([sectionChanges count] == 0 & [rowChanges count] == 0)
    {
        // Nothing has changed that affects our tableView
        return;
    }
    
    // Familiar with NSFetchedResultsController?
    // Then this should look pretty familiar
    
    [self.tableView beginUpdates];
    
    BOOL canAddBuddies = [self canAddBuddies];
    
    for (YapDatabaseViewSectionChange *sectionChange in sectionChanges)
    {
        NSUInteger sectionIndex = sectionChange.index;
//        if (canAddBuddies) {
//            sectionIndex += 1;
//        }
        
        switch (sectionChange.type)
        {
            case YapDatabaseViewChangeDelete :
            {
                [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            }
            case YapDatabaseViewChangeInsert :
            {
                [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            }
            case YapDatabaseViewChangeMove :
            case YapDatabaseViewChangeUpdate :
                break;
        }
    }
    
    
    
    for (YapDatabaseViewRowChange *rowChange in rowChanges)
    {
        NSIndexPath *indexPath = rowChange.indexPath;
        NSIndexPath *newIndexPath = rowChange.newIndexPath;
//        if (canAddBuddies) {
//            indexPath = [NSIndexPath indexPathForItem:rowChange.indexPath.row inSection:1];
//            newIndexPath = [NSIndexPath indexPathForItem:rowChange.newIndexPath.row inSection:1];
//        }
//        else {
//            
//        }
        
        switch (rowChange.type)
        {
            case YapDatabaseViewChangeDelete :
            {
                [self.tableView deleteRowsAtIndexPaths:@[ indexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            }
            case YapDatabaseViewChangeInsert :
            {
                [self.tableView insertRowsAtIndexPaths:@[ newIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            }
            case YapDatabaseViewChangeMove :
            {
                [self.tableView deleteRowsAtIndexPaths:@[ indexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableView insertRowsAtIndexPaths:@[ newIndexPath ]
                                      withRowAnimation:UITableViewRowAnimationAutomatic];
                break;
            }
            case YapDatabaseViewChangeUpdate :
            {
                [self.tableView reloadRowsAtIndexPaths:@[ indexPath ]
                                      withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    }
    
    [self.tableView endUpdates];
}


@end
