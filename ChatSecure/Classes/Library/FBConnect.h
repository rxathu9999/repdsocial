//
//  FBConnect.h
//  yuppie
//
//  Created by BuiSiNam on 7/15/13.
//  Copyright (c) 2013 Egood JSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>
typedef void (^completeFbBlock)(NSMutableDictionary *data);
typedef void (^errorFbBlock)(NSError *error);


typedef void (^completeBlock)(NSMutableArray *data);
typedef void (^errorBlock)(NSMutableDictionary *error);


typedef void (^completeID)(id *data);

@interface FBConnect : NSObject{
    NSMutableData *data_;
    completeFbBlock completeFbBlock_;
    errorFbBlock errorFbBlock_;
    
    completeBlock completeBlock_;
    completeID completeID_;
}

-(void)getLocation:(completeBlock)completeFbBlock
      errorFbBlock:(errorFbBlock)errorFbBlock;

-(void)loginFaceBook:(completeFbBlock)completeFbBlock
           errorFbBlock:(errorFbBlock)errorFbBlock;


-(void)getUserEvent:(completeBlock)completeFbBlock
        errorFbBlock:(errorFbBlock)errorFbBlock;


-(void)createEventFacebook:(completeFbBlock)completeFbBlock
              errorFbBlock:(errorFbBlock)errorFbBlock item:(EKEvent *)item index:(int)index;

// nhn
-(void)getUserEventNotReplied:(completeFbBlock)completeFbBlock
                 errorFbBlock:(errorFbBlock)errorFbBlock;

-(void)postRSVP:(completeFbBlock)completeFbBlock
errorFbBlock:(errorFbBlock)errorFbBlock
       id_event:(NSString *)idEvent withRSVP:(NSString *)RSVP;

-(void)getOrganizeEvent:(completeBlock)completeFbBlock
           errorFbBlock:(errorFbBlock)errorFbBlock;

-(void)cancelEvent:(completeFbBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
          id_event:(NSString *)idEvent;
-(void)detailEvent:(completeFbBlock)completeFbBlock
errorFbBlock:(errorFbBlock)errorFbBlock
          id_event:(NSString *)idEvent;

//nhn
-(void)getFriendList:(completeBlock)completeFbBlock
        errorFbBlock:(errorFbBlock)errorFbBlock;

-(void)postInviteFriend:(completeFbBlock)completeFbBlock
           errorFbBlock:(errorFbBlock)errorFbBlock
               id_event:(NSString *)idEvent withIdFriends:(NSString *)strIdFriends;
-(void)updateEvent:(completeFbBlock)completeFbBlock
errorFbBlock:(errorFbBlock)errorFbBlock
          id_event:(NSString *)idEvent params:(NSMutableDictionary *)params;

-(void)createOrganizeEvent:(completeFbBlock)completeFbBlock
      errorFbBlock:(errorFbBlock)errorFbBlock
          params:(NSMutableDictionary *)params;
-(void)getMutualFriend:(completeBlock)completeFbBlock
          errorFbBlock:(errorFbBlock)errorFbBlock idFriend:(NSString *)idFriend;
@end