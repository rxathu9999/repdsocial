//
//  JSonConnect.h
//  yuppie
//
//  Created by Ho Anh Truc on 7/15/13.
//  Copyright (c) 2013 Egood JSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#define STR_URL_SERVER @"http://peerbits.in/apps/crossfit/web/v1/"
typedef void (^completeNetworkBlock)(NSMutableDictionary *data);
typedef void (^errorBlock)(NSMutableDictionary *error);

@interface JSonConnect : NSObject{
    NSMutableData *data_;
    completeNetworkBlock completeNetworkBlock_;
    errorBlock errorBlock_;
}

- (void)postPath:(NSString *)path
      parameters:(NSMutableDictionary *)params
   completeNetworkBlock:(completeNetworkBlock)completeNetworkBlock
      errorBlock:(errorBlock)errorBlock;

- (void)getPath:(NSString *)path
      parameters:(NSDictionary *)params
   completeNetworkBlock:(completeNetworkBlock)completeNetworkBlock
      errorBlock:(errorBlock)errorBlock;
@end
