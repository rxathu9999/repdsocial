//
//  LocationManager.m
//  nearby
//
//  Created by Bùi Si Nam on 11/17/14.
//  Copyright (c) 2014 BuiSiNam. All rights reserved.
//

#import "LocationManager.h"
#import "INTULocationManager.h"
@implementation LocationManager

-(id)init
{
    if (self = [super init]) {
        self.locationRequestID = NSNotFound;
    }
    return self;
    
}
-(void)getLocationCompleteBlock:(locationBlock)completeBlock
{
    completeLocationBlock=[completeBlock copy];
    
    
    __weak __typeof(self) weakSelf = self;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse
                                                                timeout:10
                                                   delayUntilAuthorized:YES
                                                                  block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                      __typeof(weakSelf) strongSelf = weakSelf;
                                                                      
                                                                      if (status == INTULocationStatusSuccess) {
                                                                          // achievedAccuracy is at least the desired accuracy (potentially better)
                                                                         completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);
                                                                      }
                                                                      else if (status == INTULocationStatusTimedOut) {
                                                                          // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                                                          completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);
                                                                      }
                                                                      else {
                                                                          // An error occurred
                                                                          if (status == INTULocationStatusServicesNotDetermined) {
                                                                              completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);
                                                                          } else if (status == INTULocationStatusServicesDenied) {
                                                                              completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);;
                                                                          } else if (status == INTULocationStatusServicesRestricted) {
                                                                              completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);;
                                                                          } else if (status == INTULocationStatusServicesDisabled) {
                                                                              completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);;
                                                                          } else {
                                                                              completeLocationBlock(currentLocation.coordinate.longitude,currentLocation.coordinate.latitude);;
                                                                          }
                                                                      }
                                                                      
                                                                      strongSelf.locationRequestID = NSNotFound;
                                                                  }];

}

@end
