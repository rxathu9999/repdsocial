//
//  LocationGPS.m
//  Transport
//
//  Created by buisinam on 10/25/13.
//  Copyright (c) 2013 buisinam. All rights reserved.
//

#import "LocationGPS.h"
//#import "CustomAlertView.h"
#import "SBJsonParser.h"

@implementation LocationGPS
static LocationGPS *sharedInstance;
+ (LocationGPS *)sharedInstance {
    @synchronized(self) {
        
        sharedInstance=[[LocationGPS alloc] init];
        
    }
    
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        complete=false;
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
    }
    return self;
    
}

-(void)getLocationCompleteBlock:(locationBlock)completeBlock
{
    completeLocationBlock=[completeBlock copy];
    
    
    
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //completeLocationBlock(106.70667,10.77591);
    completeLocationBlock(0, 0);
    
    //    NSLog(@"didFailWithError: %@", error);
    //    CustomAlertView *errorAlert = [[CustomAlertView alloc]
    //                               initWithTitle:@"feil" message:@"Klarte ikke å få din Sted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [errorAlert show];
    complete = true;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    completeLocationBlock(newLocation.coordinate.longitude,newLocation.coordinate.latitude);
    complete = true;
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    if(!complete)
    {
        CLLocation *location = [locations lastObject];
        completeLocationBlock(location.coordinate.longitude,location.coordinate.latitude);
        complete = true;
    }
    
}


-(NSMutableDictionary *)ConvertAddressToLongLat:(NSString *)locationString
{
    
    
    NSError *error = nil;
    NSString *addressSearch = [locationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *urlString = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?address=%@&sensor=false",addressSearch]];
    NSLog(@"%@",urlString);
    locationString = [[NSString alloc] initWithContentsOfURL:urlString encoding:NSUTF8StringEncoding error:&error];
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    NSMutableDictionary *jsonObjects = [jsonParser objectWithString:locationString error:&error];
    NSMutableArray *resultsArr=[[NSMutableArray alloc]init];
    resultsArr = [jsonObjects objectForKey:@"results"];
    NSLog(@"%@",resultsArr);
    NSMutableDictionary *locationDic;
    if(resultsArr.count>0){
        NSMutableDictionary *formattedAddress = [resultsArr objectAtIndex:0];
        return locationDic = [[formattedAddress valueForKey:@"geometry"] valueForKey:@"location"];
    }
    else
        return locationDic=[[NSMutableDictionary alloc] init];
    
    
    
}

@end
