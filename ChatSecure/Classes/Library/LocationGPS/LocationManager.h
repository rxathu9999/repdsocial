//
//  LocationManager.h
//  nearby
//
//  Created by Bùi Si Nam on 11/17/14.
//  Copyright (c) 2014 BuiSiNam. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^locationBlock)(double longtitude,double latitude);
@interface LocationManager : NSObject{
    locationBlock completeLocationBlock;
    bool complete;
}
@property (assign, nonatomic) NSInteger locationRequestID;

-(void)getLocationCompleteBlock:(locationBlock)completeBlock;

@end
