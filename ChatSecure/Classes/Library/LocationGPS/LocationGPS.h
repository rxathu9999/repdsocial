//
//  LocationGPS.h
//  Transport
//
//  Created by buisinam on 10/25/13.
//  Copyright (c) 2013 buisinam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^locationBlock)(double longtitude,double latitude);
@interface LocationGPS : NSObject<CLLocationManagerDelegate>{
    locationBlock completeLocationBlock;
    bool complete;
}
+ (LocationGPS *)sharedInstance;
-(NSMutableDictionary *)ConvertAddressToLongLat:(NSString *)locationString;
-(void)getLocationCompleteBlock:(locationBlock)completeBlock;
@property(nonatomic,strong)CLLocationManager *locationManager;
@end