//
//  RAMCollectionViewCell.m
//  RAMCollectionViewFlemishBondLayoutDemo
//
//  Created by Rafael Aguilar Martín on 20/10/13.
//  Copyright (c) 2013 Rafael Aguilar Martín. All rights reserved.
//

#import "RAMCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@interface RAMCollectionViewCell ()
    @property (nonatomic, strong) UILabel *label;
    @property (nonatomic, strong) UIImageView *imgView;
@end

@implementation RAMCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark - Setup
- (void)setup
{
    [self setupView];
    [self setupLabel];
}

- (void)setupView
{
    self.backgroundColor = [UIColor whiteColor];
}

- (void)setupLabel
{
//    self.label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 60, 30)];
    self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.imgView.contentMode = UIViewContentModeCenter;
    [self addSubview:self.imgView];
}

#pragma mark - Configure
- (void)configureCellWithIndexPath:(NSIndexPath *)indexPath
{
//    self.label.text = [NSString stringWithFormat:@"Cell %ld", (long)(indexPath.row + 1)];
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:@"http://images1.fanpop.com/images/photos/1800000/Leah-s-photoshoot-for-website-profile-leah-dizon-1856157-531-800.jpg"] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.imgView setImage:[self imageWithImage:image scaledToWidth:self.imgView.frame.size.width]];
    }];
    
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
