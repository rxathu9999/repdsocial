//
//  NavigationViewController.h
//  Restaurant
//
//  Created by Bùi Si Nam on 9/5/14.
//  Copyright (c) 2014 buisinam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavigationViewControllerDelegate <NSObject>

@optional
-(void)clickLeftMenu;

@end

@interface NavigationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWelCome;
@property (strong, nonatomic)id<NavigationViewControllerDelegate> delegte;
@end
