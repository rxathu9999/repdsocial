//
//  UILabel+Boldify.h
//  ChatSecure
//
//  Created by buisinam on 7/7/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)
- (void) boldSubstring: (NSString*) substring;
- (void) boldRange: (NSRange) range;
- (void) autosizeForWidth: (float) width;
@end
