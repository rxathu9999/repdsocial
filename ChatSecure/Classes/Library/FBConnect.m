    //
//  FBConnect.m
//  yuppie
//
//  Created by BuiSiNam on 7/15/13.
//  Copyright (c) 2013 Egood JSC. All rights reserved.
//

#import "FBConnect.h"
#import <FacebookSDK/FacebookSDK.h>
#import <EventKit/EventKit.h>
@implementation FBConnect
//-(void)loginFaceBook:(completeFbBlock)completeFbBlock
//        errorFbBlock:(errorFbBlock)errorFbBlock
//{
//    completeFbBlock_ = [completeFbBlock copy];
//    errorFbBlock_ = [errorFbBlock copy];
//    
//    
//    
//    
//    
//    
//
//    // If the session state is any of the two "open" states when the button is clicked
//    if (FBSession.activeSession.state == FBSessionStateOpen
//        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//        
//        // Close the session and remove the access token from the cache
//        // The session state handler (in the app delegate) will be called automatically
//        [FBSession.activeSession closeAndClearTokenInformation];
//        errorFbBlock_(nil);
//
//        // If the session state is not any of the two "open" states when the button is clicked
//    } else {
//        // Open a session showing the user the login UI
//        // You must ALWAYS ask for basic_info permissions when opening a session
//        NSArray *permissions = @[@"basic_info",@"email"];
//        
//        ;
//        
//        
//        [FBSession openActiveSessionWithReadPermissions:permissions
//                                           allowLoginUI:YES
//                                      completionHandler:^(FBSession *session,
//                                                          FBSessionState status,
//                                                          NSError *error) {
//                                          // if login fails for any reason, we alert
//                                          if (error) {
//                                              
//                                              UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Login fail" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                                              [alertView show];
//                                              errorFbBlock_(error);
//                                              // if otherwise we check to see if the session is open, an alternative to
//                                              // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
//                                              // property of the session object; the macros are useful, however, for more
//                                              // detailed state checking for FBSession objects
//                                          } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
//                                              // send our requests if we successfully logged in
//                                               [self sessionStateChanged:session state:status error:error];
//                                              [FBRequestConnection
//                                               startForMeWithCompletionHandler:^(FBRequestConnection *connection,
//                                                                                 id<FBGraphUser> user,
//                                                                                 NSError *error) {
//                                                    if (!error) {
//                                                           NSMutableDictionary *data=(NSMutableDictionary*)user;
//                                                           NSMutableDictionary * result=[[NSMutableDictionary alloc]init];
//                                                           
//                                                           [result setValue:user.id forKey:@"id"];
//                                                           [result setValue:user.first_name forKey:@"first_name"];
//                                                           [result setValue:user.last_name forKey:@"last_name"];
//                                                           [result setValue:[data valueForKey:@"email"] forKey:@"email"];
//                                                           [result setValue:[data valueForKey:@"gender"] forKey:@"gender"];
//                                                           [result setValue:user.first_name forKey:@"first_name"];
//                                                           [result setValue:user.birthday forKey:@"birthday"];
//                                                            [result setValue:[data valueForKey:@"name"] forKey:@"name"];
//                                                           [result setValue:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=normal", user.id] forKey:@"avatar"];
//                                                        
//                                               
//                                             
//                                             
//                                             completeFbBlock_(result);
//                                             
//                                                        
//                             
//                             
//                             
//                         }
//                         else
//                         {
//                             errorFbBlock_(error);
//                         }
//                         
//                         
//                     }];
//
//
//}
//}];
//    }

    


    
    
//}


- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        // Show the user the logged-in UI
        //        [self userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        //        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            //            [self showMessage:alertText withTitle:alertTitle];
           
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:alertText delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You cancelled login" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];

                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                //                [self showMessage:alertText withTitle:alertTitle];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:alertText delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];

                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:alertText delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];

            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        //        [self userLoggedOut];
    }
}
- (void)logout{
    if (FBSession.activeSession.isOpen){
        [FBSession.activeSession closeAndClearTokenInformation];
        [FBSession setActiveSession:nil];
    }
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
    
    for (NSHTTPCookie* cookie in facebookCookies) {
        [cookies deleteCookie:cookie];
    }
    
    
}

-(void)loginFaceBook:(completeFbBlock)completeFbBlock
        errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeFbBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
//    NSArray *permissions = @[@"user_about_me",@"user_birthday",@"email",@"user_photos",@"public_profile"];

    
//    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//        
//        if (status == FBSessionStateOpen) {
//            
//            FBRequest *fbRequest = [FBRequest requestForMe];
//            [fbRequest setSession:session];
//            
//            [fbRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error){
//                NSMutableDictionary *userInfo = nil;
//                if( [result isKindOfClass:[NSDictionary class]] ){
//                    userInfo = (NSMutableDictionary *)result;
//                    if( [userInfo count] > 0 ){
//                        [userInfo setObject:session.accessTokenData.accessToken forKey:@"accessToken"];
//                    }
//                }
//
//            }];
//        }else if(status == FBSessionStateClosedLoginFailed){
//
//        }
//    }];


    
    
    [self logout];
    
    // If the session state is any of the two "open" states when the button is clicked
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        errorFbBlock_(nil);
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for basic_info permissions when opening a session
        
//        @[@"user_about_me",
//          @"user_birthday",
//          @"email",
//          @"user_photos",
//          @"publish_stream",
//          @"user_events",
//          @"friends_events",
//          @"manage_pages",
//          @"share_item",
//          @"publish_actions",
//          @"user_friends",
//          @"manage_pages",
//          @"user_videos",
//          @"public_profile"]];
        
        
        NSArray *permissions = @[@"user_about_me",@"email",@"user_birthday",@"public_profile",@"user_birthday",@"user_photos"];

        ;
        
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState status,
                                                          NSError *error) {
                                          // if login fails for any reason, we alert
                                          if (error) {
                                              
                                              UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Login fail" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                              [alertView show];
                                              errorFbBlock_(error);
                                              // if otherwise we check to see if the session is open, an alternative to
                                              // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                              // property of the session object; the macros are useful, however, for more
                                              // detailed state checking for FBSession objects
                                          } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                              // send our requests if we successfully logged in
                                              [self sessionStateChanged:session state:status error:error];
                                              [FBRequestConnection
                                               startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                                                                 id<FBGraphUser> user,
                                                                                 NSError *error) {
                                                   if (!error) {
                                                       NSMutableDictionary *data=(NSMutableDictionary*)user;
                                                       NSMutableDictionary * result=[[NSMutableDictionary alloc]init];
                                                       
                                                       [result setValue:user.objectID forKey:@"id"];
                                                       [result setValue:user.first_name forKey:@"first_name"];
                                                       [result setValue:user.last_name forKey:@"last_name"];
                                                       [result setValue:[data valueForKey:@"email"] forKey:@"email"];
                                                       [result setValue:[data valueForKey:@"gender"] forKey:@"gender"];
                                                       [result setValue:user.first_name forKey:@"first_name"];
                                                       [result setValue:user.birthday forKey:@"birthday"];
                                                       [result setValue:[data valueForKey:@"name"] forKey:@"name"];
                                                       [result setValue:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=normal", user.objectID] forKey:@"avatar"];
                                                       
                                                       
                                                       
                                                       
                                                       completeFbBlock_(result);
}
                                                   else
                                                   {
                                                       errorFbBlock_(error);
                                                   }
                                                   
                                                   
                                               }];
                                              
                                              
                                              
                                              
                                          }
                                      }];
    }
    
    
    
    
    
    
}


-(void)getUserEvent:(completeBlock)completeFbBlock
       errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    // We will request the user's events
    // These are the permissions we need:
    
   
    
    
    if (FBSession.activeSession.isOpen)
    {
        [FBRequestConnection startWithGraphPath:@"me/events?fields=id,name,start_time,rsvp_status,owner,cover,invited.fields(rsvp_status,name),location,parent_group,feed_targeting"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  if (!error) {
                                      // Sucess! Include your code to handle the results here
                                      //                                                                        NSLog(@"user events: %@", result);
                                      NSMutableArray *arrayEvent=[result valueForKey:@"data"];
                                      
                                      
                                      
                                      // get event not_replied
                                      [FBRequestConnection startWithGraphPath:@"me/events/not_replied?fields=owner,name,location,end_time,rsvp_status,start_time,id,invited.fields(name,rsvp_status),cover,parent_group,feed_targeting"
                                                            completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                if (!error) {
                                                                    // Sucess! Include your code to handle the results here
                                                                    //                                                                        NSLog(@"user events: %@", result);
                                                                    NSMutableArray *arrInvited=[result valueForKey:@"data"];
                                                                    for (int i=0; i<arrInvited.count; i++) {
                                                                        [arrayEvent addObject:[arrInvited objectAtIndex:i] ];
                                                                    }
                                                                    //completeBlock_(arrayEvent);
                                                                    
                                                                } else {
                                                                    // An error occurred, we need to handle the error
                                                                    // See: https://developers.facebook.com/docs/ios/errors
                                                                    completeBlock_(arrayEvent);
                                                                }
                                        }];
                                      
                                      // get event declined
                                      [FBRequestConnection startWithGraphPath:@"me/events/declined?fields=owner,name,location,end_time,rsvp_status,start_time,id,invited.fields(name,rsvp_status),cover,parent_group,feed_targeting"
                                                            completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                if (!error) {
                                                                    // Sucess! Include your code to handle the results here
                                                                    //                                                                        NSLog(@"user events: %@", result);
                                                                    NSMutableArray *arrInvited=[result valueForKey:@"data"];
                                                                    for (int i=0; i<arrInvited.count; i++) {
                                                                        [arrayEvent addObject:[arrInvited objectAtIndex:i] ];
                                                                    }
                                                                    completeBlock_(arrayEvent);
                                                                    
                                                                } else {
                                                                    // An error occurred, we need to handle the error
                                                                    // See: https://developers.facebook.com/docs/ios/errors
                                                                    completeBlock_(arrayEvent);
                                                                }
                                                            }];

                                      
                                  } else {
                                      // An error occurred, we need to handle the error
                                      // See: https://developers.facebook.com/docs/ios/errors
                                      errorFbBlock_(error);
                                  }
        }];

    
    }
    else
    {
    NSArray *permissions = @[@"user_events"];

    
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      // if login fails for any reason, we alert
                                      if (error) {
                                          UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Đăng nhập thất bại" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                          [alertView show];
                                          errorFbBlock_(error);
                                          // if otherwise we check to see if the session is open, an alternative to
                                          // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                          // property of the session object; the macros are useful, however, for more
                                          // detailed state checking for FBSession objects
                                      } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                          // send our requests if we successfully logged in
                                          
                                          [FBRequestConnection startWithGraphPath:@"me/events?fields=id,name,start_time,rsvp_status,owner,cover,invited.fields(rsvp_status,name),location,parent_group,feed_targeting"
                                                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                    if (!error) {
                                                                        // Sucess! Include your code to handle the results here
                                                                        //                                                                        NSLog(@"user events: %@", result);
                                                                        NSMutableArray *arrayEvent=[result valueForKey:@"data"];
                                                                        
                                                                        // get event not_replied
                                                                        [FBRequestConnection startWithGraphPath:@"me/events/not_replied?fields=owner,name,location,end_time,rsvp_status,start_time,id,invited.fields(name,rsvp_status),cover,parent_group,feed_targeting"
                                                                                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                                                  if (!error) {
                                                                                                      // Sucess! Include your code to handle the results here
                                                                                                      //                                                                        NSLog(@"user events: %@", result);
                                                                                                      NSMutableArray *arrInvited=[result valueForKey:@"data"];
                                                                                                      for (int i=0; i<arrInvited.count; i++) {
                                                                                                          [arrayEvent addObject:[arrInvited objectAtIndex:i] ];
                                                                                                      }
                                                                                                      //completeBlock_(arrayEvent);
                                                                                                      
                                                                                                  } else {
                                                                                                      // An error occurred, we need to handle the error
                                                                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                                                                      completeBlock_(arrayEvent);
                                                                                                  }
                                                                                              }];
                                                                        
                                                                        // get event declined
                                                                        [FBRequestConnection startWithGraphPath:@"me/events/declined?fields=owner,name,location,end_time,rsvp_status,start_time,id,invited.fields(name,rsvp_status),cover,parent_group,feed_targeting"
                                                                                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                                                  if (!error) {
                                                                                                      // Sucess! Include your code to handle the results here
                                                                                                      //                                                                        NSLog(@"user events: %@", result);
                                                                                                      NSMutableArray *arrInvited=[result valueForKey:@"data"];
                                                                                                      for (int i=0; i<arrInvited.count; i++) {
                                                                                                          [arrayEvent addObject:[arrInvited objectAtIndex:i] ];
                                                                                                      }
                                                                                                      completeBlock_(arrayEvent);
                                                                                                      
                                                                                                  } else {
                                                                                                      // An error occurred, we need to handle the error
                                                                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                                                                      completeBlock_(arrayEvent);
                                                                                                  }
                                                                                              }];
                                                                        
                                                                        
                                                                    } else {
                                                                        // An error occurred, we need to handle the error
                                                                        // See: https://developers.facebook.com/docs/ios/errors
                                                                        errorFbBlock_(error);
                                                                    }
                                                                }];
                                          
                                          
                                      }
                                  }];
    
    }
}

-(void)getOrganizeEvent:(completeBlock)completeFbBlock
       errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    // We will request the user's events
    // These are the permissions we need:
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *uid=[defaults valueForKey:@"uid"];
    
    
    if (FBSession.activeSession.isOpen)
    {
        [FBRequestConnection startWithGraphPath:@"me/events?fields=id,name,start_time,rsvp_status,owner,cover,invited.fields(rsvp_status,name),location,parent_group,feed_targeting"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  if (!error) {
                                      // Sucess! Include your code to handle the results here
                                      //                                                                        NSLog(@"user events: %@", result);
                                      NSMutableArray *arrayEvent=[result valueForKey:@"data"];
                                      NSMutableArray *arrResult=[[NSMutableArray alloc] init];
                                      
                                      for (int i=0; i<arrayEvent.count; i++) {
                                          NSMutableDictionary *object=[arrayEvent objectAtIndex:i];
                                          NSString *owner=[[object valueForKey:@"owner"] valueForKey:@"id"];
                                          if([owner isEqualToString:uid])
                                             [arrResult addObject:[arrayEvent objectAtIndex:i]];
                                      }
                                      
                                      completeBlock_(arrResult);
                                      
                                      
                                      
                                  } else {
                                      // An error occurred, we need to handle the error
                                      // See: https://developers.facebook.com/docs/ios/errors
                                      errorFbBlock_(error);
                                  }
                              }];
        
        
    }
    else
    {
        NSArray *permissions = @[@"user_events"];
        
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState status,
                                                          NSError *error) {
                                          // if login fails for any reason, we alert
                                          if (error) {
                                              UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Đăng nhập thất bại" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                              [alertView show];
                                              errorFbBlock_(error);
                                              // if otherwise we check to see if the session is open, an alternative to
                                              // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                              // property of the session object; the macros are useful, however, for more
                                              // detailed state checking for FBSession objects
                                          } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                              // send our requests if we successfully logged in
                                              
                                              [FBRequestConnection startWithGraphPath:@"me/events?fields=id,name,start_time,rsvp_status,owner,cover,invited.fields(rsvp_status,name),location,parent_group,feed_targeting"
                                                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                        if (!error) {
                                                                            // Sucess! Include your code to handle the results here
                                                                            //                                                                        NSLog(@"user events: %@", result);
                                                                            NSMutableArray *arrayEvent=[result valueForKey:@"data"];
                                                                            NSMutableArray *arrResult=[[NSMutableArray alloc] init];
                                                                            
                                                                            for (int i=0; i<arrayEvent.count; i++) {
                                                                                NSMutableDictionary *object=[arrayEvent objectAtIndex:i];
                                                                                NSString *owner=[[object valueForKey:@"owner"] valueForKey:@"id"];
                                                                                if([owner isEqualToString:uid])
                                                                                   [arrResult addObject:[arrayEvent objectAtIndex:i]];
                                                                                   }
                                                                                   
                                                                                   completeBlock_(arrResult);

                                                                            
                                                                            
                                                                            
                                                                            
                                                                        } else {
                                                                            // An error occurred, we need to handle the error
                                                                            // See: https://developers.facebook.com/docs/ios/errors
                                                                            errorFbBlock_(error);
                                                                        }
                                                                    }];
                                              
                                              
                                          }
                                      }];
        
    }
}



-(void)createEventFacebook:(completeFbBlock)completeFbBlock
              errorFbBlock:(errorFbBlock)errorFbBlock item:(EKEvent *)item index:(int)index
{
//    UIImage *image = [[UIImage alloc]initWithImageNamed@"test.png"];
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                   [delegate facebook].accessToken, @"access_token",
//                                   @"Going To Eat!", @"name",
//                                   @"description of the event", @"description",
//                                   @"2012-08-20T17:00:00+0000", @"start_time",
//                                   @"2012-08-21T17:00:00+0000", @"end_time",
//                                   @"Carlsbad", @"city",
//                                   @"CA", @"state",
//                                   @"900 safe street", @"street",
//                                   @"OPEN", @"privacy",
//                                   image, @"picture",
//                                   nil];
    
    completeFbBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];

    
//    if (FBSession.activeSession.isOpen)
//    {
    
     NSMutableDictionary *params =[[NSMutableDictionary alloc] init];
    
    NSString *start_date=[self convertDate:item.startDate];
    NSString *end_date=[self convertDate:item.endDate];
    NSString *location=item.location;
    if(location!=nil)
        [params setValue:item.location forKey:@"location"];
    
    else
        [params setValue:@"" forKey:@"location"];
   
    
    
    [params setValue:item.title forKey:@"name"];
    
    [params setValue:start_date forKey:@"start_time"];
    [params setValue:end_date forKey:@"end_time"];
   
    
    
    NSLog(@"%@----%@",start_date,end_date);
//
    
//        NSString * pRequest = [NSString stringWithFormat:@"me/events"];
//        
//        [FBRequestConnection startWithGraphPath:pRequest parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//            
//            if (!error)
//            {
//                NSLog(@"Facebook Post Success..");
//                completeFbBlock_(result);
//                
//            } else
//            {
//                NSLog(@"Facebook Post Failed..");
//                
//                NSLog(@"ERROR : %@", error.localizedDescription);
//                errorFbBlock_(error);
//            }
//            
//        }];
    
   
//    FBRequest *facebook = [[FBRequest alloc] initWithSession:facebook.session graphPath:@"me/events" parameters:params HTTPMethod:@"POST"];
    
    
//    NSMutableArray *array=[NSMutableArray arrayWithObject:@"create_events"];
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                   @"My Test Event",@"name",
//                                   @"2013-09-04 20:00",@"start_time",
//                                   @"2013-09-05 20:00",@"end_time",@"SECRET",@"privacy_type", nil];
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//                            @"This is a test event", @"name",
//                            @"2014-03-01T13:00:00-0800", @"start_time",
//                            nil
//                            ];
    /* make the API call */
    [FBRequestConnection startWithGraphPath:@"/me/events"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              /* handle the result */
                              if(!error)
                              {
                                  NSLog(@"success post");
                                  
                                  [result setValue:[NSString stringWithFormat:@"%d",index] forKey:@"index"];
                                  completeFbBlock_(result);
                              }
                              else
                              {
                                  errorFbBlock_(error);
                              }
                          }];
//    }
//    else
//    {
//        
//        static NSString *PublishStreamPermission = @"create_event";
//        [FBSession
//         openActiveSessionWithPublishPermissions:@[PublishStreamPermission]
//         defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES
//         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//             if ([session state] != FBSessionStateOpen) {
//                 // failed to open a valid session
//             } else if ([[session permissions] containsObject:PublishStreamPermission]) {
//                 // session opened, permissions granted, now you can post
//                 NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                               [NSString stringWithFormat:@"%@",item.title],@"name",
//                                                item.location,@"location",
//                                                [self convertDate:item.startDate],@"start_time",
//                                                [self convertDate:item.endDate],@"end_time", nil];
//                 
//                 NSString * pRequest = [NSString stringWithFormat:@"me/events"];
//                 
//                 [FBRequestConnection startWithGraphPath:pRequest parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                     
//                     if (!error)
//                     {
//                         NSLog(@"Facebook Post Success..");
//                           completeFbBlock_(result);
//                     } else
//                     {
//                         NSLog(@"Facebook Post Failed..");
//                         errorFbBlock_(error);
//                         NSLog(@"ERROR : %@", error.localizedDescription);
//                     }
//                     
//                 }];
//
//             } else {
//                 // session opened, but requested permissions not granted
//                 errorFbBlock_(error);
//             }
//         }];
//
//    }
}

// nhn

- (void) getUserEventNotReplied:(completeFbBlock)completeFbBlock
                   errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeFbBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    // We will request the user's events
    // These are the permissions we need:
    NSArray *permissions = @[@"user_events"];
    
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      // if login fails for any reason, we alert
                                      if (error) {
                                          UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Đăng nhập thất bại" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                          [alertView show];
                                          errorFbBlock_(error);
                                          // if otherwise we check to see if the session is open, an alternative to
                                          // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                          // property of the session object; the macros are useful, however, for more
                                          // detailed state checking for FBSession objects
                                      } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                          // send our requests if we successfully logged in
                                          // "me/events/not_replied?fields=owner,name,location,end_time,rsvp_status,start_time,id,invited.fields(name,rsvp_status),cover,parent_group,feed_targeting"
                                          [FBRequestConnection startWithGraphPath:@"me/events/not_replied?fields=owner,name,location,end_time,rsvp_status,start_time,id,invited.fields(name,rsvp_status),cover,parent_group,feed_targeting"
                                                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                    if (!error) {
                                                                        // Sucess! Include your code to handle the results here
                                                                        //                                                                        NSLog(@"user events: %@", result);
                                                                        
                                                                        completeFbBlock_(result);
                                                                        
                                                                    } else {
                                                                        // An error occurred, we need to handle the error
                                                                        // See: https://developers.facebook.com/docs/ios/errors
                                                                    }
                                                                }];
                                          
                                          
                                      }
                                  }];
}

/*
 RSVP : maybe, attending (going), declined (not going)
 */
-(void)postRSVP:(completeFbBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
       id_event:(NSString *)idEvent withRSVP:(NSString *)RSVP
{
    
    completeFbBlock_=[completeFbBlock copy];
    errorFbBlock_=[errorFbBlock copy];
    
    
    
            NSString * pRequest = [NSString stringWithFormat:@"%@/%@",idEvent,RSVP];
            
            // Make the Graph API request to post the object
            FBRequest *request = [FBRequest requestForPostWithGraphPath:pRequest
                                                            graphObject:nil];
            [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    // Success! Include your code to handle the results here
                    NSLog(@"%@",[NSString stringWithFormat:@"result: %@", result]);
                    
                    completeFbBlock_(result);
                    
                } else {
                    // An error occurred, we need to handle the error
                    // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
                    NSLog(@"%@",[NSString stringWithFormat:@"error %@", error.description]);
                    errorFbBlock_(error);
                }
            }];
//    [FBSession
//     openActiveSessionWithPublishPermissions:@[PublishStreamPermission]
//     defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES
//     completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//         if ([session state] != FBSessionStateOpen) {
//             // failed to open a valid session
//         } else if ([[session permissions] containsObject:@[PublishStreamPermission]]) {
//             // session opened, permissions granted, now you can post
//             
//             //297727050380639/maybe
//             NSString * pRequest = [NSString stringWithFormat:@"%@/%@",idEvent,RSVP];
//             
//
//         } else {
//             // session opened, but requested permissions not granted
//             errorFbBlock_(error);
//             NSLog(@"Session not open..");
//         }
//     }];
}


-(NSString*)convertDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //                 [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"GMT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    
    NSString *dateAsString = [formatter stringFromDate:date];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
//    NSString *result=[dateAsString substringToIndex:19];
    
    
    
    return dateAsString;
}

-(void)cancelEvent:(completeFbBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
       id_event:(NSString *)idEvent
{
    completeFbBlock_=[completeFbBlock copy];
    errorFbBlock_=[errorFbBlock copy];
    
    // string /EVENT_ID/invited/USER_ID.
   
      [FBRequestConnection startWithGraphPath:idEvent
                                 parameters:nil
                                 HTTPMethod:@"DELETE"
                          completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {
         if (error) {
             NSLog(@"Error: %@", result);
             errorFbBlock_(error);
         } else {
             completeFbBlock_ (result);

             NSLog(@"Success: %@", result);
         }
     }];
    
    
  }

///Nam code
-(void)detailEvent:(completeFbBlock)completeFbBlock
      errorFbBlock:(errorFbBlock)errorFbBlock
          id_event:(NSString *)idEvent
{
    
    completeFbBlock_=[completeFbBlock copy];
    errorFbBlock_=[errorFbBlock copy];
    
    /* make the API call */
    
//    NSMutableDictionary *
    
    [FBRequestConnection startWithGraphPath:idEvent
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              /* handle the result */
                              if(!error)
                              {
                                  completeFbBlock_(result);
                              }
                              else
                              {
                                  errorFbBlock_(error);
                              }
                          }];
}

-(void)getFriendList:(completeBlock)completeFbBlock
        errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    if (FBSession.activeSession.isOpen)
    {
        [FBRequestConnection startWithGraphPath:@"me/friends?fields=id,name,cover"
                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                  if (!error) {
                                      // Sucess! Include your code to handle the results here
                                      //                                                                        NSLog(@"user events: %@", result);
                                      NSMutableArray *arrayEvent=[result valueForKey:@"data"];
                                      
                                      //NSLog(@"resulf: %@", result);
                                      
                                      completeBlock_(arrayEvent);
                                      
                                      
                                  } else {
                                      // An error occurred, we need to handle the error
                                      // See: https://developers.facebook.com/docs/ios/errors
                                      errorFbBlock_(error);
                                  }
                              }];
        
        
    }
    else
    {
        NSArray *permissions = @[@"user_friends"];
        
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState status,
                                                          NSError *error) {
                                          // if login fails for any reason, we alert
                                          if (error) {
                                              UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Đăng nhập thất bại" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                              [alertView show];
                                              errorFbBlock_(error);
                                              // if otherwise we check to see if the session is open, an alternative to
                                              // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                              // property of the session object; the macros are useful, however, for more
                                              // detailed state checking for FBSession objects
                                          } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                              // send our requests if we successfully logged in
                                              
                                              [FBRequestConnection startWithGraphPath:@"me/friends?fields=id,name,cover"
                                                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                                        if (!error) {
                                                                            // Sucess! Include your code to handle the results here
                                                                            //                                                                        NSLog(@"user events: %@", result);
                                                                            //NSLog(@"resulf: %@", result);
                                                                            NSMutableArray *arrayEvent=[result valueForKey:@"data"];
                                                                            completeBlock_(arrayEvent);
                                                                            
                                                                            
                                                                        } else {
                                                                            // An error occurred, we need to handle the error
                                                                            // See: https://developers.facebook.com/docs/ios/errors
                                                                            errorFbBlock_(error);
                                                                        }
                                                                    }];
                                              
                                              
                                          }
                                      }];
    }
    
    
    
}


/*
 RSVP : maybe, attending (going), declined (not going)
 */
-(void)postInviteFriend:(completeFbBlock)completeFbBlock
           errorFbBlock:(errorFbBlock)errorFbBlock
               id_event:(NSString *)idEvent withIdFriends:(NSString *)strIdFriends
{
    
    completeFbBlock_=[completeFbBlock copy];
    errorFbBlock_=[errorFbBlock copy];
    
    ///EVENT_ID/invited?users=USER_ID1,USER_ID2,USER_ID3
    
    NSString * pRequest = [NSString stringWithFormat:@"%@/invited?user=%@",idEvent,strIdFriends];
    
    // Make the Graph API request to post the object
    FBRequest *request = [FBRequest requestForPostWithGraphPath:pRequest
                                                    graphObject:nil];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // Success! Include your code to handle the results here
            NSLog(@"%@",[NSString stringWithFormat:@"result: %@", result]);
            
            completeFbBlock_(result);
            
        } else {
            // An error occurred, we need to handle the error
            // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
            NSLog(@"%@",[NSString stringWithFormat:@"error %@", error.description]);
            errorFbBlock_(error);
        }
    }];
    
}


-(void)updateEvent:(completeFbBlock)completeFbBlock
           errorFbBlock:(errorFbBlock)errorFbBlock
               id_event:(NSString *)idEvent params:(NSMutableDictionary *)params
{
    
    completeFbBlock_=[completeFbBlock copy];
    errorFbBlock_=[errorFbBlock copy];
    
    ///EVENT_ID/invited?users=USER_ID1,USER_ID2,USER_ID3
    
       /* make the API call */
    [FBRequestConnection startWithGraphPath:idEvent
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              /* handle the result */
                              if(error)
                              {
                                  errorFbBlock_(error);
                              }
                              else
                                  completeFbBlock_(result);
                          }];
}


-(void)createOrganizeEvent:(completeFbBlock)completeFbBlock
      errorFbBlock:(errorFbBlock)errorFbBlock
          params:(NSMutableDictionary *)params
{
    
    completeFbBlock_=[completeFbBlock copy];
    errorFbBlock_=[errorFbBlock copy];
    
    [FBRequestConnection startWithGraphPath:@"/me/events"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              /* handle the result */
                              if(!error)
                              {
                                  NSLog(@"success post");
                                  
                                  completeFbBlock_(result);
                              }
                              else
                              {
                                  errorFbBlock_(error);
                              }
                          }];
    
}


/*
///////////////////////////////////////////////////////////////////////////
code function facebook get
 */

-(void)getMovies:(completeBlock)completeFbBlock
    errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
  //    NSString *uid=[defaults valueForKey:@"uid"];
    
    [FBRequestConnection startWithGraphPath:@"me/movies?fields=id,name"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Sucess! Include your code to handle the results here
                                  NSLog(@"result : %@", result);
                                  NSMutableArray *arrayData=[[result objectForKey:@"data"] valueForKey:@"name"];
                                  
                                  NSLog(@"arrayData : %@", arrayData);
                                  
                                  completeBlock_(arrayData);
                                  
                                  
                              } else {
                                  
                                  errorFbBlock_(error);
                              }
                          }];
}

-(void)getMutualFriend:(completeBlock)completeFbBlock
          errorFbBlock:(errorFbBlock)errorFbBlock idFriend:(NSString *)idFriend
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    //    NSString *uid=[defaults valueForKey:@"uid"];
   
    
    
                [FBRequestConnection startWithGraphPath:[NSString stringWithFormat :@"me/mutualfriends/%@",idFriend]
                                      completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                          if (!error) {
                                              // Sucess! Include your code to handle the results here
                                              NSLog(@"result : %@", result);
                                              NSMutableArray *arrayData=[result objectForKey:@"data"] ;
                                              
                                              NSLog(@"arrayData : %@", arrayData);
                                              
                                              completeBlock_(arrayData);
                                              
                                              
                                          } else {
                                              
                                              errorFbBlock_(error);
                                          }
                                      }];
            

}

-(void)getMusic:(completeBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
  
    
    [FBRequestConnection startWithGraphPath:@"me/music?fields=id,name"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Sucess! Include your code to handle the results here
                                  NSLog(@"result : %@", result);
                                  NSMutableArray *arrayData=[[result objectForKey:@"data"] valueForKey:@"name"];
                                  
                                  
                                  completeBlock_(arrayData);
                                  
                                  
                              } else {
                                  
                                  errorFbBlock_(error);
                              }
                          }];
}

-(void)getBooks:(completeBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    
    [FBRequestConnection startWithGraphPath:@"me/books?fields=id,name"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Sucess! Include your code to handle the results here
                                  NSLog(@"result : %@", result);
                                  NSMutableArray *arrayData=[[result objectForKey:@"data"] valueForKey:@"name"];
                                  
                              
                                  
                                  completeBlock_(arrayData);
                                  
                                  
                              } else {
                                  
                                  errorFbBlock_(error);
                              }
                          }];
}

-(void)getLikes:(completeBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
    [FBRequestConnection startWithGraphPath:@"me/likes?&limit=5000"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Sucess! Include your code to handle the results here
                                  NSLog(@"result : %@", result);
                                  NSMutableArray *arrayData=[[result objectForKey:@"data"] valueForKey:@"name"];
                                  
                                  
                                  completeBlock_(arrayData);
                                  
                                  
                              } else {
                                  
                                  errorFbBlock_(error);
                              }
                          }];
}

-(void)getLocation:(completeBlock)completeFbBlock
   errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
   
       [FBRequestConnection startWithGraphPath:@"me/?fields=locations.fields(place)"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Sucess! Include your code to handle the results here
                                  NSLog(@"result : %@", result);
                                  NSMutableArray *arrayData=[[[[result objectForKey:@"locations"]valueForKey:@"data" ]valueForKey:@"place" ]valueForKey:@"name"];
                                  
                                  
                                  completeBlock_(arrayData);
                                  
                                  
                              } else {
                                  
                                  errorFbBlock_(error);
                              }
                          }];
}

-(void)getSport:(completeBlock)completeFbBlock
      errorFbBlock:(errorFbBlock)errorFbBlock
{
    completeBlock_ = [completeFbBlock copy];
    errorFbBlock_ = [errorFbBlock copy];
    
           
    [FBRequestConnection startWithGraphPath:@"me/?fields=locations.fields(place)"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Sucess! Include your code to handle the results here
                                  NSLog(@"result : %@", result);
                                  NSMutableArray *arrayData=[[[[result objectForKey:@"locations"]valueForKey:@"data" ]valueForKey:@"place" ]valueForKey:@"name"];
                                  
                                  
                                  completeBlock_(arrayData);
                                  
                                  
                              } else {
                                  
                                  errorFbBlock_(error);
                              }
                          }];
}

@end
