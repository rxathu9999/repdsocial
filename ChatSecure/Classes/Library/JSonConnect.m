//
//  JSonConnect.m
//  yuppie
//
//  Created by Ho Anh Truc on 7/15/13.
//  Copyright (c) 2013 Egood JSC. All rights reserved.
//

#import "JSonConnect.h"
#import "AFNetworking.h"
#import "SBJsonParser.h"
#import "FunctionHelp.h"

@implementation JSonConnect

- (void)postPath:(NSString *)path
      parameters:(NSMutableDictionary *)params
   completeNetworkBlock:(completeNetworkBlock)completeNetworkBlock
      errorBlock:(errorBlock)errorBlock
{
    completeNetworkBlock_ = [completeNetworkBlock copy];
    errorBlock_ = [errorBlock copy];
    
    
    
    if (params[@"user_id"] != nil)
    {
        [params setValue:hmacForKeyAndData(app_key, [NSString stringWithFormat:@"%@",params[@"user_id"]] ) forKey:@"encrypted_data"];
    }
    else
    {
        [params setValue:hmacForKeyAndData(app_key, @"0") forKey:@"encrypted_data"];
    }
    NSLog(@"params :%@", params);
    NSString *BaseURLString = STR_URL_SERVER;
    NSURL *baseURL = [NSURL URLWithString:BaseURLString];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//    manager.requestSerializer=[AFJSONRequestSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        SBJsonParser *parser=[[SBJsonParser alloc] init];
      
        completeNetworkBlock_([parser objectWithData:responseObject]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        NSLog(@"error :%@", [parser objectWithData:error]);
        errorBlock_([parser objectWithData:error]);
        
    }];
}


- (void)getPath:(NSString *)path
     parameters:(NSDictionary *)params
  completeNetworkBlock:(completeNetworkBlock)completeNetworkBlock
     errorBlock:(errorBlock)errorBlock
{
    completeNetworkBlock_ = [completeNetworkBlock copy];
    errorBlock_ = [errorBlock copy];
    
    NSLog(@"params :%@", params);
    
    NSString *BaseURLString = STR_URL_SERVER;
    NSURL *baseURL = [NSURL URLWithString:BaseURLString];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
//        NSLog(@"responseObject :%@", responseObject);
        completeNetworkBlock_([parser objectWithData:responseObject]);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"error :%@", error);
        errorBlock_(error);
        
    }];
    
}



@end
