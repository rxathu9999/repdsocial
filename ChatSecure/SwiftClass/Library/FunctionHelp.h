//
//  FunctionHelp.h
//  Transport
//
//  Created by buisinam on 10/17/13.
//  Copyright (c) 2013 buisinam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define  app_key  @"HSAA72#@w2zF+?Sc"
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)



UIToolbar *addToolBarToKeyboard(int width,UIToolbar *keyboardToolbar,SEL previousField,SEL nextField,SEL resignKeyboard,id del);
//---------------------------------------
//date: 2013/6/15
//Author:ath
//Function: addToolBarToKeyboard
//Desciption: add toolbar keyboard for textview,
//            button in toolbar: next, pre, done
//---------------------------------------
UIToolbar *addToolBarToKeyboardWithDone(int width,UIToolbar *keyboardToolbar,SEL resignKeyboard,id del);

void changeRearView(UIViewController *currentVC,UIViewController *futureVC);

NSString *convertToSpecialString(NSString *inputString);
BOOL checkFileExist(NSString *filePath) ;
bool NSStringIsValidEmail(NSString *checkString);
NSString *getFilePath(NSString *parament);

int writeToFileDictionary(NSString *fileName,NSMutableDictionary *fileDic);
int writeToFileArray(NSString *fileName,NSMutableArray *fileDic);
UIImage *decodeBase64ToImage(NSString *strEncodeData);
NSString *encodeToBase64String(UIImage *image);
NSString *GetCurrentTimeStamp(NSString *time);
NSString *GetDateFromTimeStamp(NSString *time);
NSString *getAvatarUser();
NSInteger getNumNotifications();
NSInteger getNumBlink();
void setnNumNotifications(NSInteger num);
void setnNumBlink(NSInteger num);
NSString* fuzzyTime(NSString *datetime);
NSString *hmacForKeyAndData(NSString *key, NSString *data);
void previousField(id sender ,NSMutableArray *arrTextField);
void nextField(id sender ,NSMutableArray *arrTextField);