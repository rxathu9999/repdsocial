//
//  FunctionHelp.m
//  Transport
//
//  Created by buisinam on 10/17/13.
//  Copyright (c) 2013 buisinam. All rights reserved.
//

#import "FunctionHelp.h"
#import "NSDate+Utilities.h"
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonHMAC.h>
#pragma mark addToolBarToKeyboard





bool NSStringIsValidEmail(NSString *checkString)
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



NSString *convertToSpecialString(NSString *inputString)
{
    NSString *resull= [NSString
                       stringWithCString:[inputString cStringUsingEncoding:NSUTF8StringEncoding]
                       encoding:NSNonLossyASCIIStringEncoding];
    if(resull!=nil)
        return resull;
    else
        return inputString;
}



NSString *getFilePath(NSString *parament)
{
    NSArray *segments = [NSArray arrayWithObjects:NSHomeDirectory(), @"Documents", nil];
    return [NSString pathWithComponents:segments];
}

BOOL checkFileExist(NSString *filePath) {
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        return TRUE;
    }
    return FALSE;
}
int writeToFileDictionary(NSString *fileName,NSMutableDictionary *fileDic){
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@",docPath,fileName];
    [fileDic writeToFile:file atomically:YES];
    return 1;
}

int writeToFileArray(NSString *fileName,NSMutableArray *fileDic){
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *file = [[NSString alloc] initWithFormat:@"%@/%@",docPath,fileName];
    if([fileDic writeToFile:file atomically:YES])
        return 1;
    else
        return 0;
}

#pragma mark- base 64
NSString *encodeToBase64String(UIImage *image) {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

UIImage *decodeBase64ToImage(NSString *strEncodeData) {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}



NSString *GetCurrentTimeStamp(NSString *time)
{
    
    NSDateFormatter *df=[[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    NSDate *date = [df dateFromString:time];
    
    NSTimeInterval since1970 = [date timeIntervalSince1970]; // January 1st 1970
    double result = since1970 * 1000;
    return [[NSString alloc] initWithFormat:@"%f",result];
    
}

NSString *GetDateFromTimeStamp(NSString *time)
{
    
    NSTimeInterval _interval=[time doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *_date=[_formatter stringFromDate:date];
    return _date;
}
NSString *getAvatarUser()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSDictionary *userDict=[defaults valueForKey:@"userinfor"];
    
    NSArray *arrAvatar=[NSArray arrayWithArray:[userDict valueForKey:@"avatar"]];
    NSString *avatar = @"";
    if ([arrAvatar count]>0)
    {
        avatar=[arrAvatar objectAtIndex:0];
    }
    return avatar;

}

NSInteger getNumNotifications()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return [[defaults valueForKey:@"numnotifications"] integerValue];
}
NSInteger getNumBlink()
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return [[defaults valueForKey:@"numblinks"] integerValue];
}

void setnNumNotifications(NSInteger num)
{
    
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setValue:[[NSString alloc] initWithFormat:@"%ld",(long)num] forKey:@"numnotifications"];
    [defaults synchronize];

}

void setnNumBlink(NSInteger num)
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setValue:[[NSString alloc] initWithFormat:@"%ld",(long)num] forKey:@"numblinks"];
    [defaults synchronize];
    
}

NSString* fuzzyTime(NSString *datetime)
{
    
    NSString *formatted;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    
    
    
    NSTimeZone *gmt = [NSTimeZone localTimeZone];
    [formatter setTimeZone:gmt];
    NSDate *date = [formatter dateFromString:datetime];
    NSDate *today = [NSDate date];
    NSInteger minutes = [today minutesAfterDate:date];
    NSInteger hours = [today hoursAfterDate:date];
    NSInteger days = [today daysAfterDate:date];
    NSString *period;
    if(days >= 365){
        float years = round(days / 365) / 2.0f;
        period = (years > 1) ? @"years" : @"year";
        formatted = [NSString stringWithFormat:@" %f %@ ago", years, period];
    } else if(days < 365 && days >= 30) {
        float months = round(days / 30) / 2.0f;
        period = (months > 1) ? @"months" : @"month";
        formatted = [NSString stringWithFormat:@" %f %@ ago", months, period];
    } else if(days < 30 && days >= 2) {
        period = @"days";
        formatted = [NSString stringWithFormat:@" %li %@ ago", (long)days, period];
    } else if(days == 1){
        period = @"day";
        formatted = [NSString stringWithFormat:@" %li %@ ago", (long)days, period];
    } else if(days < 1 && minutes > 60) {
        period = (hours > 1) ? @"hours" : @"hour";
        formatted = [NSString stringWithFormat:@" %li %@ ago", (long)hours, period];
    } else {
        period = (minutes < 60 && minutes > 1) ? @"minutes" : @"minute";
        formatted = [NSString stringWithFormat:@" %li %@ ago", (long)minutes, period];
        if(minutes < 1){
            formatted = @"a moment ago";
        }
    }
    return formatted;
}

NSString *hmacForKeyAndData(NSString *key, NSString *data)
{
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256,  cKey, strlen(cKey),cData, strlen(cData), cHMAC);
    
    
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH  /2];
    
    
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", cHMAC[i]];
        
        
    }
    
    
    return output;
    
}

void previousField(id sender ,NSMutableArray *arrTextField)
{
    NSInteger firstTxt=0;
    for (NSInteger i=arrTextField.count; i>0; i--) {
        if ([[arrTextField objectAtIndex:i-1] isKindOfClass:[UITextField class]]) {
            if (firstTxt==0)
                firstTxt=i-1;
            if( [[arrTextField objectAtIndex:i-1] isFirstResponder])
            {
                for (NSInteger j=[arrTextField count]; j>=0; j--) {
                    if(j==0)
                    {
                        [[arrTextField objectAtIndex:arrTextField.count-1] becomeFirstResponder];
                        return;
                    }
                    else if ([[arrTextField objectAtIndex:j-1] isKindOfClass:[UITextField class]]) {
                        if (j<i) {
                            [[arrTextField objectAtIndex:j-1] becomeFirstResponder];
                            return;
                        }
                    }
                    
                }
            }
        }
    }
}

void nextField(id sender ,NSMutableArray *arrTextField)
{
    
    int firstTxt=0;
    for (int i=0; i<[arrTextField count]; i++) {
        if ([[arrTextField objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            if (firstTxt==0)
                firstTxt=i;
            if( [[arrTextField objectAtIndex:i] isFirstResponder])
            {
                for (int j=0; j<=[arrTextField count]; j++) {
                    if(j==[arrTextField count])
                    {
                        [[arrTextField objectAtIndex:0] becomeFirstResponder];
                        return ;
                    }
                    else if ([[arrTextField objectAtIndex:j] isKindOfClass:[UITextField class]]) {
                        if (j>i) {
                            [[arrTextField objectAtIndex:j] becomeFirstResponder];
                            return ;
                        }
                        
                    }
                    
                }
            }
        }
    }
}



