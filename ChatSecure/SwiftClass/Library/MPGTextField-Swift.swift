//
//  MPGTextField-Swift.swift
//  MPGTextField-Swift
//
//  Created by Gaurav Wadhwani on 08/06/14.
//  Copyright (c) 2014 Mappgic. All rights reserved.
//

import UIKit

@objc protocol MPGTextFieldDelegate{
    func dataForPopoverInTextField(textfield: MPGTextField_Swift) -> [Dictionary<String, AnyObject>]

    optional func textFieldDidEndEditing(textField: MPGTextField_Swift, withSelection data: Dictionary<String,AnyObject>)
    optional func textFieldShouldSelect(textField: MPGTextField_Swift) -> Bool
}

class MPGTextField_Swift: UITextField, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    var mDelegate : MPGTextFieldDelegate?
    var tableViewController : UITableViewController?
    var data = [Dictionary<String, AnyObject>]()
    
    var resultData = [Dictionary<String, AnyObject>]()
    
    var selectedData : Dictionary<String, AnyObject>!
    
    //Set this to override the default color of suggestions popover. The default color is [UIColor colorWithWhite:0.8 alpha:0.9]
    @IBInspectable var popoverBackgroundColor : UIColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    
    //Set this to override the default frame of the suggestions popover that will contain the suggestions pertaining to the search query. The default frame will be of the same width as textfield, of height 200px and be just below the textfield.
    @IBInspectable var popoverSize : CGRect?
    
    //Set this to override the default seperator color for tableView in search results. The default color is light gray.
    @IBInspectable var seperatorColor : UIColor = UIColor.clearColor()
    @IBInspectable var showInTable : UITableView!
    
    var someTimer  :  NSTimer? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initialization code
        
    }
    
    required init(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
    

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if someTimer == nil
        {
            someTimer = NSTimer.scheduledTimerWithTimeInterval(0.0, target: self, selector: nil, userInfo: nil, repeats: false)
        }
        
        
        if someTimer!.valid
        {
            someTimer!.invalidate()
        }
        
        someTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("updateData"), userInfo: nil, repeats: true)
        
        
        return true
    }
    
    override func layoutSubviews(){
        super.layoutSubviews()
        self.delegate = self
        self.textColor = UIColor.whiteColor()
//
    }
    
    func updateData()
    {
        if  count(self.text) == 0
        {
            return
        }
        
        println("fucking update data")
        someTimer?.invalidate()
        let str : String = self.text
        
        if (count(str) > 0) && (self.isFirstResponder())
        {
            if (mDelegate != nil){
                data = mDelegate!.dataForPopoverInTextField(self)
                //                dispatch_async(dispatch_get_main_queue()) {
                if count(self.text) > 0{
                    self.provideSuggestions()
                }
                //                }
            }
            else{
                println("<MPGTextField> WARNING: You have not implemented the requred methods of the MPGTextField protocol.")
            }
        }
        else{
            if let table = self.tableViewController{
                if table.tableView.superview != nil{
                    table.tableView.removeFromSuperview()
                    self.tableViewController = nil
                }
            }
        }

    }
    
    override func resignFirstResponder() -> Bool{
        UIView.animateWithDuration(0.3,
            animations: ({
                if self.tableViewController != nil
                {
                    self.tableViewController!.tableView.alpha = 0.0
                }
            }),
            completion:{
                    (finished : Bool) in
                if self.tableViewController != nil
                {

                    self.tableViewController!.tableView.removeFromSuperview()
                    self.tableViewController = nil
                }
                })
        self.handleExit()
        return super.resignFirstResponder()
    }
    
    func provideSuggestions(){
        if let tvc = self.tableViewController {
            
            self.applyFilterWithSearchQuery(self.text)
             self.tableViewController!.tableView.reloadData()
            Async.background {
                println("This is run on the background queue")
                
                }.main {
                    println("This is run on the main queue, after the previous block")
                    
            }
            
           
        }
        else if self.applyFilterWithSearchQuery(self.text).count > 0{
            //Add a tap gesture recogniser to dismiss the suggestions view when the user taps outside the suggestions view
            let tapRecognizer = UITapGestureRecognizer(target: self, action: "tapped:")
            tapRecognizer.numberOfTapsRequired = 1
            tapRecognizer.cancelsTouchesInView = false
            tapRecognizer.delegate = self
            self.superview!.addGestureRecognizer(tapRecognizer)

            self.tableViewController = UITableViewController.alloc()
            self.tableViewController!.tableView.delegate = self
            self.tableViewController!.tableView.dataSource = self
            self.tableViewController!.tableView.backgroundColor = self.popoverBackgroundColor
            self.tableViewController!.tableView.separatorColor = self.seperatorColor
            if let frameSize = self.popoverSize{
                self.tableViewController!.tableView.frame = frameSize
            }
            else{
                //PopoverSize frame has not been set. Use default parameters instead.
                var frameForPresentation = self.frame
                frameForPresentation.origin.y += self.frame.size.height
                frameForPresentation.size.height = 200
                self.tableViewController!.tableView.frame = frameForPresentation
            }
            
            
            
            
            if (showInTable != nil)
            {
                var frameForPresentation = self.frame
                
                var f = self.convertRect(self.frame, toView: showInTable)
                
                var tbContentSize = showInTable.frame.size.height
                frameForPresentation.origin.y = f.origin.y
//                if tbContentSize - f.origin.y  > 100
//                {
//                    frameForPresentation.origin.y = f.origin.y
//                }
//                else
//                {
//                    frameForPresentation.origin.y = f.origin.y - 220
//                }

                frameForPresentation.size.height = 200;
                tableViewController!.tableView.frame = frameForPresentation
                showInTable.addSubview(tableViewController!.tableView)
            }
            else
            {
                var frameForPresentation = self.frame
                frameForPresentation.origin.y += self.frame.size.height;
                frameForPresentation.size.height = 200;
                tableViewController!.tableView.frame = frameForPresentation
                self.superview!.addSubview(tableViewController!.tableView)
            }
            self.tableViewController!.tableView.alpha = 0.0
            UIView.animateWithDuration(0.3,
                animations: ({
                    self.tableViewController!.tableView.alpha = 1.0
                    }),
                completion:{
                    (finished : Bool) in
                    
                })
        }
        
    }
    
    func tapped (sender : UIGestureRecognizer!){
        if let table = self.tableViewController{
            if !CGRectContainsPoint(table.tableView.frame, sender.locationInView(self.superview)) && self.isFirstResponder(){
                self.resignFirstResponder()
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var count = resultData.count//self.applyFilterWithSearchQuery(self.text).count
        if count == 0{
            UIView.animateWithDuration(0.3,
                animations: ({
                    self.tableViewController!.tableView.alpha = 0.0
                    }),
                completion:{
                    (finished : Bool) in
                    if let table = self.tableViewController{
                        table.tableView.removeFromSuperview()
                        self.tableViewController = nil
                    }
                })
        }
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("MPGResultsCell") as? UITableViewCell
        
        if !(cell != nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "MPGResultsCell")
        }

        cell!.backgroundColor = UIColor.clearColor()
         dispatch_async(dispatch_get_main_queue()) {
            let dataForRowAtIndexPath = self.resultData[indexPath.row]//self.applyFilterWithSearchQuery(self.text)[indexPath.row]
            let FIELD4 : AnyObject? = dataForRowAtIndexPath["FIELD4"]
            let displaySubText : AnyObject? = dataForRowAtIndexPath["FIELD5"]
            cell!.textLabel!.text = FIELD4 as? String
            cell!.textLabel!.textColor = UIColor.lightGrayColor()
            cell!.detailTextLabel!.text = displaySubText as? String
        }
        
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        selectedData = self.resultData[indexPath.row]
        self.text = selectedData["FIELD4"] as! String
        self.resignFirstResponder()

    }
    
    
//   #pragma mark Filter Method
    
    func applyFilterWithSearchQuery(filter : String) -> [Dictionary<String, AnyObject>]
    {
        //let predicate = NSPredicate(format: "FIELD4 BEGINSWITH[cd] \(filter)")
        
        
//        var lower = (filter as NSString).lowercaseString
//        let resultPredicate1 = NSPredicate(format: "FIELD4 contains[c] %@", filter)
//        var arrSearch = NSArray(array: data)
//        
//        var result = arrSearch.filteredArrayUsingPredicate(resultPredicate1)
        var filteredData = data.filter({
                if let match : AnyObject  = $0["FIELD4"],  match1 : AnyObject  = $0["FIELD5"]{
                    //println("LCS = \(filter.lowercaseString)")
                    
                    var result = (match as! NSString).lowercaseString.rangeOfString((filter as NSString).lowercaseString)
                    
                    if result != nil
                    {
                        
                        return true
                    }
                    else
                    {
                        var result1 = (match1 as! NSString).lowercaseString.rangeOfString((filter as NSString).lowercaseString)
                        if result1 != nil
                        {
                            
                            return true
                        }
                    }
                    
                    return false//(match as! NSString).lowercaseString.hasPrefix((filter as NSString).lowercaseString)
                }
                else{
                    return false
                }
            })
        
       self.resultData = filteredData as [Dictionary<String, AnyObject>]
        return filteredData
    }
    
    func handleExit(){
        if let table = self.tableViewController{
            self.textColor = UIColor.whiteColor()
            table.tableView.removeFromSuperview()
        }
        if (mDelegate?.textFieldShouldSelect?(self) != nil){
            if self.applyFilterWithSearchQuery(self.text).count > 0 {
                
                if selectedData != nil{
                
                    let FIELD4 : AnyObject? = selectedData["FIELD4"]
                    self.text = FIELD4 as! String
                    self.textColor = UIColor.whiteColor()
                    mDelegate?.textFieldDidEndEditing?(self, withSelection: selectedData)
                }
                else
                {
                    self.textColor = UIColor.whiteColor()
                    mDelegate?.textFieldDidEndEditing?(self, withSelection: ["FIELD4":self.text, "CustomObject":"NEW"])
                }
            }
            else{
                self.textColor = UIColor.whiteColor()
                mDelegate?.textFieldDidEndEditing?(self, withSelection: ["FIELD4":self.text, "CustomObject":"NEW"])
            }
        }

    }
    
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: -25);
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    private func newBounds(bounds: CGRect) -> CGRect {
        
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= (padding.top * 2) - padding.bottom
        newBounds.size.width -= (padding.left * 2) - padding.right
        return newBounds
    }

}
