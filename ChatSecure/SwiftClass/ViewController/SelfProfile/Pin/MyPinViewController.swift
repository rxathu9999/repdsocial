//
//  MyPinViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/15/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class MyPinViewController: FactoryViewController,UITableViewDelegate,UITableViewDataSource {
    
    var backgroundNav : UIImage?
    var arrImg = []
    var arrName = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tableRoot.delegate = self
       
        
        self.tableRoot.registerNib(UINib(nibName: "PinTableViewCell", bundle: nil), forCellReuseIdentifier: "PinTableViewCell")

        arrName = ["tttttttmiles/kms travelled","Crossfit Los Angetes Santa \n Monica Blvd.","Rebook Crossfit SSC \n Chuo-Ku, Sapporo, Hokkaido","Crossfit Los Angetes Santa \n Monica Blvd."]
        arrImg = ["repd_like","repd_home","repd_enter","repd_stick"]
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        self.navigationController?.navigationBar.setBackgroundImage(backgroundNav, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = backgroundNav
        self.navigationController?.navigationBar.translucent = false
        
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        
        backgroundNav = self.navigationController?.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController?.title = "Pin"
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrImg.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "PinTableViewCell"
        var cell: PinTableViewCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? PinTableViewCell
        cell!.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI))
        cell.lblName.text = arrName[indexPath.row] as? String
        cell.imgAvatar.image = UIImage(named: (arrImg[indexPath.row] as? String)!)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    
    
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
