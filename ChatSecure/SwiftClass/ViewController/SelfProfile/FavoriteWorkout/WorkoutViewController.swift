//
//  WorkoutViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/17/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class WorkoutViewController: FactoryViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableRoot.delegate = self
        self.tableRoot.dataSource = self
        tableRoot!.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI));
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        cell.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI))
        cell.textLabel!.text =  "Fran"
        return cell
        
        
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 60
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("headerCell") as! HeaderWorkoutViewCell
        cell.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI))
        cell.lblName.text = "Top named wode"
        cell.btnAdd.tag = section
        cell.btnAdd.addTarget(self, action: "addWorkout:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell
    }
    
    func addWorkout(sender : UIButton)
    {
        let workoutVC = Utilities.viewController("PostWorkoutViewController", onStoryboard: "SelfProfile")
        self.navigationController?.pushViewController(workoutVC, animated: true)
        
    }


}
