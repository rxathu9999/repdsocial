//
//  BlurFollowTableViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/16/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class BlurFollowTableViewController: UITableViewController {

    
    var  page = 1
    var arrUserFollow = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
         tableView.registerNib(UINib(nibName: "BlurFollowTableViewCell", bundle: nil), forCellReuseIdentifier: "BlurFollowTableViewCell")
        
        self.loadFollower()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Follow"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
       

    }
    
    
    func loadFollower()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,
            "page" : String(page)]
        
        json.postPath("user/getfollowinglist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            self.arrUserFollow = result["user"] as! NSArray

            self.tableView.reloadData()
            
            }) { (error) -> Void in
                //                Utilities.showErrorStatus(error)
                //                self.heightFollowView.constant = 0
                //                self.view.updateConstraintsIfNeeded()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrUserFollow.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BlurFollowTableViewCell", forIndexPath: indexPath) as! BlurFollowTableViewCell

        // Configure the cell...
        
        
        
        var useDict = self.arrUserFollow[indexPath.row] as! NSDictionary
        cell.btnUnfollow.tag = indexPath.row
        cell.lblName.text = useDict["name"] as! String!
        var imgStr = useDict["user_image"] as! String!
        cell.imgAvatar.layer.masksToBounds = true
        cell.imgAvatar.layer.cornerRadius  = cell.imgAvatar.frame.size.height / 2
        cell.imgAvatar.sd_setImageWithURL(NSURL(string: "http://www.mantality.co.za/blog/wp-content/uploads/2014/03/sexy-girl-at-bar-counter1.jpg") , placeholderImage: nil) { (image, errorBlock, cacheType, url) -> Void in
            
            var color  = UIColor(red: 89/255, green: 83/255, blue: 101/255, alpha: 0.4)
            
            var newimage = image.applyBlurWithRadius(8, tintColor: color, saturationDeltaFactor: 1.0, maskImage: nil)
            
            
            cell.imgMain.image = newimage
        }
        

        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return  80;
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
