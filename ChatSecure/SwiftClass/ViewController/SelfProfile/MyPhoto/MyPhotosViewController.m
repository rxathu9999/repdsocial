//
//  MyPhotosViewController.m
//  ChatSecure
//
//  Created by buisinam on 7/16/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import "MyPhotosViewController.h"
#import "FMMosaicCellView.h"
#import "FMMosaicLayout.h"
#import "FMHeaderView.h"
#import "FMFooterView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
static const CGFloat kFMHeaderFooterHeight  = 44.0;
static const NSInteger kFMMosaicColumnCount = 2;



@interface MyPhotosViewController ()<FMMosaicLayoutDelegate>

@end

@implementation MyPhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.collectionView registerNib:[UINib nibWithNibName:@"FMHeaderView" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                 withReuseIdentifier:[FMHeaderView reuseIdentifier]];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"FMFooterView" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                 withReuseIdentifier:[FMFooterView reuseIdentifier]];
    
    [self adjustContentInsets];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)adjustContentInsets {
    UIEdgeInsets insets = UIEdgeInsetsMake([UIApplication sharedApplication].statusBarFrame.size.height, 0, 0, 0);
    self.collectionView.contentInset = insets;
    self.collectionView.scrollIndicatorInsets = insets;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 31;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FMMosaicCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[FMMosaicCellView reuseIdentifier] forIndexPath:indexPath];
    
    // Configure the cell
    cell.titleLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.item + 1];
    cell.imageView.layer.masksToBounds = YES;
    cell.imageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    cell.imageView.layer.borderWidth = 2;
    
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://yolo.jp/upload/2015071503618/6683.jpg"]];//.image = self.stockImages[indexPath.item % self.stockImages.count];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        FMHeaderView *headerView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                           withReuseIdentifier:[FMHeaderView reuseIdentifier] forIndexPath:indexPath];
        
        headerView.titleLabel.text = [NSString stringWithFormat:@"SECTION %ld", (long)indexPath.section + 1];
        reusableView = headerView;
        
    } else if([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        FMFooterView *footerView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                           withReuseIdentifier:[FMFooterView reuseIdentifier] forIndexPath:indexPath];
        
        NSInteger assetCount = [self collectionView:self.collectionView numberOfItemsInSection:indexPath.section];
        footerView.titleLabel.text = assetCount == 1 ? @"1 ASSET" : [NSString stringWithFormat:@"%ld ASSETS", (long)assetCount];
        reusableView = footerView;
    }
    
    return reusableView;
}

#pragma mark <FMMosaicLayoutDelegate>

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
   numberOfColumnsInSection:(NSInteger)section {
    return kFMMosaicColumnCount;
}

- (FMMosaicCellSize)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
  mosaicCellSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.item % 12 == 0) ? FMMosaicCellSizeBig : FMMosaicCellSizeSmall;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
interitemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
 heightForHeaderInSection:(NSInteger)section {
    return 240;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
 heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (BOOL)headerShouldOverlayContentInCollectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout {
    return NO;
}

- (BOOL)footerShouldOverlayContentInCollectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout {
    return NO;
}

#pragma mark - Accessors



@end
