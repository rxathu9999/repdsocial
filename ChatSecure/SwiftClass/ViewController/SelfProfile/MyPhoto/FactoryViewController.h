//
//  MyPhotosViewController.h
//  ChatSecure
//
//  Created by buisinam on 7/15/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FactoryViewController : UIViewController
{
    __weak IBOutlet UIImageView *imgAvatar;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UILabel *lblAddress;
    __weak IBOutlet UIImageView *imgMain;

    
}

@property (strong, nonatomic) IBOutlet UITableView *tableRoot;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
