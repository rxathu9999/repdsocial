//
//  MyPhotosViewController.m
//  ChatSecure
//
//  Created by buisinam on 7/15/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import "FactoryViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"

@interface FactoryViewController ()
{
    UIImage *imageNav;
}
-(void)loadAllView;
@property (nonatomic, strong) NSArray *stockImages;

@end


@implementation FactoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadAllView];
    
    
}



-(void)viewDidAppear:(BOOL)animated
{
    imgAvatar.layer.masksToBounds = YES;
    imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height / 2;
    
    imgAvatar.layer.borderWidth = 0.0f;
  
    UIColor *myColor2  = [UIColor colorWithRed:(108.0 / 255.0) green:(96.0 / 255.0) blue:(121.0 / 255.0) alpha: 1];
    CGPoint point      = imgAvatar.frame.origin;
    CGFloat width      = imgAvatar.frame.size.width + 11;
    CGFloat height     = imgAvatar.frame.size.height +11;
    UIView *holderView = [[UIView alloc] initWithFrame:CGRectMake(point.x - 5.5, point.y - 5.5, width, height)];
    
    holderView.layer.borderWidth  = 2.0;
    holderView.layer.cornerRadius = holderView.frame.size.height / 2;

    holderView.layer.borderColor  = myColor2.CGColor;

    [self.view addSubview:holderView];
    [self.view bringSubviewToFront:self.collectionView];
}

-(void)viewWillAppear:(BOOL)animated
{

    
    imageNav = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    
//    self.navigationItem.title = @"Photos";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
      
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setBackgroundImage:imageNav forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = imageNav;
    self.navigationController.navigationBar.translucent = YES;
}




#pragma mark - Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadAllView
{
    _tableRoot.transform = CGAffineTransformMakeRotation(-M_PI);
    
    
       
    
    
    
    [imgAvatar sd_setImageWithURL:[NSURL URLWithString:@"http://www.ifreewallpaper.com/wallpapers_download/mobile/960_854_wallpapers/20110205/B/B_sexygirl_fdp86m38.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        image = [image applyBlurWithRadius:5 tintColor:[UIColor colorWithRed:89/255 green:83/255 blue:101/255 alpha:0.4] saturationDeltaFactor:1.0 maskImage:nil];
        
        
        
        [imgMain setImage:image];
    }];
}

@end
