//
//  MyPhotosViewController.h
//  ChatSecure
//
//  Created by buisinam on 7/16/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FactoryViewController.h"

@interface MyPhotosViewController : FactoryViewController

@end
