//
//  MyPhotosViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/15/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class MyPhotosViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableRoot: UITableView!
    
    var  _cellSizes : NSMutableArray!
    var  arrGallery : NSMutableArray = []
    
    let CELL_IDENTIFIER = "photoCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.collectionView.dataSource  = self
        self.collectionView.delegate = self

        
        //Layout setup
        setupCollectionView()
        
        //Register nibs
        registerNibs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    //MARK: - CollectionView UI Setup
    func setupCollectionView(){
        
        // Create a waterfall layout
        var layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 3
        layout.minimumInteritemSpacing = 3
        layout.columnCount = 4
        // Collection view attributes

//        [self.collectImage registerClass:[CHTCollectionViewWaterfallCell class]
//            forCellWithReuseIdentifier:CELL_IDENTIFIER];
        // Add the waterfall layout to your collection view
        self.collectionView.collectionViewLayout = layout
    }
    
    // Register CollectionView Nibs
    func registerNibs(){
        
        collectionView.registerClass(CHTCollectionViewWaterfallCell.self, forCellWithReuseIdentifier: CELL_IDENTIFIER)
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4;
    }
    
    
    //** Create a basic CollectionView Cell */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // Create the cell and return the cell
        
        var cell = collectionView .dequeueReusableCellWithReuseIdentifier(CELL_IDENTIFIER, forIndexPath: indexPath) as! CHTCollectionViewWaterfallCell
    
       cell.loadImage(NSURL(string: "https://s-media-cache-ak0.pinimg.com/originals/3e/c2/62/3ec2624aa57d2ed7a8c7fe05ea2c372e.jpg"), withSize: (self._cellSizes[indexPath.item] as! NSValue).CGSizeValue())
        
        
        // Add image to cell
     
        return cell
    }
    
    
    //MARK: - CollectionView Waterfall Layout Delegate Methods (Required)
    
    //** Size for the cells in the Waterfall Layout */
    func collectionView(collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        
        // create a cell size from the image size, and return the size
        var size : CGSize!
        
        
        return (self.cellSizes()[indexPath.item] as! NSValue).CGSizeValue()
    }
    
    func cellSizes() -> NSMutableArray
    {
        if (_cellSizes == nil)
        {
            _cellSizes = NSMutableArray()
            var size : CGSize!
            for  i in  0...19 {//self.arrGallery.count - 1 {
                if i % 2 == 0
                {
                    size = CGSizeMake(self.collectionView.width / 2, self.collectionView.width / 2)
                }
                else
                {
                    size = CGSizeMake(self.collectionView.width / 4, self.collectionView.width / 4)
                }
                _cellSizes[i] = NSValue(CGSize: size)
            }
        }
        return _cellSizes
        
        

    }

}
