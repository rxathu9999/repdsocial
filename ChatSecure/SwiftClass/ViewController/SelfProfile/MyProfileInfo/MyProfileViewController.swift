//
//  MyProfileViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/17/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class MyProfileViewController: FactoryViewController,UITableViewDelegate,UITableViewDataSource {
    
    var isEditting = false
    
    var editButton : UIBarButtonItem!
    var doneButton : UIBarButtonItem!
    
    @IBOutlet weak var btnSnapshot: UIButton!
    var  params = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableRoot.delegate = self
        self.tableRoot.dataSource = self
        
        editButton = UIBarButtonItem(image: UIImage(named: "icon_editprofile"), style: .Plain, target: self, action: "editProfile")
        doneButton = UIBarButtonItem(image: UIImage(named: "icon_stick"), style: .Plain, target: self, action: "doneEditting")
        self.navigationItem.rightBarButtonItem = editButton
       
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func editProfile()
    {
        self.navigationItem.rightBarButtonItem = doneButton
        self.view.bringSubviewToFront(btnSnapshot)
        btnSnapshot.hidden = false
    }
    
    func doneEditting()
    {
        self.navigationItem.rightBarButtonItem = editButton
        btnSnapshot.hidden = true
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 3
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("EditProfileViewCell", forIndexPath: indexPath) as! EditProfileViewCell
         cell.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI));
        if  isEditting {
            cell.txtDescription.enabled = true
            
        }
        else
        {
            cell.txtDescription.enabled = false
        }

        if  indexPath.row  == 0
        {
            cell.imgAvatar.image = UIImage(named: "icon_dob")
            cell.lblName.text  = "Date of Birth"
            cell.txtDescription.text = "02/28/1990"
        }
        else  if indexPath.row  == 1
        {
            cell.imgAvatar.image = UIImage(named: "icon_gender")
            cell.lblName.text  = "Gender"
            cell.txtDescription.text = "FEMALE"
        }
        else
        {
            cell.imgAvatar.image = UIImage(named: "icon_follwing")
            cell.lblName.text  = "Following"
            cell.txtDescription.text = "108 FOLLOWING"
            cell.txtDescription.enabled = false
        }
        
        return cell
        
        
    }
        
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    @IBAction func clickSNap(sender: AnyObject) {
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
