//
//  ProfilesViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/8/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class ProfilesViewController: UITableViewController {

    @IBOutlet weak var heightSettingPanel: NSLayoutConstraint!
    
    @IBOutlet weak var spacingBottom: NSLayoutConstraint!
    @IBOutlet weak var viewSetting: UIView!
    var backgroundNav : UIImage!
    
    @IBOutlet weak var headerView: UIView!
    var page = 1
    var arrPost = []
    internal var  view_user_id = 0
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblFollower: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_help"), style: .Plain, target: self, action: Selector("gotoHelp"))
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationItem.title = "Help"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]

        self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_login1"))
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.registerNib(UINib(nibName: "PostViewCell", bundle: nil), forCellReuseIdentifier: "nibCell")
        self.loadListPost()
        self.loadProfiles()
    }
    
    
    
    func gotoHelp()
    {
//        var helpVC = Utilities.viewTableController("helpVC", onStoryboard: "Help")
//        self.navigationController?.pushViewController(helpVC, animated: true)
        
//        heightSettingPanel.constant = heightSettingPanel.constant == 0 ? 179 : 0
//        
//        spacingBottom.constant = heightSettingPanel.constant == 0 ? 179 : 0
//                println(self.viewSetting.hidden)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            if self.viewSetting.hidden == false
            {
                self.headerView.height -= 179
            }
            else
            {
                self.headerView.height += 179
            }

            self.viewSetting.hidden = !self.viewSetting.hidden
            
            
            self.view.layoutIfNeeded()

        }) { (bool) -> Void in
            UIView.transitionWithView(self.tableView,
                duration:0.5,
                options:.TransitionCrossDissolve,
                animations:
                { () -> Void in
                    self.tableView.reloadData()
                },
                completion: nil);
        }
       
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        self.navigationController?.navigationBar.setBackgroundImage(backgroundNav, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = backgroundNav
        self.navigationController?.navigationBar.translucent = false
        
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        
        backgroundNav = self.navigationController?.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)

        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        if self.imgAvatar.hidden == true
        {
            
            self.imgAvatar.hidden = false
            self.imgAvatar.layer.masksToBounds = true
            self.imgAvatar.layer.cornerRadius = self.imgAvatar.height / 2
            self.imgAvatar.image = UIImage(named: "placeholder_NonImage")
        }

    }
    func generateData(homeboxid : String){
        var err : NSErrorPointer?
        var dataPath = NSBundle.mainBundle().pathForResource("convertcsv", ofType: "json")
        var data : NSData!
        var contents : JSON!
        
        var IndexAddress = 0
        
        data = NSData(contentsOfFile: dataPath!)
        
        
        contents = JSON(data:data)
        var arrHomeBox = contents.arrayObject as! NSArray
        
        let resultPredicate1 = NSPredicate(format: "FIELD1 = %@", homeboxid)
        var result = arrHomeBox.filteredArrayUsingPredicate(resultPredicate1)
        
        if result.count > 0
        {
        
            self.lblAddress.text = (result[0] as! NSDictionary)["FIELD4"] as? String
        }
        
    }
    
    func  loadProfiles()
    {
        if  view_user_id == 0{
            lblName.text = User.sharedInstance.currentUser.last_name
        }
        
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id, "page" : String(page),
            "get_own_post" : "y",
            "view_user_id" :  view_user_id == 0 ? User.sharedInstance.currentUser.user_id : view_user_id
        ]
        
        
        
        json.postPath("user/getuserprofile", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
             let  user = result["User"] as! NSDictionary!
            self.imgAvatar.sd_setImageWithURL(NSURL(string: user["profile_image"] as! String))
            self.lblFollower.text = String(user["followers_count"] as! Int)
             self.lblFollowing.text = String(user["following_count"] as! Int)
//            self.generateData(String(user["home_box_id"] as! Int))
            }) { (error) -> Void in
                
                
        }

    }
    
    func loadListPost()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id, "page" : String(page), "get_own_post" : "y"]
        
        
        
        json.postPath("post/postlist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            self.arrPost = result["post"] as! NSArray
            self.tableView.reloadData()
            
            }) { (error) -> Void in
                
        }

    }
    
//    override func scrollViewDidScroll(scrollView: UIScrollView) {
//        
//        for cell in tableView.visibleCells() as! [UITableViewCell] {
//            
//            var point = tableView.convertPoint(cell.center, toView: tableView.superview)
//            cell.alpha = ((point.y * 100) / tableView.bounds.maxY) / 100
//        }
//    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrPost.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            var cell = tableView.dequeueReusableCellWithIdentifier("postCell",forIndexPath: indexPath) as? PostViewCell
            
            var postObj = PostObj()
            postObj.setValuesForKeysWithDictionary(self.arrPost[indexPath.row] as! [NSObject : AnyObject])
            // Configure the cell...
            
            cell!.btnComment.tag = indexPath.row
            cell!.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
            cell!.lblName.text = postObj.owner
            cell!.lblStatus.text = postObj.caption
            cell!.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",postObj.inserted_date)))
            cell!.lblNumberLike.text = String(format: "%d like",postObj.post_likes)
            cell!.imgAvatar.layer.masksToBounds = true
            cell!.imgAvatar.layer.cornerRadius  = cell!.imgAvatar.frame.size.height / 2
            cell!.imgAvatar.sd_setImageWithURL(NSURL(string: postObj.owner_image), placeholderImage: UIImage.new())
            if postObj.images.count ==  0
            {
                cell!.heightCollect.constant = 0
                cell!.heightImgMain.constant = 0
                cell!.updateConstraintsIfNeeded()
            }else
            {
                
                let arrGallery = NSMutableArray(array: postObj.images )
                if arrGallery.count > 1
                {
                    cell?.imgMain.image = nil
                    cell!.setup()
                    cell!.arrGallery = NSMutableArray(array: postObj.images )
                    
                    if cell!.arrGallery.count >= 4
                    {
                        var witdth = cell!.imgMain.width
                        cell!.heightCollect.constant = cell!.imgMain.width
                        cell!.heightImgMain.constant = cell!.imgMain.width
                        cell!.updateConstraints()
                        cell!.collectImage.reloadData()
                        return cell!
                        
                    }
                    else
                    {
                        cell!.heightCollect.constant = 195
                        cell!.heightImgMain.constant = 195
                        
                        cell!.updateConstraints()
                        cell!.collectImage.reloadData()
                        return cell!
                        
                    }
                    
                    
                }
                    
                    
                else
                {
                    
                    var imgobject: AnyObject = arrGallery[0]
                    var url = NSURL(string: (imgobject["image_original"] as? String!)!)
                    
                    var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
                    if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
                    {
                        let imageAspectRatio = image.size.width / cell!.imgMain.frame.size.width
                        cell!.heightImgMain.constant = image.size.height / imageAspectRatio
                        cell!.heightCollect.constant = cell!.heightImgMain.constant
                        //                        cell!.heightImgMain.constant = cell!.heightImgMain.constant
                        cell!.imgMain.image = image
                        cell!.updateConstraints()
                        return cell!
                        
                    }
                    else
                    {
                        weak var wself  = cell
                        cell!.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
                            
                            if (image != nil)
                            {
                                var strongself : PostViewCell = wself!
                                
                                strongself.updateConstraintsIfNeeded()
                                
                            }
                            
                        })
                        return cell!
                        
                    }
                }
            }
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("nibCell",forIndexPath: indexPath) as? PostViewCell
            var postObj = PostObj()
            postObj.setValuesForKeysWithDictionary(self.arrPost[indexPath.row] as! [NSObject : AnyObject])
            
            cell!.btnComment.tag = indexPath.row
            cell!.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
            cell!.lblName.text = postObj.owner
            
            cell!.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",postObj.inserted_date)))
            cell!.lblNumberLike.text = String(format: "%d like",postObj.post_likes)
            cell!.imgAvatar.layer.masksToBounds = true
            cell!.imgAvatar.layer.cornerRadius  = cell!.imgAvatar.frame.size.height / 2
            cell!.imgAvatar.sd_setImageWithURL(NSURL(string: postObj.owner_image), placeholderImage: UIImage.new())
            
            cell!.lblStatus.text = postObj.caption
            cell!.imgMain.hidden = false
            cell!.collectImage.hidden = false
            cell!.setup()
            if postObj.images.count ==  0
            {
                cell!.imgMain.hidden = true
                cell!.collectImage.hidden = true
                cell!.imgMain.height = 0
                cell!.collectImage.height = 0
                cell!.collectImage.reloadData()
                cell!.reloadCell()
                return cell!
                
            }
            else
            {
                
                
                if  postObj.images.count == 1
                {
                    cell!.imgMain.hidden = false
                    cell!.collectImage.hidden = true
                    
                    var imgobject: AnyObject = postObj.images[0]
                    var url = NSURL(string: (imgobject["image_original"] as? String!)!)
                    
                    var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
                    if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
                    {
                        var imageAspectRatio = image.size.width / cell!.imgMain.width
                        cell!.imgMain.height = image.size.height / imageAspectRatio
                        cell!.imgMain.image = image
                        
                        cell!.reloadCell()
                        return cell!
                    }
                    else
                    {
                        
                        
                        var wself  = cell
                        cell!.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
                            if (image != nil)
                            {
                                
                                
                                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                                
                                
                            }
                            
                            
                        })
                    }
                    
                }
                else
                {
                    cell!.imgMain.hidden = true
                    cell!.collectImage.hidden = false
                    
                    
                    cell!.collectImage.height = cell!.bg_view.width
                    println(cell!.collectImage.height)
                    cell!.arrGallery = (NSMutableArray(array: postObj.images))
                    cell!.collectImage.reloadData()
                    cell!.reloadCell()
                    return cell!
                }
            }
            
            
            
            
            
            return cell!
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        /*If you just want to support ios8+,
        
        return UITableViewAutomaticDimension
        
        only.
        */
        
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            return UITableViewAutomaticDimension
        }
        else if (tableView == self.searchDisplayController?.searchResultsTableView)
        {
            return 69
        }
        
        return self.heightForBasicCellAtIndexPath(indexPath)
        
        
        
        
    }
    
    
    /********All methods below is not necessary if you want to support ios8+ ***********/
    
    func calculateHeightForConfiguredSizingCell(cell: PostViewCell,index : Int) -> CGFloat {
        
        
        
        var postObj = PostObj()
        postObj.setValuesForKeysWithDictionary(self.arrPost[index] as! [NSObject : AnyObject])
        
        
        cell.lblStatus.text = postObj.caption
        cell.lblStatus.autosizeForWidth(Float(cell.lblStatus.width))
        
        if postObj.images.count ==  0
        {
            cell.imgMain.height = 0
            cell.collectImage.height = 0
        }
        else
        {
            
            
            if  postObj.images.count == 1
            {
                var imgobject: AnyObject = postObj.images[0]
                var url = NSURL(string: (imgobject["image_original"] as? String!)!)
                
                var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
                if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
                {
                    var imageAspectRatio = image.size.width / cell.imgMain.width
                    cell.imgMain.height = image.size.height / imageAspectRatio
                    cell.imgMain.image = image
                    
                }
                
            }
            else
            {
                cell.imgMain.height =   cell.imgMain.width
                cell.collectImage.height = cell.imgMain.width
            }
        }
        cell.reloadCell()
        return cell.imgMain.bottom + 50 //distance to bottom
        
        
    }
    func heightForBasicCellAtIndexPath(indexPath: NSIndexPath)-> CGFloat {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("nibCell") as? PostViewCell
        
        //        self.configureBasicCell(cell!, atIndexPath: indexPath)
        return self.calculateHeightForConfiguredSizingCell(cell!,index: indexPath.row)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickMenuSetting(sender: AnyObject) {
        var btnTemp = sender as! UIButton
        if  btnTemp.tag == 0
        {
            
            var followVC = Utilities.viewController("MyPinViewController", onStoryboard: "SelfProfile")
            self.navigationController?.pushViewController(followVC, animated: true)
        }
        else if  btnTemp.tag == 1
        {
            
            var followVC = Utilities.viewController("BlurFollowTableViewController", onStoryboard: "SelfProfile")
            self.navigationController?.pushViewController(followVC, animated: true)
        }
        else if btnTemp.tag == 2
        {
            var helpVC = Utilities.viewController("NotesViewController", onStoryboard: "SelfProfile")
            self.navigationController?.pushViewController(helpVC, animated: true)

        }
        else if btnTemp.tag == 4
        {
            var photosVC = Utilities.viewController("MyPhotosViewController", onStoryboard: "SelfProfile")
            self.navigationController?.pushViewController(photosVC, animated: true)
            
        }

        else if btnTemp.tag == 5
        {
            var helpVC = Utilities.viewTableController("helpVC", onStoryboard: "Help")
            self.navigationController?.pushViewController(helpVC, animated: true)

        }
        else if btnTemp.tag == 6
        {
            var helpVC = Utilities.viewController("WorkoutViewController", onStoryboard: "SelfProfile")
            self.navigationController?.pushViewController(helpVC, animated: true)
            
        }
        else if btnTemp.tag == 10
        {
            var helpVC = Utilities.viewController("MyProfileViewController", onStoryboard: "SelfProfile")
            self.navigationController?.pushViewController(helpVC, animated: true)
            
        }
        
    }

}
