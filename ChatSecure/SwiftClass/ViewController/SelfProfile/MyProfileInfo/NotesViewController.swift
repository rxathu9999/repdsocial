//
//  NotesViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/14/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController {

    @IBOutlet weak var txtNotes: UITextView!
    
    @IBOutlet weak var tableNotes: UITableView!
    
    var  arrComment = []
    
    var page = 1
    
    var  to_user_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtNotes.layer.borderWidth = 1
        txtNotes.layer.borderColor = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1).CGColor
        
        txtNotes.layer.cornerRadius = 8
        
        // Do any additional setup after loading the view.
        
        txtNotes.text = "Notes"
        txtNotes.textColor = UIColor.lightGrayColor()
        
        
        
        txtNotes.selectedTextRange = txtNotes.textRangeFromPosition(txtNotes.beginningOfDocument, toPosition: txtNotes.beginningOfDocument)
        
        self.addDoneButton()
        
        self.loadListNotes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "notes"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]

    }
    
    @IBAction func clickSend(sender: AnyObject) {
        
        if  count(txtNotes.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) == 0
        {
            return
        }
        
        var json = JSonConnect()
        
        var params : NSMutableDictionary = ["from_user_id" : User.sharedInstance.currentUser.user_id,"note" : txtNotes.text]
            params ["to_user_id"] = to_user_id == 0 ? User.sharedInstance.currentUser.user_id : to_user_id
        
        
        
        json.postPath("note/index", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                   self.loadListNotes()
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    Utilities.showErrorStatus(error)
                }
        }

    }
    
    func loadListNotes()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,"page" : String(page)]
 
        
        json.postPath("note/notelist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                    self.arrComment = result["note"] as! NSArray
                    self.tableNotes.reloadData()
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                    
                    }
                }
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        return arrComment.count
    }
    
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //        var cell = tableComment.dequeueReusableCellWithIdentifier("postCell") as? CommentViewCell
        //
        //        if cell == nil {
        //            cell = CommentViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "postCell")
        //
        //        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("postCell", forIndexPath: indexPath) as! CommentViewCell
        
        
        var object = self.arrComment[indexPath.row] as! NSMutableDictionary
        cell.lblComment.text = object["note"] as? String
        cell.lblName.text = object["user"] as? String
        
      
        cell.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",object["date"] as! Int)))

        
        return cell
    }
    
    
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    func dismissKeyboard(){
        txtNotes.resignFirstResponder()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Notes"
            textView.textColor = UIColor.lightGrayColor()
        }
        textView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView){
        if (textView.text == "Notes"){
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        textView.becomeFirstResponder()
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:NSString = textView.text
        let updatedText = currentText.stringByReplacingCharactersInRange(range, withString:text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if count(updatedText) == 0 {
            
            textView.text = "Notes"
            textView.textColor = UIColor.lightGrayColor()
            
            textView.selectedTextRange = textView.textRangeFromPosition(textView.beginningOfDocument, toPosition: textView.beginningOfDocument)
            
            return false
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, clear
            // the text view and set its color to black to prepare for
            // the user's entry
        else if textView.textColor == UIColor.lightGrayColor() && count(text) > 0 {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
        
        return true
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        txtNotes.inputAccessoryView = keyboardToolbar
    }


}
