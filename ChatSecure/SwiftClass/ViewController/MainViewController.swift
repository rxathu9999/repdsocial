//
//  MainViewController.swift
//  Crossfit
//
//  Created by buisinam on 6/22/15.
//  Copyright (c) 2015 buisinam. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    var gotoLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(animated: Bool) {
         self.navigationController?.navigationBar.hidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        User.sharedInstance.loadCurrentUser()
        if User.sharedInstance.currentUser.user_id != 0
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTab =  storyboard.instantiateViewControllerWithIdentifier("MainTab") as! UITabBarController
            self.navigationController?.pushViewController(mainTab, animated: true)
            
        }

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if gotoLogin == true
        {
            gotoLogin = false
            let loginVC = Utilities.viewController("LoginViewController", onStoryboard: "Login")
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickRegister(sender: AnyObject) {
        
        let parentRegister = Utilities.viewController("ParentContainerViewController", onStoryboard: "Login")
        self.navigationController?.pushViewController(parentRegister, animated: true)
    }
    @IBAction func clickLogin(sender: AnyObject) {
        let loginVC = Utilities.viewController("LoginViewController", onStoryboard: "Login")
        self.navigationController?.pushViewController(loginVC, animated: true)
        
    }

}
