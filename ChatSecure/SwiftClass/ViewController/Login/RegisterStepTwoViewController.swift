//
//  RegisterStepTwoViewController.swift
//  ChatSecure
//
//  Created by buisinam on 6/23/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit


class RegisterStepTwoViewController: UITableViewController ,UITextFieldDelegate,IQActionSheetPickerViewDelegate,CLLocationManagerDelegate,MPGTextFieldDelegate{
    internal var sampleData = [Dictionary<String, AnyObject>]()
    @IBOutlet weak var txtFirstName: CustomTextField!
    
    @IBOutlet weak var txtLastName: CustomTextField!
    
    @IBOutlet weak var txtDate: CustomTextField!
    
    @IBOutlet weak var txtHome: MPGTextField_Swift!
    @IBOutlet var txtMobile: CustomTextField!
    @IBOutlet weak var btnSkip: UIButton!

    var pickerDate : IQActionSheetPickerView!
    
    var autocompleteTextField : MLPAutoCompleteTextField!
    
    var parent : ParentContainerViewController!
    
    @IBOutlet var heightMobiel: NSLayoutConstraint!
    internal var params : [String : String]!
    
    var home_box_id = -1
    
    internal var isFromNormal = false
    
    internal var registerDict : NSMutableDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        txtFirstName.attributedPlaceholder = NSAttributedString(string:"First name",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtLastName.attributedPlaceholder = NSAttributedString(string:"Last Name",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtDate.attributedPlaceholder = NSAttributedString(string:"Date of birth",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtHome.attributedPlaceholder = NSAttributedString(string:"Enter home box",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtHome.font = txtDate.font
        txtMobile.attributedPlaceholder = NSAttributedString(string:"Mobile",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
         btnMale.selected = true
        
        pickerDate = IQActionSheetPickerView(title: "", delegate: self)
        pickerDate.actionSheetPickerStyle = IQActionSheetPickerStyle.DatePicker
        
        let currentDate: NSDate = NSDate()
        
        let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)!
        
        let components: NSDateComponents = NSDateComponents()
        components.calendar = calendar
        
        components.year = -18
        let minDate: NSDate = calendar.dateByAddingComponents(components, toDate: currentDate, options: nil)!

        
        pickerDate.maximumDate = minDate
        dispatch_async(dispatch_get_main_queue()) {
            self.generateData()
        }
        
        txtHome!.mDelegate = self
        txtHome.layer.backgroundColor = UIColor.clearColor().CGColor
        txtHome.layer.masksToBounds = true
        txtHome.layer.borderWidth = 0.5
        txtHome.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        self.fillTextfield()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWasShown:"), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil)
        
        self.addDoneButton()
    }
        func keyboardWasShown(notification: NSNotification) {
            var info = notification.userInfo!
            var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.size.height + 20, right: 0)
            self.view.layoutIfNeeded()
            
        }
        
        func keyboardWillHide(notification: NSNotification){
            var info = notification.userInfo!
            var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
            
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.view.layoutIfNeeded()
        }

    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        txtFirstName.inputAccessoryView = keyboardToolbar
        txtLastName.inputAccessoryView = keyboardToolbar
        txtMobile.inputAccessoryView = keyboardToolbar
        txtHome.inputAccessoryView = keyboardToolbar
    }
    @IBAction func clickSkip(sender: AnyObject) {
        btnSkip.selected = !btnSkip.selected
    }

    func fillTextfield()
    {
        
        if (self.params == nil)
        {
            self.heightMobiel.constant = 0
            
            self.updateViewConstraints()
        }
        else
        {
            if let firstName = params["first_name"]
            {
                txtFirstName.text = firstName
            }
            if let lastname = params["last_name"]
            {
                txtLastName.text = lastname
            }
            if let gender = params["gender"]
            {
                if gender == "male"
                {
                    btnMale.selected = true
                    btnFemale.selected = false
                }
                else
                {
                    btnMale.selected = false
                    btnFemale.selected = true

                }
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
       
    }
    
    
    
    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField == txtDate{
            txtFirstName.resignFirstResponder()
            txtHome.resignFirstResponder()
            txtLastName.resignFirstResponder()
            txtMobile.resignFirstResponder()
            pickerDate.show()
            return false
        }
        else
        {
            
            return true
        }
    }
    
    
   //MARK: - PickerviewDelegate
    func actionSheetPickerView(pickerView: IQActionSheetPickerView!, didSelectDate date: NSDate!) {
        var formatdate = NSDateFormatter()
        formatdate.dateFormat = "dd-MM-yyyy"
        txtDate.text = formatdate.stringFromDate(date)
    }
    
    func actionSheetPickerView(pickerView: IQActionSheetPickerView!, didSelectTitles titles: [AnyObject]!) {
        
    }
    
    @IBAction func clickLocation(sender: AnyObject) {

        let locationGPS = LocationManager()
        locationGPS.getLocationCompleteBlock { (longtitude, latitude) -> Void in
            var geocoder = CLGeocoder()
            var location = CLLocation(latitude: latitude, longitude: longtitude)
            geocoder.reverseGeocodeLocation(location) {
                (placemarks, error) -> Void in
                if let placemarks = placemarks as? [CLPlacemark] where placemarks.count > 0 {
                    var placemark = placemarks[0] as CLPlacemark
                    println(placemark.addressDictionary)
                    self.txtHome.text = String(format: "%@%@%@%@", placemark.addressDictionary["Street"] as! String,placemark.addressDictionary["SubLocality"] as! String,placemark.addressDictionary["SubAdministrativeArea"] as! String,placemark.addressDictionary["State"] as! String)


                }
                
            }
        }
        

    }
    
    
    func generateData(){
        
        dispatch_async(dispatch_get_main_queue(),{
            var err : NSErrorPointer?
            var dataPath = NSBundle.mainBundle().pathForResource("convertcsv", ofType: "json")
            var data : NSData!
            var contents : JSON!
            
            var IndexAddress = 0
            
            data = NSData(contentsOfFile: dataPath!)
            
            
            contents = JSON(data:data)
            self.sampleData = contents.arrayObject as! [Dictionary<String, AnyObject>]

        })
        
        
    }
    
    func dataForPopoverInTextField(textfield: MPGTextField_Swift) -> [Dictionary<String, AnyObject>]
    {
        return sampleData
    }
    
    func textFieldShouldSelect(textField: MPGTextField_Swift) -> Bool{
        return true
    }
    
    func textFieldDidEndEditing(textField: MPGTextField_Swift, withSelection data: Dictionary<String,AnyObject>){
        println("Dictionary received = \(data)")
        
        if let str = data["FIELD1"] as? Int
        {
            
            home_box_id = str//;(data["FIELD1"] as! String).toInt()!
        }
        else
        {
            println("can't not find")
            if home_box_id != -1
            {
                return
            }
            else
            {
                home_box_id = -1
            }
        }
        
    }

    
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    
    @IBAction func clickGender(sender: AnyObject) {
        if sender as! NSObject == btnMale
        {
            btnFemale.selected = false
            btnMale.selected = true
        }
        else
        {
            btnFemale.selected = true
            btnMale.selected = false
        }
    }
    
    @IBAction func clickUpdate(sender: AnyObject) {
            self.validate()
    }
    func validate()
    {
        txtHome.resignFirstResponder()
        
        var alertview : UIAlertView!
        
        if count(txtFirstName.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter first name", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        
        if count(txtLastName.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter last name", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if count(txtMobile.text) < 0
        {
            alertview = UIAlertView(title: "", message: "Please enter mobile number", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
//        if Utilities.isValidNumber(txtMobile.text)
//        {
//            alertview = UIAlertView(title: "", message: "Mobile phone is wrong format", delegate: nil, cancelButtonTitle: "OK")
//            alertview.show()
//            return
//        }

        
        if count(txtDate.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please select DOB" ,delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if self.home_box_id == -1
        {
            if  count(self.txtHome.text) == 0
            {
                alertview = UIAlertView(title: "", message: "Please enter homebox(GYM) name" ,delegate: nil, cancelButtonTitle: "OK")
                alertview.show()
                return
            }
            else
            {
                alertview = UIAlertView(title: "", message: "Please select homebox(GYM) name" ,delegate: nil, cancelButtonTitle: "OK")
                alertview.show()
                return
            }
        }
        
        if btnSkip.selected == false
        {
            alertview = UIAlertView(title: "", message: "You must agree tos before signup", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }

        
        ///go from signup email
        if self.isFromNormal == false {
            
            ProgressHUD.show("Loading...")
            
            var json = JSonConnect()
            //check 1st login
            
            //        let paramscheck = ["social_media_type" : params["social_media_type"], "id" : params["id"]]
            
            params["first_name"] = txtFirstName.text
             params["last_name"] = txtLastName.text
             params["mobile"] = txtMobile.text
             params["dob"] = txtDate.text
             params["home_box_id"] = String(home_box_id)
            params["gender"] = btnMale.selected == true ? "m" : "l"
            
            json.postPath("user/loginviasocial", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
                
                var userObj = User()
                userObj.setValuesForKeysWithDictionary(result["User"]  as! [NSObject : AnyObject])
//                userObj.token = result["token"] as! String
                User.sharedInstance.saveToDefault(userObj)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainTab =  storyboard.instantiateViewControllerWithIdentifier("MainTab") as! UITabBarController
                self.parent.navigationController?.pushViewController(mainTab, animated: true)
                ProgressHUD.dismiss()
                
                }, errorBlock: { (error) -> Void in
                    alertview = UIAlertView(title: "", message: error.description ,delegate: nil, cancelButtonTitle: "OK")
                    alertview.show()
                    ProgressHUD.dismiss()
            })
            
        }
        else
        {
            registerDict["first_name"] = txtFirstName.text
            registerDict["last_name"] = txtLastName.text
            
            registerDict["dob"] = txtDate.text
            registerDict["home_box_id"] = String(home_box_id)
            registerDict["gender"] = btnMale.selected == true ? "m" : "l"
            NSNotificationCenter.defaultCenter().postNotificationName("childViewClicked", object: nil, userInfo: ["index" : 1])
        }
        
    }

    
}
