//
//  LoginViewController.swift
//  Crossfit
//
//  Created by buisinam on 6/22/15.
//  Copyright (c) 2015 buisinam. All rights reserved.
//

import UIKit
import TwitterKit


class LoginViewController: UITableViewController,GPPSignInDelegate {
    
    @IBOutlet weak var txtEmail: CustomTextField!
    
    @IBOutlet weak var txtPassword: CustomTextField!
    var  animateDistance : CGFloat = 0.0
    var twitter : STTwitterAPI!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_login1"))
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email id or Mobile number",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        var signIn = GPPSignIn.sharedInstance();
        signIn.shouldFetchGooglePlusUser = true;
        signIn.clientID = "282941716499-tdqt5e6gnl386v69kv1c15qog60gan5f.apps.googleusercontent.com"
        signIn.shouldFetchGoogleUserEmail = true;
        signIn.shouldFetchGoogleUserID = true;
        signIn.scopes = [kGTLAuthScopePlusLogin]
        signIn.trySilentAuthentication();
        signIn.delegate = self;
        
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "callBackFromTwitter:",
            name:"callBackFromTwitter",
            object: nil
        )
        self.addDoneButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
       
        txtEmail.inputAccessoryView = keyboardToolbar
       
        txtPassword.inputAccessoryView = keyboardToolbar
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.title = "Login"
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!]
//        self.setNeedsStatusBarAppearanceUpdate()
        
        
                
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    func callBackFromTwitter(notification : NSNotification)
    {
        var oauth_token = (notification.userInfo as! [String:String])["oauth_token"]
        var oauth_verifier = (notification.userInfo as! [String:String])["oauth_verifier"]
        
        self.twitter.postAccessTokenRequestWithPIN(oauth_verifier, successBlock: { (oauthToken, oauthTokenSecret, userID, screenName) -> Void in
            
            //get user profiles
            self.twitter.getUsersShowForUserID(userID, orScreenName: screenName, includeEntities: 0, successBlock: { (reusult) -> Void in
                
            }, errorBlock: { (error) -> Void in
                
            })
            
        }) { (error) -> Void in
            
        }
        
       

        
//        var params = ["social_media_type" : "g",
//            "id" : String(user.identifier),
//            "email_id" : GPPSignIn.sharedInstance().userEmail as String,
//            "first_name" : user.name.familyName as String,
//            "last_name" : user.name.givenName as String,
//            "device_id" : appDelegate.device_token as String,
//            "device_type" : "i",
//            "gender" : user.gender == "male" ? "male" : "female",
//            
//        ]
//        
//        self.loginSocial(params)

       
    }
    
    @IBAction func clickLogin(sender: AnyObject) {
        
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
        
        var alertview : UIAlertView!
        
        if count(txtEmail.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter email id", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        
        if count(txtPassword.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter password", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        

        
        var params = ["email_id" : txtEmail.text,
            "password" : txtPassword.text
            
            
        ]
        ProgressHUD.show("logging...", interaction: false)
        var json = JSonConnect()
        
        json.postPath("user/login", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            var userObj = User()
            userObj.setValuesForKeysWithDictionary(result["User"]  as! [NSObject : AnyObject])
//            userObj.token = result["token"] as! String
            userObj.is_normal = true
            User.sharedInstance.saveToDefault(userObj)
            
            self.txtEmail.text = ""
            self.txtPassword.text = ""
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainTab =  storyboard.instantiateViewControllerWithIdentifier("MainTab") as! MainTabbarViewController
            self.navigationController?.pushViewController(mainTab, animated: true)

            
            }, errorBlock: { (error) -> Void in
                if (error != nil)
                {
                    ProgressHUD.dismiss()
                    Utilities.showErrorStatus(error)
                }
        })


        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickLoginFB(sender: AnyObject) {
        var fbConnect = FBConnect()
        ProgressHUD.show("Loading...", interaction: false)
        fbConnect.loginFaceBook({ (userDict) -> Void in
            
            var appDelegate = UIApplication.sharedApplication().delegate as! OTRAppDelegate
            
            var params = ["social_media_type" : "f",
                        "id" : userDict["id"] as! String,
            "email_id" : userDict["email"] as! String,
            "first_name" : userDict["first_name"] as! String,
            "last_name" : userDict["last_name"] as! String,
            "device_id" : appDelegate.device_token as String,
            "device_type" : "i",
            "gender" : userDict["gender"] as! String! == "male" ? "male" : "female",
                
            ]
            
            
           self.loginSocial(params)
            
            ProgressHUD.dismiss()
        }, errorFbBlock: { (error) -> Void in
            ProgressHUD.dismiss()
        })
        
    }
    @IBAction func clickTwitter(sender: AnyObject) {
//        [[Twitter sharedInstance] startWithConsumerKey:@"VeUW2c7mmVOZtaEh3YyD6fNpZ" consumerSecret:@"VlCXkoahWMjd41jeZLvaULBwjfFHXArc4KkZ3e4MrgeHxA6dWr"];
//        self.twitter = STTwitterAPI(OAuthConsumerKey: "VeUW2c7mmVOZtaEh3YyD6fNpZ", consumerSecret: "VlCXkoahWMjd41jeZLvaULBwjfFHXArc4KkZ3e4MrgeHxA6dWr")
//        
//        self.twitter.postTokenRequest({ (url , authToken ) -> Void in
//            UIApplication.sharedApplication().openURL(url)
//        }, authenticateInsteadOfAuthorize: false, forceLogin: (true), screenName: nil, oauthCallback: "myapp://twitter_access_tokens/") { (error) -> Void in
//            println(error)
//        }
        
        Twitter.sharedInstance().logInWithCompletion {
            (session, error) -> Void in
            if (session != nil) {
                println("signed in as \(session.userName)");
                
               var  shareEmailViewController = TWTRShareEmailViewController(completion: { (email, error) -> Void in
                    
               })
                self.presentViewController(shareEmailViewController, animated: true, completion: nil)
            } else {
                println("error: \(error.localizedDescription)");
            }
        }

    }
    @IBAction func clickGG(sender: AnyObject) {
        
        // Update the buttons and text.
        var signIn = GPPSignIn.sharedInstance();
        signIn.authenticate();

    }
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        updateUI()
    }
    
    func updateUI() {
        // TODO: Toggle buttons here.
        if (GPPSignIn.sharedInstance().userID != nil){
            // Signed in?
            var user = GPPSignIn.sharedInstance().googlePlusUser;
          var appDelegate = UIApplication.sharedApplication().delegate as! OTRAppDelegate
                var params = ["social_media_type" : "g",
                "id" : String(user.identifier),
                "email_id" : GPPSignIn.sharedInstance().userEmail as String,
                "first_name" : user.name.familyName as String,
                "last_name" : user.name.givenName as String,
                "device_id" : appDelegate.device_token as String,
                "device_type" : "i",
                "gender" : user.gender == "male" ? "male" : "female",
                
            ]
            
            self.loginSocial(params)
            
          
        } else {
        
        
        }
    }
    
    func loginSocial(params : [String:String])
    {
        ProgressHUD.show("Loading...", interaction: false)
        var json = JSonConnect()
        //check 1st login
        
        
        json.postPath("user/checksocialid", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            
            if (result != nil)
            {
                let is_exist: Int = (result["is_exist"] as? Int)!
                
                if is_exist == 0 //1st  login
                {
                    let parentContainerViewController = Utilities.viewController("ParentContainerViewController", onStoryboard: "Login") as! ParentContainerViewController
                    parentContainerViewController.step = 2
                    parentContainerViewController.params = params
                    self.navigationController?.pushViewController(parentContainerViewController, animated: true)

                }
                else
                {
                    var userObj = User()
                    userObj.setValuesForKeysWithDictionary(result["User"]  as! [NSObject : AnyObject])

                    User.sharedInstance.saveToDefault(userObj)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainTab =  storyboard.instantiateViewControllerWithIdentifier("MainTab") as! UITabBarController
                    self.navigationController?.pushViewController(mainTab, animated: true)

                }
            }
            
            ProgressHUD.dismiss()
            
            
            }, errorBlock: { (error) -> Void in
                ProgressHUD.dismiss()
                if  (error != nil)
                {
                    Utilities.showErrorStatus(error)
                }
        })
        
    }
    
    @IBAction func gotoChangePassword(sender: AnyObject) {
        
        let forgotVC = Utilities.viewController("ForgotPassViewController", onStoryboard: "Login")
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
        
    }

}
