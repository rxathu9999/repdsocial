//
//  RegisterStepOneViewController.swift
//  ChatSecure
//
//  Created by buisinam on 6/23/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class RegisterStepOneViewController: UITableViewController,UITextFieldDelegate {

    @IBOutlet weak var txtEmail: CustomTextField!
    
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var txtConfirm: CustomTextField!
    
    @IBOutlet weak var txtNumber: CustomTextField!
    
    @IBOutlet weak var btnSkip: UIButton!
    var  OTRXMPPRegisterFailedNotificationNameObject :AnyObject?
    var  OTRXMPPRegisterSucceededNotificationNameObject :AnyObject?
    
    let  account = OTRXMPPAccount ()
    
    let  arrTextField : NSMutableArray = []
    
    internal var registerDict : NSMutableDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email id",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtConfirm.attributedPlaceholder = NSAttributedString(string:"Confirm password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtNumber.attributedPlaceholder = NSAttributedString(string:"Mobile phone number",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        var currentLocale = NSLocale.currentLocale()
        var countryCode: AnyObject? = currentLocale.objectForKey(NSLocaleCountryCode)
        
        txtNumber.text = countryCode?.string
        
        self.addDoneButton()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == txtNumber
        {
            var newLength = count(textField.text) + count(string) - range.length;
        
            return !(newLength > 18)
        }
        else
        {
            return  true
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        var _RegisterStepOneViewController = self
        
        self.OTRXMPPRegisterSucceededNotificationNameObject = NSNotificationCenter.defaultCenter().addObserverForName(OTRXMPPRegisterSucceededNotificationName, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { (note) -> Void in
            _RegisterStepOneViewController.didReceiveRegistrationSucceededNotification(note)
        })
        
        self.OTRXMPPRegisterFailedNotificationNameObject = NSNotificationCenter.defaultCenter().addObserverForName(OTRXMPPRegisterFailedNotificationName, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { (note) -> Void in
            _RegisterStepOneViewController.didReceiveRegistrationFailedNotification(note)
        })

            
       

    }
    
    @IBAction func clickSkip(sender: AnyObject) {
        btnSkip.selected = !btnSkip.selected
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if OTRXMPPRegisterFailedNotificationNameObject != nil && OTRXMPPRegisterSucceededNotificationNameObject != nil
        {
        
            NSNotificationCenter.defaultCenter().removeObserver(self.OTRXMPPRegisterFailedNotificationNameObject!)
            NSNotificationCenter.defaultCenter().removeObserver(self.OTRXMPPRegisterSucceededNotificationNameObject!)
        }
       
    }
    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickRegister(sender: AnyObject) {

        
//        var newUsername = self.fixUsername(txtEmail.text, domain: HOST_PEER)
//        self.account.username = newUsername
//        self.account.domain = HOST_PEER
//        
//        var xmppManager = self.xmppManagerForCurrentAccount()
//        if !xmppManager.isKindOfClass(NSNull)
//        {
//            xmppManager.registerNewAccountWithPassword(txtPassword.text)
//        }
        
        //
        
        
        self.validate()

        
        
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        
        let previous = UIBarButtonItem(title: "Previous", style: .Plain, target: self, action: "previous:")
        
        let next = UIBarButtonItem(title: "Next", style: .Plain, target: self, action: "next:")
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [previous,next,flexBarButton, doneBarButton]
        
        arrTextField.addObject(txtEmail)
        arrTextField.addObject(txtPassword)
        arrTextField.addObject(txtConfirm)
        arrTextField.addObject(txtNumber)
        
        
        txtConfirm.inputAccessoryView = keyboardToolbar
        txtEmail.inputAccessoryView = keyboardToolbar
        txtNumber.inputAccessoryView = keyboardToolbar
        txtPassword.inputAccessoryView = keyboardToolbar
    }
    
    func next(sender : AnyObject)
    {
        nextField(sender, arrTextField)
    }
    
    func previous(sender : AnyObject)
    {
        previousField(sender, arrTextField)
    }
        
    func validate()
    {
        var alertview : UIAlertView!
        
        if count(txtEmail.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter email id", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if  !Utilities.isValidEmail(txtEmail.text)
        {
            alertview = UIAlertView(title: "", message: "Please enter valid email id", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if count(txtPassword.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter password", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if count(txtPassword.text) < 6
        {
            alertview = UIAlertView(title: "", message: "Please enter password at least 6 characters long", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if txtPassword.text != txtConfirm.text
        {
            alertview = UIAlertView(title: "", message: "Confirm password  does not match", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        if count(txtNumber.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter mobile number", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
//        if btnSkip.selected == false
//        {
//            alertview = UIAlertView(title: "", message: "You must agree tos before signup", delegate: nil, cancelButtonTitle: "OK")
//            alertview.show()
//            return
//        }
//        
        
//        if Utilities.isValidNumber(txtNumber.text)
//        {
//            alertview = UIAlertView(title: "", message: "Mobile phone is wrong format", delegate: nil, cancelButtonTitle: "OK")
//            alertview.show()
//            return
//        }
        
//        ProgressHUD.show("Loading...")
        
        ////call services here
        var jsonAPI = JSonConnect()
        
        registerDict = ["email_id" : txtEmail.text,
            "password" : txtPassword.text ,
            "mobile" : txtNumber.text,
        
        "first_name" : " ",
        "last_name" : " ",
        "dob" : " ",
        "home_box_id" : " ",
        "gender" : "Male"]
        
            NSNotificationCenter.defaultCenter().postNotificationName("childViewClicked", object: nil, userInfo: ["index" : 0])
       
    }
    
    func fixUsername(username : String , domain : String) -> String
    {
        var finalUsername = username.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) as String
       
        if count(finalUsername) > 0
        {
            if finalUsername.rangeOfString("@peerbits.in") == nil
            {
                finalUsername = String(format: "%@@%@",username,domain)
            }
        }
        
       return finalUsername
       
        
    }
    
    func xmppManagerForCurrentAccount()-> OTRXMPPManager
    {
        var protocolID = OTRProtocolManager.sharedInstance().protocolForAccount(self.account)
        var xmppManager : OTRXMPPManager =  OTRXMPPManager()
        if protocolID.isKindOfClass(OTRXMPPManager)
        {
            xmppManager = protocolID as! OTRXMPPManager
        }
        return xmppManager
    }
    
    func didReceiveRegistrationSucceededNotification(notification : NSNotification)
    {
    
    }
    
    func didReceiveRegistrationFailedNotification(notification : NSNotification)
    {
        
    }
   

}
