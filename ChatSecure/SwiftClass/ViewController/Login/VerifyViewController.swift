//
//  VerifyViewController.swift
//  ChatSecure
//
//  Created by buisinam on 6/25/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class VerifyViewController: UITableViewController,UITextFieldDelegate {
    @IBOutlet weak var txtinput: UITextField!
    
    var arrVerify = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "bg_register1"))
        txtinput.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }

    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        //delete passcode
        
        if string == ""{
            if count(arrVerify) > 0
            {
                arrVerify.removeLast()
            }
        }
        else
        {
            if count(arrVerify) < 4
            {
                arrVerify.append(string)
            }
        }
        self.updatePassCode()
        return true
    }
    
    func updatePassCode()
    {
        for i in 1...4
        {
            var view = self.view.viewWithTag(i) as! UIButton
            if i <= count(arrVerify)
            {
                view.selected = true
            }
            else
            {
                view.selected = false
            }
        }
    }
    
    @IBAction func clickContinue(sender: AnyObject) {
        
        var verifyCode = ""
       
        for str in arrVerify
        {
            verifyCode += str as String
        }
        
        var params = ["mobile" : "0123456789",
            "verification_code" : verifyCode]
            
            
        var json  = JSonConnect()
        json.postPath("user/mobileverification", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            
        }) { (error) -> Void in
            
        }
        
        
        let n: Int! = self.navigationController?.viewControllers?.count
        let myUIViewController = self.navigationController?.viewControllers[n-2] as! ParentContainerViewController
        myUIViewController.setViewControllerToContainer(myUIViewController.stepTwoVC as UITableViewController)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
}
