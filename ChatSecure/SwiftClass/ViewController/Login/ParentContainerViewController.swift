//
//  ParentContainerViewController.swift
//  Crossfit
//
//  Created by buisinam on 6/22/15.
//  Copyright (c) 2015 buisinam. All rights reserved.
//

import UIKit



class ParentContainerViewController: UIViewController {

    @IBOutlet weak var containView: UIView!
    @IBOutlet var viewHeader: UIView!
    
    internal var step  = 1
    
    var stepOneVC : RegisterStepOneViewController!
    var stepTwoVC : RegisterStepTwoViewController!
    var stepThreeVC : RegisterStepThreeViewController!
    
    @IBOutlet var heighHeader: NSLayoutConstraint!
    let viewControllerIdentifiers = ["first", "second"]
    
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblBasic: UILabel!
    @IBOutlet weak var lblPast: UILabel!
    
    @IBOutlet weak var check1: UIImageView!
    @IBOutlet weak var check2: UIImageView!
    @IBOutlet weak var check3: UIImageView!

    var params : [String : String]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "bgNav"), forBarMetrics: UIBarMetrics.Default)
        
        
        
        stepOneVC = Utilities.viewController("stepOne", onStoryboard: "Login") as! RegisterStepOneViewController
        
        
        stepTwoVC = Utilities.viewController("stepTwoVC", onStoryboard: "Login") as!
        RegisterStepTwoViewController
        stepTwoVC.parent = self
        
        stepThreeVC = Utilities.viewController("stepThreeVC", onStoryboard: "Login") as!
        RegisterStepThreeViewController
        stepThreeVC.generateData()
        
        if (self.params != nil)
        {
            stepTwoVC.params = self.params
            
            self.heighHeader.constant = 0
            self.updateViewConstraints()
            viewHeader.hidden = true
        }
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "childViewClicked:",
            name:"childViewClicked",
            object: nil
        )
        
        if step == 1
        {
            self.setViewControllerToContainer(stepOneVC as UITableViewController)
        }
        else if step == 2
        {
            self.setViewControllerToContainer(stepTwoVC as UITableViewController)
        }
//
//        self.setViewControllerToContainer(stepTwoVC as UITableViewController)
//        self.setViewControllerToContainer(stepThreeVC as UITableViewController)
    }
    
    func setViewControllerToContainer(viewVC : UITableViewController)
    {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
            
            var f = self.containView.frame
            f.origin.x = 0
            f.origin.y = 0
            viewVC.view.frame = f
            for view in self.containView.subviews
            {
                view.removeFromSuperview()
            }
            self.containView.addSubview(viewVC.view)
            self.updateView(viewVC)
            
            UIView.setAnimationTransition(UIViewAnimationTransition.FlipFromRight, forView: self.containView!, cache: false)
        })
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.navigationItem.title = "Register"
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!]
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }

    
    func childViewClicked(notification: NSNotification)
    {
        var index = (notification.userInfo as! [String:Int])["index"]
        
        if index == 0
        {
                step = 1
//            let verifyViewController = Utilities.viewController("VerifyViewController", onStoryboard: "Login")
//
//            self.navigationController?.pushViewController(verifyViewController, animated: true)
            stepTwoVC.registerDict = stepOneVC.registerDict
            self.stepTwoVC.isFromNormal = true
            self.setViewControllerToContainer(self.stepTwoVC)
        }
        else if index == 1
        {
            step = 2
            stepThreeVC.sampleData = stepTwoVC.sampleData
            stepThreeVC.registerDict = stepTwoVC.registerDict
            self.setViewControllerToContainer(self.stepThreeVC)
        }
        else if index == 2
        {
            
            var numberOfViewControllers = self.navigationController!.viewControllers.count;
            
            if numberOfViewControllers < 2
            {
                return
            }
            else
            {
                var mainVC = self.navigationController?.viewControllers[numberOfViewControllers-2] as! MainViewController
                mainVC.gotoLogin = true
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
        else if index == 3
        {
            step = 0
            self.setViewControllerToContainer(self.stepOneVC)
        }
    }
    
    func updateView(viewVC : AnyObject)
    {
        if viewVC.isEqual(stepOneVC) {
            lblLogin.highlighted = true
            lblBasic.highlighted = false
            lblPast.highlighted = false
            
            check1.hidden = false
            check2.hidden = true
            check3.hidden = true
        }
        else if viewVC.isEqual(stepTwoVC)
        {
            lblLogin.highlighted = true
            lblBasic.highlighted = true
            lblPast.highlighted = false
            
            check1.hidden = false
            check2.hidden = false
            check3.hidden = true
        }
        else
        {
            lblLogin.highlighted = true
            lblBasic.highlighted = true
            lblPast.highlighted = true
            
            check1.hidden = false
            check2.hidden = false
            check3.hidden = false
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func childClick(index: Int) {
        
    }

    @IBAction func clickSignup(sender: AnyObject) {
        
//        stepThreeVC.signUp()
        
        if step == 2
        {
            if stepTwoVC.isFromNormal == true
            {
            
                stepThreeVC.signUp()
            }
        }
        else
        {
           var  alertview = UIAlertView(title: "", message: "Please follow and finish all step", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
        }
//
    }
    
    @IBAction func clickPrevious(sender: AnyObject) {
        var btnTemp = sender as! UIButton

        if btnTemp.tag < step
        {
            if btnTemp.tag == 0
            {
                    step  = 0
                    self.setViewControllerToContainer(self.stepOneVC)
                
            }
            else if btnTemp.tag == 1
            {
                    step  = 1
                    self.setViewControllerToContainer(self.stepTwoVC)
            }
        }
    }
    
    
    
}
