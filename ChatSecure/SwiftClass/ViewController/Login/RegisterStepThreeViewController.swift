//
//  RegisterStepThreeViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/6/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class RegisterStepThreeViewController: UITableViewController,MPGTextFieldDelegate,IQActionSheetPickerViewDelegate,UITextFieldDelegate {
    
    
    var arrHomeBoxId = NSMutableArray()
    
    var sampleData = [Dictionary<String, AnyObject>]()
    

    var pickerDate : IQActionSheetPickerView!
    internal var registerDict : NSMutableDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        pickerDate = IQActionSheetPickerView(title: "", delegate: self)
        pickerDate.actionSheetPickerStyle = IQActionSheetPickerStyle.DatePicker
        pickerDate.maximumDate = NSDate()
        
         self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWasShown:"), name:UIKeyboardWillShowNotification, object: nil)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil)

        
//        self.tableView.reloadData()
        
    }
    
    func keyboardWasShown(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.size.height + 20, right: 0)
        self.view.layoutIfNeeded()
        
    }
    
    func keyboardWillHide(notification: NSNotification){
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
       self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        self.generateData()
    }
    
    func generateData(){
        dispatch_async(dispatch_get_main_queue(),{
            var err : NSErrorPointer?
            var dataPath = NSBundle.mainBundle().pathForResource("convertcsv", ofType: "json")
            var data : NSData!
            var contents : JSON!
            
            var IndexAddress = 0
            
            data = NSData(contentsOfFile: dataPath!)
            
            
            contents = JSON(data:data)
            self.sampleData = contents.arrayObject as! [Dictionary<String, AnyObject>]
            
        })

        
        self.tableView.reloadData()
        
        var formatdate = NSDateFormatter()
        formatdate.dateFormat = "dd-MM-yyyy"

        for i in 0...4
        {
            var nsdict = ["address": " ","box_id" : -1, "date" : formatdate.stringFromDate(NSDate())]
            arrHomeBoxId.addObject(nsdict)
        }

        //println(contents[0]["first_name"])
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
//            
//            while IndexAddress < contents.count
//            {
//                for var i = IndexAddress;i<100;++i{
//                    if i < contents.count
//                    {
//                        var node = contents.arrayValue[i]
//                        var name = node["FIELD4"].string
//                        var lName = node["FIELD2"].string
//                        var address = String(format: "%@, %@",name, lName)
//                        var email = node["FIELD5"].string
//                        //                    var dicNode :NSMutableDictionary =  node.dictionary
//                        var dictionary = ["DisplayText": address,"DisplaySubText": email,"id": node["FIELD1"].string]
//                        IndexAddress += 1
//                        self.sampleData.append(dictionary)
//                    }
//                }
//                
//                
//                
//            }
//        })
        

    }
    
    
    func showPicker(sender : UIButton)
    {
        pickerDate.tag = sender.tag
        pickerDate.show()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 5
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! PastVisitViewCell

        // Configure the cell...

        
        cell.btnDate.tag = indexPath.row
        cell.txtHome.tag = indexPath.row
        cell.txtHome.showInTable = tableView
        cell.txtHome.mDelegate  = self

        cell.btnDate.addTarget(self, action: "showPicker:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.txtHome.layer.backgroundColor = UIColor.clearColor().CGColor
        cell.txtHome.layer.masksToBounds = true
        cell.txtHome.layer.borderWidth = 0.5
        cell.txtHome.layer.borderColor = UIColor.lightGrayColor().CGColor
        var formatdate = NSDateFormatter()
        formatdate.dateFormat = "dd-MM-yyyy"
//        if indexPath.row <= arrHomeBoxId.count -  1
//        {
        
            if arrHomeBoxId.count > 0
            {
                var object  =  arrHomeBoxId[indexPath.row] as! NSDictionary
                cell.btnDate.setTitle(object["date"] as? String, forState: UIControlState.Normal)
                
                if let address  = object["address"] as? String
                {
                    if address.hasPrefix(" ")
                    {
                        cell.txtHome.text = ""
                    }
                    else
                    {
                        cell.txtHome.text = address
                    }
                }
                else
                {
                    cell.txtHome.text = ""
                }
                
                if (object["date"] != nil)
                {
                    cell.btnDate.setTitle(object["date"] as? String, forState: UIControlState.Normal)
                }
                else
                {
                     cell.btnDate.setTitle(formatdate.stringFromDate(NSDate()), forState: UIControlState.Normal)
                }
            }

        
//        }
//        else
//        {
//            cell.btnDate.setTitle(formatdate.stringFromDate(NSDate()), forState: UIControlState.Normal)
//             cell.txtHome.text = ""
//           
//        }
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    func dataForPopoverInTextField(textfield: MPGTextField_Swift) -> [Dictionary<String, AnyObject>]
    {
        return sampleData
    }
    
    func textFieldShouldSelect(textField: MPGTextField_Swift) -> Bool{
        return true
    }
    
    func textFieldDidEndEditing(textField: MPGTextField_Swift, withSelection data: Dictionary<String,AnyObject>){
        println("Dictionary received = \(data)")
//        if arrHomeBoxId.count > 0
//        {
//            if  textField.tag < arrHomeBoxId.count
//            {
//                var object : [String : String]! =  arrHomeBoxId[textField.tag] as! [String : String]
//
//                object["box_id"] = data["FIELD1"] as? String
//                object["address"] = data["FIELD5"] as? String
//                self.arrHomeBoxId.replaceObjectAtIndex(textField.tag, withObject: object)
//            }
//            else
//            {
        if let str = data["FIELD1"] as? Int
        {
            
            var object : [String : AnyObject]! =  arrHomeBoxId[textField.tag] as! [String : AnyObject]
            var  date  = object["date"] as! String
            
            var newobject = ["box_id" : String(data["FIELD1"] as! Int), "address" : data["FIELD4"] as! String!,"date" : date]
            self.arrHomeBoxId.replaceObjectAtIndex(textField.tag, withObject: newobject)

        }

        
        
        
//                self.arrHomeBoxId.addObject(object)

//            }
        
//        }
//        else
//        {
//            var object = ["box_id" : data["id"] as! String!, "address" : data["DisplayText"] as! String!]
//            
//            self.arrHomeBoxId.addObject(object)
//
//        }
        self.tableView.reloadData()
        
    }
    
    
    //MARK: - PickerviewDelegate
    func actionSheetPickerView(pickerView: IQActionSheetPickerView!, didSelectDate date: NSDate!) {
        var formatdate = NSDateFormatter()
        formatdate.dateFormat = "dd-MM-yyyy"
//        if arrHomeBoxId.count > 0
//        {
            if  pickerView.tag < arrHomeBoxId.count
            {
                var object : [String : AnyObject]! =  arrHomeBoxId[pickerView.tag] as! [String : AnyObject]
            
                object["date"] = formatdate.stringFromDate(date)
                self.arrHomeBoxId.replaceObjectAtIndex(pickerView.tag, withObject: object)
            }
            else
            {
                var object = ["date" : formatdate.stringFromDate(date)]
                self.arrHomeBoxId.addObject(object)
            }
            let cell = self.tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: NSIndexPath(forRow: pickerView.tag, inSection: 0)) as! PastVisitViewCell
            cell.btnDate.setTitle("", forState: UIControlState.Normal)
//        }
//        else
//        {
//            var object = ["date" : formatdate.stringFromDate(date)]
//            self.arrHomeBoxId.addObject(object)
//            
//
//        }
        self.tableView.reloadData()

    }
    
    func actionSheetPickerView(pickerView: IQActionSheetPickerView!, didSelectTitles titles: [AnyObject]!) {
           }

    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        
        return false
    }
    
    internal func signUp()
    {
        
        var arrBox = NSMutableArray()
        for object in arrHomeBoxId
        {
            var newobject = object.copy() as! [String : AnyObject]
            var  home_box = newobject["box_id"] as? Int
            if  home_box != -1
            {
                newobject.removeValueForKey("address")
                arrBox.addObject(newobject)
            }
        }
        
        let data = NSJSONSerialization.dataWithJSONObject(arrBox, options: nil, error: nil)
        let string = NSString(data: data!, encoding: NSUTF8StringEncoding)

        
        registerDict["past_visits"] = string
        registerDict["encrypted_data"] = hmacForKeyAndData(app_key, "0")
        
        ProgressHUD.show(nil, interaction: false)
        
        
        var json = JSonConnect()
        json.postPath("user/index", parameters: NSMutableDictionary(dictionary: registerDict) , completeNetworkBlock: { (result) -> Void in
                ProgressHUD.dismiss()
                if (result != nil)
                {
                    let status = (result["status"] as! String!)
                    if status == status_success
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.message = result["message"] as? String
                        alertView.addButtonWithTitle("Ok")
                        alertView.show()
                        NSNotificationCenter.defaultCenter().postNotificationName("childViewClicked", object: nil, userInfo: ["index" : 2])
                    }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.addButtonWithTitle("Ok")
                        alertView.message = error["message"] as! String!
                        alertView.show()
                        NSNotificationCenter.defaultCenter().postNotificationName("childViewClicked", object: nil, userInfo: ["index" : 3])
                    }
                }
        }

    }

}
