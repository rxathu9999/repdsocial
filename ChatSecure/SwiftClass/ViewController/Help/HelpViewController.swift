//
//  HelpViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/8/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class HelpViewController: UITableViewController {

    @IBOutlet weak var switchNoti: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.getNotification()
               
    }
    
    func getNotification()
    {
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id]
        
        var json = JSonConnect()
        json.postPath("user/getsetting", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                    var statusSwitch = result["push_notification_status"] as! String!
                    
                    if statusSwitch == "On"
                    {
                        self.switchNoti.setOn(true, animated: true)
                    }
                    else
                    {
                        self.switchNoti.setOn(false, animated: true)
                    }
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.addButtonWithTitle("Ok")
                        alertView.message = error["message"] as! String!
                        alertView.show()
                    }
                }
        }

    }
    
    func setNotification()
    {
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,
            "push_notification_on" :  self.switchNoti.on ? "y" : "n"
        ]
        
        var json = JSonConnect()
        json.postPath("user/getsetting", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                    self.getNotification()
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                    
                    }
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Potentially incomplete method implementation.
//        // Return the number of sections.
//        return 0
//    }
//
//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete method implementation.
//        // Return the number of rows in the section.
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func ChangeValue(sender: AnyObject) {
        self.setNotification()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        //rate the app
        if indexPath.row == 1{
            var rate = RateMyApp.sharedInstance
            rate.appID = "857846130"
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                rate.trackAppUsage()
            })


        }
        if indexPath.row == 3
        {
            if User.sharedInstance.currentUser.is_normal == true
            {
                let changepassVC =  Utilities.viewController("ChangePasswordViewController", onStoryboard: "Help")
                self.navigationController?.pushViewController(changepassVC, animated: true)
            }
            else
            {
                var alertView = UIAlertView()
                alertView.title = ""
                alertView.addButtonWithTitle("Ok")
                alertView.message = "Your account can't change password"
                alertView.show()

            }
          
            
        }
        if indexPath.row == 5{
            
            var alertview : UIAlertView!
            
                alertview = UIAlertView(title: "", message: "Are you want to logout?", delegate: nil, cancelButtonTitle: "YES")
            alertview.addButtonWithTitle("NO")
                alertview.delegate = self
                alertview.show()
                return
            

            
                  }
    }
    
     func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
     {
        if  buttonIndex == 0
        {
            NSNotificationCenter.defaultCenter().postNotificationName("logOut", object: nil)
            User.sharedInstance.logOut()
        }
    }
}
