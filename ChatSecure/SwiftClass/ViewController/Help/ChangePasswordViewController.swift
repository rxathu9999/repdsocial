//
//  ChangePasswordViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/8/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var txtOldPassword: CustomTextField!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    @IBOutlet weak var txtConfirm: CustomTextField!
    
    var  animateDistance : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(animated: Bool) {
        txtOldPassword.attributedPlaceholder = NSAttributedString(string:"Old password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtNewPassword.attributedPlaceholder = NSAttributedString(string:"New password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtConfirm.attributedPlaceholder = NSAttributedString(string:"Confirm password",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        self.navigationItem.title = "Change password"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]

        self.addDoneButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        txtOldPassword.inputAccessoryView = keyboardToolbar
        txtConfirm.inputAccessoryView = keyboardToolbar
        txtNewPassword.inputAccessoryView = keyboardToolbar
        txtOldPassword.delegate = self
        txtConfirm.delegate = self
        txtNewPassword.delegate = self
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickSend(sender: AnyObject) {
        
        var alertview : UIAlertView!
        
        if count(txtOldPassword.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter old password", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return

        }
        
        if count(txtNewPassword.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter new password", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        
        if txtConfirm.text != txtNewPassword.text
        {
            alertview = UIAlertView(title: "", message: "Confirm password  does not match", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }

        
        
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id, "password" : txtNewPassword.text,
            "old_password" : txtOldPassword.text
        ]
        
        json.postPath("user/changepassword", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                    self.txtConfirm.resignFirstResponder()
                    self.txtNewPassword.resignFirstResponder()
                    self.txtOldPassword.resignFirstResponder()
                    var alertView = UIAlertView()
                    alertView.title = ""
                    alertView.message = result["message"] as? String
                    alertView.addButtonWithTitle("Ok")
                    alertView.show()
                    self.navigationController?.popViewControllerAnimated(true)
                    
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.addButtonWithTitle("Ok")
                        alertView.message = error["message"] as! String!
                        alertView.show()
                    }
                }
        }

        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
        
    }
    
    

}
