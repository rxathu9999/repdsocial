//
//  ContactUsViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/8/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtSubtitle: CustomTextField!
    @IBOutlet weak var txtMessage: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email id or Mobile number",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtSubtitle.attributedPlaceholder = NSAttributedString(string:"Subtitle",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        txtMessage.layer.borderColor = UIColor.lightGrayColor().CGColor
        txtMessage.layer.masksToBounds = true
        txtMessage.layer.borderWidth = 0.5
        txtMessage.layer.cornerRadius = 3
        
        self.addDoneButton()
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWasShown:"), name:UIKeyboardWillShowNotification, object: nil)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil)
//

    }
    
    func keyboardWasShown(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        self.view.top = 0 - (keyboardFrame.size.height / 2)
        self.view.layoutIfNeeded()
        
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        txtMessage.inputAccessoryView = keyboardToolbar
        txtEmail.inputAccessoryView = keyboardToolbar
        txtSubtitle.inputAccessoryView = keyboardToolbar
        
    }

    
    func keyboardWillHide(notification: NSNotification){
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        self.view.top = 0
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Contact Us"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickSend(sender: AnyObject) {
        
        var alertview : UIAlertView!
        if count(txtEmail.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter email id or mobile number", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }
        
        
        if count(txtMessage.text) == 0
        {
            alertview = UIAlertView(title: "", message: "Please enter message", delegate: nil, cancelButtonTitle: "OK")
            alertview.show()
            return
        }

        
        var json = JSonConnect()
        ProgressHUD.show("Sending...", interaction: true)
        var params = ["user_id" : User.sharedInstance.currentUser.user_id,"email_id" : txtEmail.text,"subject" : txtSubtitle.text, "subject" : txtSubtitle.text,"message" : txtMessage.text]
        
        json.postPath("user/contactus", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                    var alertView = UIAlertView()
                    alertView.title = ""
                    alertView.message = result["message"] as? String
                    alertView.addButtonWithTitle("Ok")
                    alertView.show()
                    
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.addButtonWithTitle("Ok")
                        alertView.message = error["message"] as! String!
                        alertView.show()
                    }
                }
        }

        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
