//
//  AboutUsViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/8/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        var json = JSonConnect()
//        ProgressHUD.show(nil, interaction: true)
//        json.postPath("", parameters: [" " : " "], completeNetworkBlock: { (data) -> Void in
//                ProgressHUD.dismiss()
//            
//        }) { (error) -> Void in
//            ProgressHUD.dismiss()
//        }
        
        self.webView.loadHTMLString("This was my first iOS development project on Elance, and what a smooth, stress-free experience it was. Peerbits exceeded my expectations time and again on this project, and delivered a clean, high-quality app at the end of it. Their work ethic and technical skill is, I am absolutely certain, above the level of what I would receive from most app development companies in my country, and for a far more reasonable price as well! There were a number of times during the project where I asked for some changes to be made from the original app design we had discussed and every single time the development team responded to my requests. Why do they respond to these requests to changes small details for their clients? Their focus is on QUALITY. After my first experience, I completely trust in their technical skill and professionalism overall, a wonderful experience. I now look forward to developing a long-term partnership with this company",baseURL: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {

        self.navigationItem.title = "About Us"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
