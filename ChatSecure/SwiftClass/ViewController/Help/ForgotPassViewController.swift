//
//  ForgotPassViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/8/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class ForgotPassViewController: UIViewController {

    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtEmail: CustomTextField!
    
    var  animateDistance : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardNotification:", name: UIKeyboardWillChangeFrameNotification, object: nil)
//    
        self.addDoneButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: view, action: Selector("endEditing:"))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        
        txtEmail.inputAccessoryView = keyboardToolbar
    }

    
    @IBAction func clickSend(sender: AnyObject) {
        
        if count(txtEmail.text) > 0
        {
            
            var json = JSonConnect()
            let params = ["email_id" : txtEmail.text]
            ProgressHUD.show("Sending...", interaction: true)
            json.postPath("user/forgotpassword", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
                ProgressHUD.dismiss()
                if (result != nil)
                {
                    
                    let status = (result["status"] as! String!)
                    if status == status_success
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.message = result["message"] as? String
                        alertView.addButtonWithTitle("Ok")
                        alertView.show()
                        
                    }
                }
                
                }) { (error) -> Void in
                    ProgressHUD.dismiss()
                    if (error != nil)
                    {
                        let status = error["status"] as! String!
                        if status == status_error
                        {
                            var alertView = UIAlertView()
                            alertView.title = ""
                            alertView.addButtonWithTitle("Ok")
                            alertView.message = error["message"] as! String!
                            alertView.show()
                        }
                    }
            }
 
        }
        else
        {
            var alertView = UIAlertView()
            alertView.title = ""
            alertView.addButtonWithTitle("Ok")
            alertView.message = "Wrong format!!!"
            alertView.show()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "forgot password"
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.blackColor()]
        self.navigationController?.navigationBarHidden = false
        
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email id or Mobile number",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
    }
    
        func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
        
    }
}
