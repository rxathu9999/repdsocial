//
//  PeopleFollowingViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/7/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class PeopleFollowingViewController: UIViewController,UISearchBarDelegate {
    
    internal var  arrFollow = []
     var  arrBackup = []
    var backgroundNav : UIImage!
    var page = 1
    
    @IBOutlet weak var tableComment: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnRight: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnRight.addTarget(self, action: "showListNotification", forControlEvents: UIControlEvents.TouchUpInside)
        btnRight.badgeView.badgeValue = 0
        btnRight.badgeView.outlineColor =  UIColor.clearColor()
        btnRight.badgeView.outlineWidth =  1
        btnRight.badgeView.position = MGBadgePosition.TopRight
        btnRight.badgeView.badgeColor = UIColor.whiteColor()
        btnRight.badgeView.textColor =  UIColor.redColor()
        
        
        
        for view in self.searchBar.subviews
        {
            for secondview  in view.subviews
            {
                if secondview.isKindOfClass(NSClassFromString("UISearchBarBackground"))
                {
                    secondview.removeFromSuperview()
                }
                if secondview.isKindOfClass(UITextField)
                {
                    var txtField = secondview as? UITextField
                    
                    txtField!.textColor = UIColor.whiteColor()
                    txtField!.borderStyle = .None
                    txtField!.background = UIImage(named: "scope_bar")
                    txtField!.attributedPlaceholder = NSAttributedString(string:"Search",
                        attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
                    
                    
                    
                }
                
                
            }
        }
        self.searchBar.setImage(UIImage(named: "icon_search"), forSearchBarIcon: UISearchBarIcon.Search, state: UIControlState.Normal)
        self.searchBar.setImage(UIImage(named: "icon_delete"), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)
        
        self.searchBar.searchTextPositionAdjustment = UIOffsetMake(10, 0)
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "numberBadge:",
            name:"numberBadge",
            object: nil
        )
        self.loadNumberBadge()
        self.loadFollower()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(backgroundNav, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = backgroundNav
        self.navigationController?.navigationBar.translucent = true
//        self.navigationController?.navigationBarHidden = false
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        backgroundNav = self.navigationController?.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.new(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = UIImage.new()
        self.navigationController?.navigationBar.translucent = true
        
//        self.navigationController?.navigationBarHidden = true
        

        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]
        
    }
    func showListNotification()
    {
        
        //            self.setViewControllerToContainer(verifyViewController as! UITableViewController)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! OTRAppDelegate
        
        let verifyViewController = appDelegate.listNotificationVC
        
        self.navigationController?.pushViewController(verifyViewController, animated: true)
    }

    func loadNumberBadge()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! OTRAppDelegate
        
        let verifyViewController = appDelegate.listNotificationVC as! ListNotificationViewController
        verifyViewController.getCountNotification()
    }
    ///reload  number badge
    func numberBadge(notification :NSNotification)
    {
        btnRight.badgeView.badgeValue = notification.object as! Int
    }

    func loadFollower()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,
            "page" : String(page)]
        
        json.postPath("user/getfollowinglist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
             self.arrFollow = result["user"] as! NSArray
            self.arrBackup = self.arrFollow.copy() as! NSArray
            self.tableComment.reloadData()
            }) { (error) -> Void in
                       }
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrFollow.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableComment.dequeueReusableCellWithIdentifier("postCell") as? CommentViewCell
        
        if cell == nil {
            cell = CommentViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "postCell")
            
        }
        var object = self.arrFollow[indexPath.row] as! NSDictionary
        cell!.imgAvatar.layer.masksToBounds = true
        cell!.imgAvatar.layer.cornerRadius = cell!.imgAvatar.frame.size.height / 2
        
        cell!.lblName.text = object["name"] as! String!
        var imgStr = object["user_image"] as! String!
        cell!.imgAvatar.sd_setImageWithURL(NSURL(string: imgStr))
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        NSLog("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        NSLog("The default search bar keyboard search button was tapped: \(searchBar.text).")
        arrFollow = arrBackup.copy() as! NSArray
        self.tableComment.reloadData()

        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        NSLog("The default search bar cancel button was tapped.")
        arrFollow = arrBackup.copy() as! NSArray
        self.tableComment.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool // return NO to
    {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool
    {
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    func searchDisplayController(controller: UISearchDisplayController, didHideSearchResultsTableView tableView: UITableView)
    {
        self.searchDisplayController!.searchResultsTableView.hidden = false
        if self.searchDisplayController?.active == true
        {
            tableView.hidden = false
        }
        self.showTableReusltView(controller)
    }
    
    func searchDisplayControllerDidBeginSearch(controller: UISearchDisplayController)
    {
        self.showTableReusltView(controller)
    }
    
    func  showTableReusltView(controller: UISearchDisplayController)
    {
        self.searchDisplayController!.searchResultsTableView.hidden = false
        
        for v in (controller.searchResultsTableView.superview as! AnyObject).subviews
        {
            var  v1 =  v as! UIView
            if  v1.alpha < 1.0
            {
                v1.hidden = true
            }
            
        }
        
    }
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        

        return true
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if count(searchText) == 0
        {
            arrFollow = arrBackup.copy() as! NSArray
            tableComment.reloadData()
        }
        else
        {
            getSearchResult(searchText)
        }
        
    }
    
    ///call get search result
    func getSearchResult(strSearch: String!)
    {
        
        let resultPredicate1 = NSPredicate(format: "name contains[c] %@", strSearch)
        var result = self.arrBackup.filteredArrayUsingPredicate(resultPredicate1)
        self.arrFollow = result
        self.tableComment.reloadData()

    }

    
}
