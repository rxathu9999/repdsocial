//
//  ListNotificationViewController.swift
//  ChatSecure
//
//  Created by buisinam on 6/27/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit



class ListNotificationViewController: UITableViewController {

    var arrNotification = []
    var  arrAttributes = NSMutableArray()
    internal var currentNotification = 0
    var page = 1
    
    var offScreenCells: NSMutableDictionary! = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.navigationItem.title = "notification"
       
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.getCountNotification()
        self.getNotificationList()
        self.navigationItem.title = "Notification"
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Montserrat-Light", size: 18)!,NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.translucent = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }

    
  
    
    func getCountNotification(){
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id , "page" : String(page)]
        
        json.postPath("/user/getunreadnotificationcount", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {

                    NSNotificationCenter.defaultCenter().postNotificationName("numberBadge", object: result["notification_count"] as! Int)

                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    
                }
        }
    }
    
    func getNotificationList()
    {
        
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id , "page" : String(page)]
        
        json.postPath("user/getnotificationlist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            ProgressHUD.dismiss()
            if (result != nil)
            {
                let status = (result["status"] as! String!)
                if status == status_success
                {
                    self.arrNotification = result["notification"] as! NSArray
                    
                    for i in 0...self.arrNotification.count{
                        self.arrAttributes.addObject(0)
                    }
                    
                    self.tableView.reloadData()
//                    self.countUnRead()
                }
            }
            
            }) { (error) -> Void in
                ProgressHUD.dismiss()
                if (error != nil)
                {
                    let status = error["status"] as! String!
                    if status == status_error
                    {
                        var alertView = UIAlertView()
                        alertView.title = ""
                        alertView.addButtonWithTitle("Ok")
                        alertView.message = error["message"] as! String!
                        alertView.show()
                    }
                }
        }

    }
   
    func countUnRead()
    {
          let resultPredicate1 = NSPredicate(format: "is_read = n", "")
        var arrUnread = self.arrNotification.filteredArrayUsingPredicate(resultPredicate1)
       

        
        NSNotificationCenter.defaultCenter().postNotificationName("numberBadge", object: arrUnread.count)


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return arrNotification.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("notificationcell", forIndexPath: indexPath) as! CommentViewCell
//        var object = self.arrNotification[indexPath.row] as! NSDictionary
//        // Configure the cell...
//        cell.imgAvatar.layer.masksToBounds = true
//        cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.frame.size.height / 2
//        var urlImage = object["image"] as! String
//        cell.imgAvatar.sd_setImageWithURL(NSURL(string: urlImage))
//       
//                  if  let  object = arrAttributes[indexPath.row] as? NSAttributedString
//            {
//                cell.lblComment.attributedText = object
//            }
//            else
//            {
//                 cell.lblComment.text = object["notification_text"] as? String
//                self.htmlString(cell.lblComment.text!,index: indexPath.row)
//            }
//
//        
//        
//        
//        
//        
//
//        cell.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",(object["date"] as? Int)!)))
//        var is_read = object["is_read"] as? String
        let cell = self.tableView.dequeueReusableCellWithIdentifier("notificationcell") as! CommentViewCell
        
        self.configureBasicCell(cell, atIndexPath: indexPath)

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//        let  followVC = Utilities.viewTableController("", onStoryboard: "Home")
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        /*If you just want to support ios8+,
        
        return UITableViewAutomaticDimension
        
        only.
        */
        
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            return UITableViewAutomaticDimension
        }
        
        return self.heightForBasicCellAtIndexPath(indexPath)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    func htmlString(label : String, index : Int)-> NSAttributedString
        {
            // Create the html body
            var attributedHTMLBody = NSAttributedString(data: label.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: false)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil, error: nil)

            // Create the string including the text style
            var htmlString = String(format: "<html><body style='font-family:Arial; font-size:13px;'><div>%@</div></body></html>", label)
            
            // Create the final text to display
            var attributedHML = NSAttributedString(data: htmlString.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: false)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil, error: nil)
            arrAttributes.replaceObjectAtIndex(index, withObject: attributedHML!)
            tableView.reloadData()
            // Set the text
            return attributedHML!
            
        }
    func heightForBasicCellAtIndexPath(indexPath: NSIndexPath)-> CGFloat {
        
        var cell = offScreenCells.objectForKey("kkk") as? CommentViewCell
        if cell == nil {
            cell = self.tableView.dequeueReusableCellWithIdentifier("notificationcell") as? CommentViewCell
            self.offScreenCells.setObject(cell!, forKey: "kkk")
        }
        self.configureBasicCell(cell!, atIndexPath: indexPath)
        return self.calculateHeightForConfiguredSizingCell(cell!)
    }
    
    func calculateHeightForConfiguredSizingCell(cell: CommentViewCell) -> CGFloat {
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let height = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingExpandedSize).height + 1.0
        return height
        
    }
    
    func configureBasicCell(cell: CommentViewCell, atIndexPath indexPath:NSIndexPath){
        var object = self.arrNotification[indexPath.row] as! NSDictionary
        // Configure the cell...
        cell.imgAvatar.layer.masksToBounds = true
        cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.frame.size.height / 2
        var urlImage = object["image"] as! String
        cell.imgAvatar.sd_setImageWithURL(NSURL(string: urlImage))
        cell.lblComment.preferredMaxLayoutWidth = CGRectGetWidth(cell.lblComment.bounds)
      

        if  let  object = arrAttributes[indexPath.row] as? NSAttributedString
        {
            cell.lblComment.attributedText = object
        }
        else
        {
            cell.lblComment.text = object["notification_text"] as? String
            self.htmlString(cell.lblComment.text!,index: indexPath.row)
        }
        
        
        
        
        
        
        
        cell.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",(object["date"] as? Int)!)))
        var is_read = object["is_read"] as? String
        
    }
}











