//
//  DetailPostViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/2/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class DetailPostViewController: UIViewController {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    
    @IBOutlet weak var tableDetail: UITableView!
    internal var detailDict : PostObj!
    var arrGallery : NSMutableArray!
    var backgroundNav : UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.tableDetail.estimatedRowHeight = 60
        self.tableDetail.rowHeight = UITableViewAutomaticDimension
        self.arrGallery = NSMutableArray(array: detailDict.images )
        self.tableDetail.reloadData()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: self, action: nil)
        
        // Configure the cell...
        lblName.text = detailDict.owner
        lblStatus.text = detailDict.caption
       lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",detailDict.inserted_date)))
        imgAvatar.layer.masksToBounds = true
        imgAvatar.layer.cornerRadius  = imgAvatar.frame.size.height / 2
        imgAvatar.sd_setImageWithURL(NSURL(string: detailDict.owner_image), placeholderImage: UIImage.new())

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        self.navigationController?.navigationBarHidden = false
        
        self.navigationController?.navigationBar.setBackgroundImage(backgroundNav, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = backgroundNav
        self.navigationController?.navigationBar.translucent = true
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.tintColor = UIColor.blackColor()
        backgroundNav = self.navigationController?.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.new(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = UIImage.new()
        self.navigationController?.navigationBar.translucent = true
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        
         return  arrGallery.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
//        if indexPath.row == 0
//        {
//        
//            var cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as! PostViewCell
//            var postObj = detailDict
//           
//            // Configure the cell...
//            cell.lblName.text = postObj.owner
//            cell.lblStatus.text = postObj.caption
//            cell.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",postObj.inserted_date)))
//            cell.imgAvatar.layer.masksToBounds = true
//            cell.imgAvatar.layer.cornerRadius  = cell.imgAvatar.frame.size.height / 2
//            cell.imgAvatar.sd_setImageWithURL(NSURL(string: postObj.owner_image), placeholderImage: UIImage.new())
//
//            return cell
//        }
//        else
//        {
        
             var cell = tableView.dequeueReusableCellWithIdentifier("postCell") as? PostViewCell
            
            if cell == nil {
                cell = PostViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "postCell")
                
            }

            var postObj: AnyObject = self.arrGallery[indexPath.row ]
            
            // Configure the cell...
            cell!.lblNumberLike.text = String(format: "%d like",(postObj["image_likes"] as? Int)!)
//            cell!.imgMain.sd_setImageWithURL(NSURL(string: (postObj["image_original"] as? String!)!), placeholderImage: UIImage(named: "placeholder_NonImage"))

        cell!.btnComment.tag = indexPath.row
        cell!.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
            var url = NSURL(string: (postObj["image_original"] as? String!)!)
            
            var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
            if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
            {
                 let imageAspectRatio = image.size.width / cell!.bg_view.frame.size.width
                cell!.heightImgMain.constant = image.size.height / imageAspectRatio
                cell!.imgMain.image = image
                cell!.updateConstraints()

                
            }
            else
            {
                
                weak var wself  = self
                cell!.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        if !(wself != nil)
                        {
                            return
                        }
                        if (image != nil)
                        {
                            var strongself : DetailPostViewController = wself!
                            strongself.tableDetail.reloadData()
                            
                        }
                       
                        
                        
                        
                    
                    })
                    
                })
            }
            
            return cell!
            
        
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
                  var postObj: AnyObject = self.arrGallery[indexPath.row]
            println(postObj)
            var detailVC =  Utilities.viewController("CommentsViewController", onStoryboard: "Home") as! CommentsViewController
            detailVC.post_id = String(self.detailDict.post_id)
            detailVC.image_id = String(postObj["id"] as! Int)
            self.navigationController?.pushViewController(detailVC, animated: true)
     
    }
    
    func commentThePost(sender : UIButton)
    {
        var postObj: AnyObject = self.arrGallery[sender.tag]
        println(postObj)
        var detailVC =  Utilities.viewController("CommentsViewController", onStoryboard: "Home") as! CommentsViewController
        detailVC.post_id = String(self.detailDict.post_id)
        detailVC.image_id = String(postObj["id"] as! Int)
        self.navigationController?.pushViewController(detailVC, animated: true)

    }
    
    @IBAction func clickBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        /*If you just want to support ios8+,
        
        return UITableViewAutomaticDimension
        
        only.
        */
        
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            return UITableViewAutomaticDimension
        }
        return self.heightForBasicCellAtIndexPath(indexPath)
    }
    
    var offScreenCells: NSMutableDictionary! = NSMutableDictionary()
    
    func heightForBasicCellAtIndexPath(indexPath: NSIndexPath)-> CGFloat {
        var cell = self.offScreenCells.objectForKey("kkk") as? PostViewCell
        if cell == nil {
            cell = tableDetail.dequeueReusableCellWithIdentifier("postCell") as? PostViewCell
            self.offScreenCells.setObject(cell!, forKey: "kkk")
        }
        self.configureBasicCell(cell!, atIndexPath: indexPath)
        return self.calculateHeightForConfiguredSizingCell(cell!)
    }
    
    func configureBasicCell(cell: PostViewCell, atIndexPath indexPath:NSIndexPath){
    
        var postObj: AnyObject = self.arrGallery[indexPath.row ]
        
        // Configure the cell...
        cell.lblNumberLike.text = String(format: "%d like",(postObj["image_likes"] as? Int)!)
        //            cell!.imgMain.sd_setImageWithURL(NSURL(string: (postObj["image_original"] as? String!)!), placeholderImage: UIImage(named: "placeholder_NonImage"))
        
        cell.btnComment.tag = indexPath.row
        cell.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
        var url = NSURL(string: (postObj["image_original"] as? String!)!)
        
        var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
        if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
        {
            let imageAspectRatio = image.size.width / cell.bg_view.frame.size.width
            cell.heightImgMain.constant = image.size.height / imageAspectRatio
            cell.imgMain.image = image
            cell.updateConstraints()
            
            
        }
        else
        {
            
            weak var wself  = self
            cell.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
                
                
                dispatch_async(dispatch_get_main_queue(), {
                    if !(wself != nil)
                    {
                        return
                    }
                    if (image != nil)
                    {
                        var strongself : DetailPostViewController = wself!
                        strongself.tableDetail.reloadData()
                        
                    }
                    
                    
                    
                    
                    
                })
                
            })
        }

    }
    
    func calculateHeightForConfiguredSizingCell(cell: PostViewCell) -> CGFloat {
        
        cell.updateConstraintsIfNeeded()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let height = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingExpandedSize).height + 1.0
        return height
        
    }
}
