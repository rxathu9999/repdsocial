//
//  ConcernViewController.swift
//  ChatSecure
//
//  Created by buisinam on 7/6/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class CommentsViewController: SLKTextViewController {
    

    
    var user_id : String! = ""
    var post_id : String! = ""
    var image_id : String! = ""
    var page = 1
    var arrComment = []
    @IBOutlet var tableComment: UITableView!
    
    var offScreenCells: NSMutableDictionary! = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableComment.estimatedRowHeight = 60
        self.tableComment.rowHeight = UITableViewAutomaticDimension
            var f = self.navigationController?.navigationBar.frame.size
        self.navigationItem.title = "Comment"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
//      self.navigationController?.navigationBar.setBackgroundImage(Utilities.getImageWithColor(UIColor.redColor(), size: f!), forBarMetrics: UIBarMetrics.Default)
//        self.navigationController?.navigationBar.shadowImage = UIImage.new()
//        self.navigationController?.navigationBar.translucent = true
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.navigationController?.navigationBar.alpha = 1
        

        self.bounces = true
        self.shakeToClearEnabled = true
        self.keyboardPanningEnabled = true
        self.inverted = false
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        self.tableComment.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        
        self.textView.placeholder = "Add comment"
        self.textView.placeholderColor = UIColor.whiteColor()
        
        self.rightButton.tintColor = COLOR_APP
        self.rightButton.setTitleColor(COLOR_APP, forState: UIControlState.Normal)
        self.rightButton.setTitle("SEND", forState: UIControlState.Normal)
        
        self.textInputbar.autoHideRightButton = false
        self.textInputbar.backgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
        self.textInputbar.barTintColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
        self.textInputbar.translucent = false

        self.textView.backgroundColor = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1)
        
        
        self.textView.textColor = COLOR_APP
        self.textView.tintColor  = COLOR_APP
        self.textView.layer.borderWidth = 0
        self.textView.layer.cornerRadius = 3
        self.textInputbar.clipsToBounds = true
         
        
        self.loadCommentList()
    }
    
    override func viewWillAppear(animated: Bool) {
                self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: delegate SLKViewcontroller
    
    override func canPressRightButton() -> Bool {
        return super.canPressRightButton()
    }
    
    override func didPressRightButton(sender: AnyObject!) {
      
        self.textView.resignFirstResponder()
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,"post_id" : post_id,"image_id" : image_id,
            "access_token" : User.sharedInstance.currentUser.token,
        "comment" : self.textView.text,
            "tagged_user_id" : []]
        self.textView.text = ""
        json.postPath("comment/index", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            let status = (result["status"] as! String!)
            if status == status_success
            {
                

               self.loadCommentList()
            }
            
            }) { (error) -> Void in
                
        }

    }
    
    
    func loadCommentList()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,"post_id" : post_id,"image_id" : image_id,
            "page" : String(page)]
        
        json.postPath("comment/commentlist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
                self.arrComment = result["comment"] as! NSArray
            self.tableView.reloadData()
//            self.tableComment.reloadData()
            }) { (error) -> Void in
                
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
       return arrComment.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
//        var cell = tableComment.dequeueReusableCellWithIdentifier("postCell") as? CommentViewCell
//        
//        if cell == nil
//        {
//            
//            var nib = NSBundle.mainBundle().loadNibNamed("CommentViewCell", owner: nil, options: nil)
//            
//            for  oneObject in nib
//            {
//                if oneObject.isKindOfClass(CommentViewCell)
//                {
//                    cell = oneObject as? CommentViewCell
//                }
//            }
//            
//            
//            
//            
//            
//        }
        
        let cell = tableComment.dequeueReusableCellWithIdentifier("postCell") as! CommentViewCell
        
        self.configureBasicCell(cell, atIndexPath: indexPath)
        
//        var object = self.arrComment[indexPath.row] as! NSMutableDictionary
//        cell!.lblComment.text = object["comment"] as? String
//        cell!.lblName.text = object["owner"] as? String
//        
//        var imgStr = object["owner_image"] as! String!
//        cell!.imgAvatar.sd_setImageWithURL(NSURL(string: imgStr))
//        cell!.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",object["inserted_date"] as! Int)))
        return cell
    }
    
//    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//        /*If you just want to support ios8+,
//        
//        return UITableViewAutomaticDimension
//        
//        only.
//        */
//        
//        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
//            return UITableViewAutomaticDimension
//        }
//        else
//        {
//            
//            var cell = tableComment.dequeueReusableCellWithIdentifier("postCell") as? CommentViewCell
//            if cell == nil
//            {
//                
//                var nib = NSBundle.mainBundle().loadNibNamed("CommentViewCell", owner: nil, options: nil)
//                
//                for  oneObject in nib
//                {
//                    if oneObject.isKindOfClass(CommentViewCell)
//                    {
//                        cell = oneObject as? CommentViewCell
//                    }
//                }
//                
//               
//               
//
//                
//            }
//            
//            var object = self.arrComment[indexPath.row] as! NSMutableDictionary
//            cell!.lblComment.text = object["comment"] as? String
//            cell!.lblName.text = object["owner"] as? String
//            cell!.lblComment.preferredMaxLayoutWidth = CGRectGetWidth(cell!.lblComment.bounds)
//            cell!.setNeedsLayout()
//            cell!.layoutIfNeeded()
//            cell!.contentView.layoutIfNeeded()
//            let height = cell!.contentView.systemLayoutSizeFittingSize(UILayoutFittingExpandedSize).height + 1.0
//           return height
//        }
//        
//        
//        
//    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
      
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        /*If you just want to support ios8+,
        
        return UITableViewAutomaticDimension
        
        only.
        */
        
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            return UITableViewAutomaticDimension
        }
        
        return self.heightForBasicCellAtIndexPath(indexPath)
    }
    
    
    /********All methods below is not necessary if you want to support ios8+ ***********/
    
    func calculateHeightForConfiguredSizingCell(cell: CommentViewCell) -> CGFloat {
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let height = cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingExpandedSize).height + 1.0
        return height
        
    }
    func configureBasicCell(cell: CommentViewCell, atIndexPath indexPath:NSIndexPath){
        var object = self.arrComment[indexPath.row] as! NSMutableDictionary
        cell.lblComment.preferredMaxLayoutWidth = CGRectGetWidth(cell.lblComment.bounds)
        println(cell.lblComment)
        
       cell.lblComment.text = object["comment"] as? String
        cell.lblName.text = object["owner"] as? String
        var imgStr = object["owner_image"] as! String!
        cell.imgAvatar.sd_setImageWithURL(NSURL(string: imgStr))
        cell.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",object["inserted_date"] as! Int)))


    }
    
    func heightForBasicCellAtIndexPath(indexPath: NSIndexPath)-> CGFloat {
        
        var cell = self.offScreenCells.objectForKey("kkk") as? CommentViewCell
        if cell == nil {
            cell = tableComment.dequeueReusableCellWithIdentifier("postCell") as? CommentViewCell
            self.offScreenCells.setObject(cell!, forKey: "kkk")
        }
        self.configureBasicCell(cell!, atIndexPath: indexPath)
        return self.calculateHeightForConfiguredSizingCell(cell!)
    }


}
