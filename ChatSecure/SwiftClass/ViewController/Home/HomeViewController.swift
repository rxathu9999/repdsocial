//
//  HomeViewController.swift
//  ChatSecure
//
//  Created by buisinam on 6/27/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit


struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}
class HomeViewController: UIViewController,UISearchBarDelegate,UICollectionViewDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate
{
    
    
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tablePost: UITableView!
    
    @IBOutlet weak var collectFollow: UICollectionView!
    
    @IBOutlet weak var viewFollw: UIView!
    @IBOutlet weak var heightFollowView: NSLayoutConstraint!
    
    var backgroundNav : UIImage!
    
    var page = 1
    var arrUserFollow  = []
    var arrUserFollowBackup  = []
    var arrPost = []
    var existHeighFollowView  = 0.0
    
    var friendsArray = []
    var filteredFriends = []
    var offScreenCells: NSMutableDictionary! = NSMutableDictionary()
    
    var arrHeightEachCell = [Int : Float]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnRight.addTarget(self, action: "showListNotification", forControlEvents: UIControlEvents.TouchUpInside)
        btnRight.badgeView.badgeValue = 0
        btnRight.badgeView.outlineColor =  UIColor.clearColor()
        btnRight.badgeView.outlineWidth =  1
        btnRight.badgeView.position = MGBadgePosition.TopRight
        btnRight.badgeView.badgeColor = UIColor.whiteColor()
        btnRight.badgeView.textColor =  UIColor.redColor()
        
//        self.searchDisplayController?.searchResultsTableView.registerClass(PostViewCell.self, forCellReuseIdentifier: "postCell")
        
        for view in self.searchDisplayController!.searchBar.subviews
        {
            for secondview  in view.subviews
            {
                if secondview.isKindOfClass(NSClassFromString("UISearchBarBackground"))
                {
                    secondview.removeFromSuperview()
                }
                if secondview.isKindOfClass(UITextField)
                {
                    var txtField = secondview as? UITextField
                    
                    txtField!.textColor = UIColor.whiteColor()
                    txtField!.borderStyle = .None
                    txtField!.background = UIImage(named: "scope_bar")
                    txtField!.attributedPlaceholder = NSAttributedString(string:"Search",
                        attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
                    
                    

                }

                
            }
        }
       self.searchDisplayController!.searchBar.setImage(UIImage(named: "icon_search"), forSearchBarIcon: UISearchBarIcon.Search, state: UIControlState.Normal)
        self.searchDisplayController!.searchBar.setImage(UIImage(named: "icon_delete"), forSearchBarIcon: UISearchBarIcon.Clear, state: UIControlState.Normal)

        self.searchDisplayController!.searchBar.searchTextPositionAdjustment = UIOffsetMake(10, 0)
        self.searchDisplayController!.delegate = self
        self.tablePost.estimatedRowHeight = 60
        self.tablePost.rowHeight = UITableViewAutomaticDimension
        self.searchDisplayController!.searchResultsTableView.estimatedRowHeight = 60
        self.searchDisplayController!.searchResultsTableView.rowHeight = UITableViewAutomaticDimension
        existHeighFollowView = Double(self.heightFollowView.constant)

       self.loadListPost()
        self.loadFollower()
        
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "numberBadge:",
            name:"numberBadge",
            object: nil
        )
        self.loadNumberBadge()
         tablePost.registerNib(UINib(nibName: "PostViewCell", bundle: nil), forCellReuseIdentifier: "nibCell")
        tablePost.addPullToRefreshWithAction({
            NSOperationQueue().addOperationWithBlock {
                var json = JSonConnect()
                let params = ["user_id" : User.sharedInstance.currentUser.user_id, "page" : String(self.page), "get_own_post" : "n"]
                
                
                
                json.postPath("post/postlist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
                    self.arrPost = result["post"] as! NSArray
                    self.tablePost.reloadData()
                        self.tablePost.stopPullToRefresh()
                    }) { (error) -> Void in
                        self.tablePost.stopPullToRefresh()
                }
            }
            }, withAnimator: Animator())


    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.navigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(backgroundNav, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = backgroundNav
        self.navigationController?.navigationBar.translucent = true

//         self.navigationController?.navigationBar.alpha = 1
                self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        
        backgroundNav = self.navigationController?.navigationBar.backgroundImageForBarMetrics(UIBarMetrics.Default)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.new(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = UIImage.new()
        self.navigationController?.navigationBar.translucent = true

        self.navigationController?.navigationBarHidden = true
    }
    
    
    ///call get numberBadget
    func loadNumberBadge()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! OTRAppDelegate
        
        let verifyViewController = appDelegate.listNotificationVC as! ListNotificationViewController
        verifyViewController.getCountNotification()
    }
    
    ///reload  number badge
    func numberBadge(notification :NSNotification)
    {
        btnRight.badgeView.badgeValue = notification.object as! Int
    }
    
    func loadFollower()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id,
            "page" : String(page)]
        
        json.postPath("user/getfollowinglist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            self.arrUserFollow = result["user"] as! NSArray
            self.arrUserFollowBackup = self.arrUserFollow.copy() as! NSArray
            self.collectFollow.reloadData()

            }) { (error) -> Void in
//                Utilities.showErrorStatus(error)
//                self.heightFollowView.constant = 0
//                self.view.updateConstraintsIfNeeded()
        }

    }
    
    func loadListPost()
    {
        var json = JSonConnect()
        
        let params = ["user_id" : User.sharedInstance.currentUser.user_id, "page" : String(page), "get_own_post" : "n"]
        

        
        json.postPath("post/postlist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
            self.arrPost = result["post"] as! NSArray
            self.tablePost.reloadData()
        }) { (error) -> Void in

        }
    }
    
    // MARK: - Table view data source

     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if (tableView == self.searchDisplayController?.searchResultsTableView)
        {
            return arrUserFollow.count

        }
        else
        {
//            if arrPost.count > 0
//            {
//                return 2
//            }
            return arrPost.count
        }
    }

    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (tableView == self.searchDisplayController?.searchResultsTableView)
        {
            var cell = tablePost.dequeueReusableCellWithIdentifier("followCell") as? CommentViewCell
            
            if cell == nil {
                cell = CommentViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "postCell")
                
            }
            var object = self.arrUserFollow[indexPath.row] as! NSDictionary
            cell!.imgAvatar.layer.masksToBounds = true
            cell!.imgAvatar.layer.cornerRadius = cell!.imgAvatar.frame.size.height / 2
            
            cell!.lblName.text = object["name"] as! String!
            var imgStr = object["user_image"] as! String!
            cell!.imgAvatar.sd_setImageWithURL(NSURL(string: imgStr))
            return cell!

        }
        else
        {
        
            
            if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
                var cell = tablePost.dequeueReusableCellWithIdentifier("postCell",forIndexPath: indexPath) as? PostViewCell

                    var postObj = PostObj()
                    postObj.setValuesForKeysWithDictionary(self.arrPost[indexPath.row] as! [NSObject : AnyObject])
                // Configure the cell...
               
                cell!.btnComment.tag = indexPath.row
                cell!.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
                cell!.lblName.text = postObj.owner
                cell!.lblStatus.text = postObj.caption
                cell!.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",postObj.inserted_date)))
                cell!.lblNumberLike.text = String(format: "%d like",postObj.post_likes)
                cell!.imgAvatar.layer.masksToBounds = true
                cell!.imgAvatar.layer.cornerRadius  = cell!.imgAvatar.frame.size.height / 2
                cell!.imgAvatar.sd_setImageWithURL(NSURL(string: postObj.owner_image), placeholderImage: UIImage.new())
                if postObj.images.count ==  0
                {
                    cell!.heightCollect.constant = 0
                    cell!.heightImgMain.constant = 0
                    cell!.updateConstraintsIfNeeded()
                }else
                {
                   
                    let arrGallery = NSMutableArray(array: postObj.images )
                    if arrGallery.count > 1
                    {
                        cell?.imgMain.image = nil
                        cell!.setup()
                        cell!.arrGallery = NSMutableArray(array: postObj.images )
                        
                        if cell!.arrGallery.count >= 4
                        {
                            var witdth = cell!.imgMain.width
                            cell!.heightCollect.constant = cell!.imgMain.width
                            cell!.heightImgMain.constant = cell!.imgMain.width
                            cell!.updateConstraints()
                            cell!.collectImage.reloadData()
                            return cell!

                        }
                        else
                        {
                            cell!.heightCollect.constant = 195
                            cell!.heightImgMain.constant = 195

                            cell!.updateConstraints()
                            cell!.collectImage.reloadData()
                            return cell!

                        }
                        

                    }
                    

                    else
                    {
                        
                        var imgobject: AnyObject = arrGallery[0]
                        var url = NSURL(string: (imgobject["image_original"] as? String!)!)
                        
                        var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
                        if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
                        {
                            let imageAspectRatio = image.size.width / cell!.imgMain.frame.size.width
                            cell!.heightImgMain.constant = image.size.height / imageAspectRatio
                             cell!.heightCollect.constant = cell!.heightImgMain.constant
    //                        cell!.heightImgMain.constant = cell!.heightImgMain.constant
                            cell!.imgMain.image = image
                            cell!.updateConstraints()
                            return cell!
                            
                        }
                        else
                        {
                            weak var wself  = cell
                           cell!.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
                            
                                    if (image != nil)
                                    {
                                        var strongself : PostViewCell = wself!

                                        strongself.updateConstraintsIfNeeded()
                                        
                                    }
 
                            })
                            return cell!

                        }
                    }
                }
                return cell!
            }
            else
            {
                let cell = tablePost.dequeueReusableCellWithIdentifier("nibCell",forIndexPath: indexPath) as? PostViewCell
                var postObj = PostObj()
                postObj.setValuesForKeysWithDictionary(self.arrPost[indexPath.row] as! [NSObject : AnyObject])
                
                cell!.btnComment.tag = indexPath.row
                cell!.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
                cell!.lblName.text = postObj.owner
                
                cell!.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",postObj.inserted_date)))
                cell!.lblNumberLike.text = String(format: "%d like",postObj.post_likes)
                cell!.imgAvatar.layer.masksToBounds = true
                cell!.imgAvatar.layer.cornerRadius  = cell!.imgAvatar.frame.size.height / 2
                cell!.imgAvatar.sd_setImageWithURL(NSURL(string: postObj.owner_image), placeholderImage: UIImage.new())
                
                cell!.lblStatus.text = postObj.caption
                cell!.imgMain.hidden = false
                cell!.collectImage.hidden = false
                cell!.setup()
                if postObj.images.count ==  0
                {
                    cell!.imgMain.hidden = true
                    cell!.collectImage.hidden = true
                    cell!.imgMain.height = 0
                    cell!.collectImage.height = 0
                    cell!.collectImage.reloadData()
                    cell!.reloadCell()
                    return cell!

                }
                else
                {
                    
                    
                    if  postObj.images.count == 1
                    {
                        cell!.imgMain.hidden = false
                        cell!.collectImage.hidden = true
                        
                        var imgobject: AnyObject = postObj.images[0]
                        var url = NSURL(string: (imgobject["image_original"] as? String!)!)
                        
                        var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
                        if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
                        {
                            var imageAspectRatio = image.size.width / cell!.imgMain.width
                            cell!.imgMain.height = image.size.height / imageAspectRatio
                            cell!.imgMain.image = image

                            cell!.reloadCell()
                            return cell!
                        }
                        else
                        {
                            
                            
                            var wself  = cell
                            cell!.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
                                if (image != nil)
                                {
                                   
                            
                                    self.tablePost.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                                    
                                    
                                }
                                
                                
                            })
                        }
                        
                    }
                    else
                    {
                        cell!.imgMain.hidden = true
                        cell!.collectImage.hidden = false
                        
                       
                        cell!.collectImage.height = cell!.bg_view.width
                        println(cell!.collectImage.height)
                        cell!.arrGallery = (NSMutableArray(array: postObj.images))
                        cell!.collectImage.reloadData()
                        cell!.reloadCell()
                        return cell!
                    }
                }
               
                
                

                
                return cell!
            }
            
        }
    }
    
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
//    {
//        if (tableView == self.searchDisplayController?.searchResultsTableView)
//        {
//            return
//        }
//    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if (tableView != self.searchDisplayController?.searchResultsTableView)
        {

        
            var postObj = PostObj()
            postObj.setValuesForKeysWithDictionary(self.arrPost[indexPath.row] as! [NSObject : AnyObject])
            
            if postObj.images.count > 0
            {
                var detailVC =  Utilities.viewController("DetailPostViewController", onStoryboard: "Home") as! DetailPostViewController
                detailVC.detailDict = postObj
                self.navigationController?.pushViewController(detailVC, animated: true)
            }
            else
            {
                var detailVC =  Utilities.viewController("CommentsViewController", onStoryboard: "Home") as! CommentsViewController
                detailVC.post_id = String(postObj.post_id)

                self.navigationController?.pushViewController(detailVC, animated: true)

            }
        }
        
    }
    
    
    func commentThePost(sender : UIButton)
    {
        var postObj = PostObj()
        postObj.setValuesForKeysWithDictionary(self.arrPost[sender.tag] as! [NSObject : AnyObject])

        var detailVC =  Utilities.viewController("CommentsViewController", onStoryboard: "Home") as! CommentsViewController
        detailVC.post_id = String(postObj.post_id)
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        /*If you just want to support ios8+,
        
        return UITableViewAutomaticDimension
        
        only.
        */
        
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            return UITableViewAutomaticDimension
        }
        else if (tableView == self.searchDisplayController?.searchResultsTableView)
        {
                return 69
        }
    
        return self.heightForBasicCellAtIndexPath(indexPath)

        


    }
    
    
    /********All methods below is not necessary if you want to support ios8+ ***********/
    
    func calculateHeightForConfiguredSizingCell(cell: PostViewCell,index : Int) -> CGFloat {


        
        var postObj = PostObj()
        postObj.setValuesForKeysWithDictionary(self.arrPost[index] as! [NSObject : AnyObject])
        
        
        cell.lblStatus.text = postObj.caption
        cell.lblStatus.autosizeForWidth(Float(cell.lblStatus.width))
        
        if postObj.images.count ==  0
         {
            cell.imgMain.height = 0
            cell.collectImage.height = 0
         }
         else
         {
            
            
            if  postObj.images.count == 1
            {
                var imgobject: AnyObject = postObj.images[0]
                var url = NSURL(string: (imgobject["image_original"] as? String!)!)
                
                var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
                if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
                {
                    var imageAspectRatio = image.size.width / cell.imgMain.width
                    cell.imgMain.height = image.size.height / imageAspectRatio
                    cell.imgMain.image = image
                    
                }

            }
            else
            {
                cell.imgMain.height =   cell.imgMain.width
                cell.collectImage.height = cell.imgMain.width
            }
        }
        cell.reloadCell()
        return cell.imgMain.bottom + 50 //distance to bottom
        
        
    }
    func configureBasicCell(cell: PostViewCell, atIndexPath indexPath:NSIndexPath){
        
        
//        var postObj = PostObj()
//        postObj.setValuesForKeysWithDictionary(self.arrPost[indexPath.row] as! [NSObject : AnyObject])
//        cell.lblStatus.preferredMaxLayoutWidth = CGRectGetWidth(cell.lblStatus.bounds)
//        cell.lblStatus.text = postObj.caption
//        
//        cell.btnComment.tag = indexPath.row
//        cell.btnComment.addTarget(self, action: "commentThePost:", forControlEvents: UIControlEvents.TouchUpInside)
//        cell.lblName.text = postObj.owner
//        
//
//        
//
//        cell.lblTime.text = fuzzyTime(GetDateFromTimeStamp(String(format: "%d",postObj.inserted_date)))
//        cell.lblNumberLike.text = String(format: "%d like",postObj.post_likes)
//        cell.imgAvatar.layer.masksToBounds = true
//        cell.imgAvatar.layer.cornerRadius  = cell.imgAvatar.frame.size.height / 2
//        cell.imgAvatar.sd_setImageWithURL(NSURL(string: postObj.owner_image), placeholderImage: UIImage.new())
//
//        if postObj.images.count ==  0
//        {
//            cell.heightCollect.constant = 0
//            cell.heightImgMain.constant = 0
//        }
//        else
//        {
//            
//            
//
//            
//            if  postObj.images.count > 1
//            {
//                cell.imgMain.hidden =  true
//                cell.collectImage.hidden = false
//                if cell.arrGallery == nil
//                {
//                        cell.setup(NSMutableArray(array: postObj.images))
//                    
//
//                        var witdth =  self.view.width
//                        self.arrHeightEachCell[indexPath.row] = (Float(witdth - cell.imgMain.left - cell.imgMain.right))
//                        cell.heightCollect.constant = CGFloat(witdth)
//                        cell.heightImgMain.constant = CGFloat(witdth)
//                        cell.collectImage.height = (witdth)
//                    cell.setNeedsLayout()
//                    cell.layoutIfNeeded()
//                        self.arrHeightEachCell[indexPath.row] = (Float(cell.heightImgMain.constant))
//                        cell.collectImage.reloadData()
//
//                }
//                else
//                {
//                    var witdth =  self.arrHeightEachCell[indexPath.row]
//                    if  witdth != nil
//                    {
//                        cell.heightCollect.constant = CGFloat(witdth!)
//                        cell.heightImgMain.constant = CGFloat(witdth!)
//                    }
//
//                }
//                
//                
//                
//            }
//            else
//            {
//                cell.imgMain.hidden = false
//                cell.collectImage.hidden = true
//                if cell.arrGallery == nil
//                {
//
//                    cell.setup([])
//                    cell.arrGallery = NSMutableArray(array: postObj.images)
//                }
//                if let height: Float = arrHeightEachCell[indexPath.row]{
//                    cell.heightImgMain.constant = CGFloat(height)
//                    var imgobject: AnyObject = cell.arrGallery[0]
//
//                    cell.imgMain.sd_setImageWithURL(NSURL(string: imgobject["image_original"] as! String))
//                } else {
//                    
//
//                
//                    var imgobject: AnyObject = cell.arrGallery[0]
//                    var url = NSURL(string: (imgobject["image_original"] as? String!)!)
//                    
//                    
//                    
//                    
//                    var key = SDWebImageManager.sharedManager().cacheKeyForURL(url)
//                    if  let image = SDImageCache.sharedImageCache().imageFromDiskCacheForKey(key)
//                    {
//                        var imageAspectRatio = image.size.width / cell.imgMain.frame.size.width
//                        arrHeightEachCell[indexPath.row] = (Float(image.size.height / imageAspectRatio))
//                        cell.heightImgMain.constant = CGFloat(arrHeightEachCell[indexPath.row]!)
//
//                        cell.imgMain.image = image
//                        
//                    }
//
//                    else
//                    {
//
//                        
//                         var wself  = cell
//                        cell.imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "placeholder_NonImage"), options: nil, completed: { (image, error, cache, url) -> Void in
//                            if (image != nil)
//                            {
//                                //                            self.offScreenCells.removeObjectForKey("kkk")
//                                
//                                var imageAspectRatio = image.size.width / cell.imgMain.frame.size.width
//                                self.arrHeightEachCell[indexPath.row] = (Float(image.size.height / imageAspectRatio))
//                                cell.heightImgMain.constant = CGFloat(self.arrHeightEachCell[indexPath.row]!)
//                                
//                                cell.imgMain.image = image
//                                
//                                var strongself : PostViewCell = wself
//                                strongself.layoutIfNeeded()
//                                strongself.layoutSubviews()
//                                
//
//                                
//                            }
//                            
//                            
//                        })
//                    }
//                }
//            }
//            
//        }
        
//        var cell = tablePost.dequeueReusableCellWithIdentifier("nibCell") as? PostViewCell
        






        
    }
    
    
    
    func heightForBasicCellAtIndexPath(indexPath: NSIndexPath)-> CGFloat {
        
        var cell = tablePost.dequeueReusableCellWithIdentifier("nibCell") as? PostViewCell
        
//        self.configureBasicCell(cell!, atIndexPath: indexPath)
        return self.calculateHeightForConfiguredSizingCell(cell!,index: indexPath.row)
    }
       /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickRing(sender: AnyObject) {
        self.showListNotification()
    }
    
    func showListNotification()
    {
        
        //            self.setViewControllerToContainer(verifyViewController as! UITableViewController)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! OTRAppDelegate
        
        let verifyViewController = appDelegate.listNotificationVC
        
        self.navigationController?.pushViewController(verifyViewController, animated: true)
    }
    
    
    
    // MARK: - CollectionView   
    func numberOfSectionsInCollectionView(collectionView:
        UICollectionView!) -> Int {
            return 1
    }
    
    func collectionView(collectionView: UICollectionView!,
        numberOfItemsInSection section: Int) -> Int {
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
            {
                return min(arrUserFollow.count,4) + 1
            }
            else if DeviceType.IS_IPHONE_6
            {
                return min(arrUserFollow.count,4) + 1
            }
            else
            {
                return min(arrUserFollow.count,4) + 1
            }
    }
    
    func collectionView(collectionView: UICollectionView!,
        cellForItemAtIndexPath indexPath: NSIndexPath!) ->
        UICollectionViewCell! {
        
        var MaxItem = 0
            
            if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
            {
                MaxItem = min(arrUserFollow.count,4)
            }
            else if DeviceType.IS_IPHONE_6
            {
                MaxItem = min(arrUserFollow.count,4)
            }
            else
            {
                MaxItem = min(arrUserFollow.count,4)
            }

        
        if indexPath.item != MaxItem
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FollowCollectionViewCell",
            forIndexPath: indexPath) as! FollowCollectionViewCell
            
            // Configure the cell
            var useDict = self.arrUserFollow[indexPath.row] as! NSDictionary

                cell.lblName.text = useDict["name"] as! String!
                var imgStr = useDict["user_image"] as! String!
            cell.imgAvatar.layer.masksToBounds = true
            cell.imgAvatar.layer.cornerRadius  = cell.imgAvatar.frame.size.height / 2
            cell.imgAvatar.sd_setImageWithURL(NSURL(string: imgStr))
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("FollowCollectionViewCell",
                forIndexPath: indexPath) as! FollowCollectionViewCell
            
            // Configure the cell
            let image = UIImage(named: "icon_more")
            cell.imgAvatar.image = image
            cell.lblName.text = "More"
            return cell

            
        }
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        var MaxItem = 0
        
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS
        {
            MaxItem = min(arrUserFollow.count,4)
        }
        else if DeviceType.IS_IPHONE_6
        {
            MaxItem = min(arrUserFollow.count,4)
        }
        else
        {
            MaxItem = min(arrUserFollow.count,4)
        }
        
         if indexPath.item == MaxItem
         {
            let verifyViewController = Utilities.viewController("PeopleFollowingViewController", onStoryboard: "Home")
            
            self.navigationController?.pushViewController(verifyViewController, animated: true)
        }
    }
    
    
    //MARK : Search bar delegate
    // MARK: UISearchBarDelegate
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        NSLog("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        NSLog("The default search bar keyboard search button was tapped: \(searchBar.text).")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        NSLog("The default search bar cancel button was tapped.")
       self.arrUserFollow = self.arrUserFollowBackup.copy() as! NSArray
       self.searchDisplayController?.searchResultsTableView.reloadData()
        searchBar.resignFirstResponder()
    }
    
//    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool // return NO to
//    {
//         searchBar.setShowsCancelButton(true, animated: true)
//        return true
//    }
//    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool
//    {
//         searchBar.setShowsCancelButton(false, animated: true)
//        return true
//    }
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        
        
        return true
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
//        var textSearch =  ""
//        if count(searchText) == 0
//        {
//            
//            textSearch = searchText + " "
//        
//            searchBar.delegate!.searchBar!(searchBar, textDidChange: " ")
//            return
//        }
//        else
//        {
            if count(searchText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())) == 0
            {
                self.arrUserFollow = self.arrUserFollowBackup.copy() as! NSArray
                self.searchDisplayController?.searchResultsTableView.reloadData()
            }
            else
            {
                getSearchResult(searchText)
            }
//        }
    }
    
    ///call get search result
    func getSearchResult(strSearch: String!)
    {
        
            var json = JSonConnect()
            
            let params = ["user_id" : User.sharedInstance.currentUser.user_id,"word": strSearch,
                "page" : "1"]
            

            
            json.postPath("user/searchfollowinglist", parameters: NSMutableDictionary(dictionary: params), completeNetworkBlock: { (result) -> Void in
                self.arrUserFollow = result["user"] as! NSArray
                self.searchDisplayController?.searchResultsTableView.reloadData()
                
               
            }) { (error) -> Void in
                
            }
        
    }
    
    
    // MARK: - Search Methods
    
    func filterContenctsForSearchText(searchText: String, scope: String = "Title")
    {
        
        
        
    }
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String!) -> Bool
    {
        
        self.filterContenctsForSearchText(searchString, scope: "Title")
        
        return true
        
        
    }
    
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchScope searchOption: Int) -> Bool
    {
        
        self.filterContenctsForSearchText(self.searchDisplayController!.searchBar.text, scope: "Title")
        
        return true
        
    }
    func searchDisplayController(controller: UISearchDisplayController, didHideSearchResultsTableView tableView: UITableView)
    {
        self.searchDisplayController!.searchResultsTableView.hidden = false
        if self.searchDisplayController?.active == true
        {
            tableView.hidden = false
        }
        self.showTableReusltView(controller)
    }
    
    func searchDisplayControllerDidBeginSearch(controller: UISearchDisplayController)
    {
        self.showTableReusltView(controller)
    }
    
    func  showTableReusltView(controller: UISearchDisplayController)
    {
        self.searchDisplayController!.searchResultsTableView.hidden = false
        
        for v in (controller.searchResultsTableView.superview as! AnyObject).subviews
        {
            var  v1 =  v as! UIView
            if  v1.alpha < 1.0
            {
                v1.hidden = true
            }
            
        }

    }
    
    


}
extension Array {
    func ref (i:Int) -> T? {
        return 0 <= i && i < count ? self[i] : nil
    }
}
