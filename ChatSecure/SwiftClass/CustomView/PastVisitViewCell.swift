//
//  PastVisitViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 7/6/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class PastVisitViewCell: UITableViewCell {
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var txtHome: MPGTextField_Swift!
    
    var arrHomeBoxId = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
