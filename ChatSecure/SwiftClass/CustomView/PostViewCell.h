//
//  PostViewCell.h
//  ChatSecure
//
//  Created by buisinam on 6/29/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHTCollectionViewWaterfallLayout.h"


@interface PostViewCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate, CHTCollectionViewDelegateWaterfallLayout>

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberLike;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollect;
@property (nonatomic, strong) IBOutlet UIImageView *imgMain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImgMain;
@property (weak, nonatomic) IBOutlet UIImageView *bg_view;




@property (nonatomic
           , strong) IBOutlet UICollectionView *collectImage;
@property (nonatomic, strong) NSMutableArray *arrGallery;
@property (nonatomic, strong) NSMutableArray *cellSizes;
//@property (nonatomic, strong) RAMCollectionViewFlemishBondLayout *collectionViewLayout;
- (void)setup;
- (void)setup:(NSMutableArray *)array;
-(void)reloadCell;

@end
