//
//  FollowCollectionViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 6/29/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class FollowCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
