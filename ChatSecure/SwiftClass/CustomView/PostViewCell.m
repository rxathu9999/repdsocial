//
//  PostViewCell.m
//  ChatSecure
//
//  Created by buisinam on 6/29/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

#import "PostViewCell.h"
#import "RAMCollectionViewCell.h"
#import "RAMCollectionAuxView.h"

#import "CHTCollectionViewWaterfallCell.h"
#import "CHTCollectionViewWaterfallHeader.h"
#import "CHTCollectionViewWaterfallFooter.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "ChatSecure-Swift.h"

#define CELL_IDENTIFIER @"photoCell"
#define HEADER_IDENTIFIER @"WaterfallHeader"
#define FOOTER_IDENTIFIER @"WaterfallFooter"

#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//static NSString *const CellIdentifier = @"photoCell";

@implementation PostViewCell
@synthesize arrGallery,cellSizes = _cellSizes,lblStatus,imgMain,collectImage;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}






///-------------------------------------------------------
#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(arrGallery.count > 0)
    {
        return arrGallery.count;
    }
    else
    {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CHTCollectionViewWaterfallCell *cell =
    (CHTCollectionViewWaterfallCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER
                                                                                forIndexPath:indexPath];
    //    cell.displayString = [NSString stringWithFormat:@"%ld", (long)indexPath.item];
    ImgObj *imgObj = [[ImgObj alloc] init];
    [imgObj setValuesForKeysWithDictionary:[self.arrGallery objectAtIndex:indexPath.item]];
    
    [cell loadImage:[NSURL URLWithString:imgObj.image_original] withSize:[self.cellSizes[indexPath.item] CGSizeValue]];
    
    return cell;
}



#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.cellSizes[indexPath.item] CGSizeValue];
}
#pragma mark - Accessors

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)setup{

        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
        layout.minimumColumnSpacing = 3;
        layout.minimumInteritemSpacing = 3;
    
    
        self.collectImage.dataSource = self;
        self.collectImage.delegate = self;
    
        [self.collectImage setCollectionViewLayout:layout];
    
        [self.collectImage registerClass:[CHTCollectionViewWaterfallCell class]
            forCellWithReuseIdentifier:CELL_IDENTIFIER];
        [self.collectImage registerClass:[CHTCollectionViewWaterfallHeader class]
            forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
                   withReuseIdentifier:HEADER_IDENTIFIER];
        [self.collectImage registerClass:[CHTCollectionViewWaterfallFooter class]
            forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter
                   withReuseIdentifier:FOOTER_IDENTIFIER];
        [self.collectImage setBackgroundColor:[UIColor clearColor]];
    

}
- (void)setup:(NSMutableArray *)array {
    
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    layout.minimumColumnSpacing = 3;
    layout.minimumInteritemSpacing = 3;
    
    if (self.arrGallery == nil)
    {
        self.arrGallery = [[NSMutableArray alloc] initWithArray:array];
    }
    
    self.collectImage.dataSource = self;
    self.collectImage.delegate = self;
    
    [self.collectImage setCollectionViewLayout:layout];
    
    [self.collectImage registerClass:[CHTCollectionViewWaterfallCell class]
          forCellWithReuseIdentifier:CELL_IDENTIFIER];
    [self.collectImage registerClass:[CHTCollectionViewWaterfallHeader class]
          forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
                 withReuseIdentifier:HEADER_IDENTIFIER];
    [self.collectImage registerClass:[CHTCollectionViewWaterfallFooter class]
          forSupplementaryViewOfKind:CHTCollectionElementKindSectionFooter
                 withReuseIdentifier:FOOTER_IDENTIFIER];
    [self.collectImage setBackgroundColor:[UIColor clearColor]];
    
    
}

- (NSMutableArray *)cellSizes {

    _cellSizes = [NSMutableArray array];

    
        for (NSInteger i = 0; i < arrGallery.count ; i++) {
            CGSize size ;
            
            if (IS_OS_8_OR_LATER)
            {
                if (arrGallery.count > 3)
                {
                    size= CGSizeMake(self.collectImage.frame.size.width /2 , self.collectImage.frame.size.height /2);
                }
                else
                {
                    if(i%3 == 0)
                    {
                        size= CGSizeMake(self.collectImage.frame.size.width /2 , self.heightCollect.constant);
                    }
                    else
                    {
                        if (arrGallery.count != 2)
                        {
                            size= CGSizeMake(self.collectImage.frame.size.width /2 , self.heightCollect.constant / 2 - 1);
                        }
                        else
                        {
                             size= CGSizeMake(self.collectImage.frame.size.width /2 , self.heightCollect.constant);
                        }
                    }
                }
                
                _cellSizes[i] = [NSValue valueWithCGSize:size];
            }
            else
            {
                if (arrGallery.count > 3)
                {
                    size= CGSizeMake(self.collectImage.frame.size.width /2 , self.collectImage.height /2);
                }
                else
                {
                    if(i%3 == 0)
                    {
                        size= CGSizeMake(self.collectImage.frame.size.width /2 , self.collectImage.height);
                    }
                    else
                    {
                        if (arrGallery.count != 2)
                        {
                            size= CGSizeMake(self.collectImage.frame.size.width /2 , self.collectImage.height / 2 - 1);
                        }
                        else
                        {
                            size= CGSizeMake(self.collectImage.frame.size.width /2 , self.collectImage.height);
                        }
                    }
                }
                
                _cellSizes[i] = [NSValue valueWithCGSize:size];
            }
        }
    
    return _cellSizes;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

-(void)reloadCell
{
    CGSize size = [lblStatus.text sizeWithFont:lblStatus.font constrainedToSize:CGSizeMake(lblStatus.width, CGFLOAT_MAX)];
    lblStatus.height = size.height+lblStatus.font.pointSize;
    
    
    int numberLine = lblStatus.height / [lblStatus.font pointSize];
    lblStatus.numberOfLines=numberLine;
    imgMain.top = lblStatus.bottom + 5;
    collectImage.top = lblStatus.bottom + 5;
    [collectImage reloadData];

}

@end
