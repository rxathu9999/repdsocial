//
//  PostViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 6/29/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class PostViewCell: UITableViewCell,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var collectImage: UICollectionView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var lblNumberLike: UILabel!

    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      
        if !collectImage.collectionViewLayout.isKindOfClass(FBLikeLayout)
        {
            var layout = FBLikeLayout()
            layout.minimumInteritemSpacing = 4
            layout.singleCellWidth = (min(self.collectImage.bounds.size.width, self.collectImage.bounds.size.height) - self.collectImage.contentInset.left - self.collectImage.contentInset.right - 8) / 3.0
            layout.maxCellSpace = 3
            layout.forceCellWidthForMinimumInteritemSpacing = false
            layout.fullImagePercentageOfOccurrency = 25
            
            self.collectImage.reloadData()
        }

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
   
//    
//    override func layoutSubviews() {
//        if !collectImage.collectionViewLayout.isKindOfClass(FBLikeLayout)
//        {
//            var layout = FBLikeLayout()
//            layout.minimumInteritemSpacing = 4
//            layout.singleCellWidth = (min(self.collectImage.bounds.size.width, self.collectImage.bounds.size.height) - self.collectImage.contentInset.left - self.collectImage.contentInset.right - 8) / 3.0
//            layout.maxCellSpace = 3
//            layout.forceCellWidthForMinimumInteritemSpacing = true
//            layout.fullImagePercentageOfOccurrency = 25
//            
//            self.collectImage.reloadData()
//        }
//    }
    
    //MARK : Collection delegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 13
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView
    {
        var view : UICollectionReusableView = collectionView .dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "identifier", forIndexPath: indexPath) as! UICollectionReusableView
        if view.isKindOfClass(NSNull)
        {
            view = UICollectionReusableView()
        }
        view.backgroundColor = UIColor.whiteColor()
        return view
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! ImageCollectionViewCell
        cell.backgroundColor = UIColor.whiteColor()
        cell.photoImageView.sd_setImageWithURL(NSURL(string: "http://xmedia.nguoiduatin.vn/100/2013/11/6/Rola%20Takizawa%20A2.jpg"), placeholderImage: UIImage.new())
        
        return cell
    }
    
    //MARK : CollectionView Flow
   
    
     func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
     {
        return CGSizeMake(200, 100)
     }
    
    
}
