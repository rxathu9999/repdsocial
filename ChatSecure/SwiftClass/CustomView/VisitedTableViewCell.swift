//
//  VisitedTableViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 7/15/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class VisitedTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
