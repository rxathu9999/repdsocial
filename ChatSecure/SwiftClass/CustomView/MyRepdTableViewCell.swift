//
//  MyRepdTableViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 7/15/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class MyRepdTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
