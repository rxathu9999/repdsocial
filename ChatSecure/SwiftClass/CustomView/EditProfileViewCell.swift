//
//  EditProfileViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 7/17/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class EditProfileViewCell: UITableViewCell {

    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtDescription: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
