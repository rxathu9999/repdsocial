//
//  CommentViewCell.swift
//  ChatSecure
//
//  Created by buisinam on 7/6/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class CommentViewCell: UITableViewCell {
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
