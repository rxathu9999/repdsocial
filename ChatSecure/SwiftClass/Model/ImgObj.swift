//
//  ImgObj.swift
//  ChatSecure
//
//  Created by buisinam on 6/30/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class ImgObj: NSObject {
    var id : Int = 0
    var image : String = ""
    var image_original : String = ""
    var image_likes : Int = 0
    var is_liked : String = ""

}
