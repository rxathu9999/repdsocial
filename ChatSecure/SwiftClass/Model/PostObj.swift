//
//  OTR.swift
//  ChatSecure
//
//  Created by buisinam on 6/30/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class PostObj: NSObject {

    var post_id: Int = 0
    var box_id: Int = 0
    var owner_id: Int = 0
    var owner: String = ""
    var owner_image: String = ""
    var post_likes: Int = 0
    var caption: String = ""
    var date: String = ""
    var inserted_date: Int = 0
    var tagged_users : [[NSObject : AnyObject]] = []
    var images : [[NSObject : AnyObject]] = []
    var can_delete: String = ""
    var can_edit: String = ""
    var is_liked: String = "n"
}
