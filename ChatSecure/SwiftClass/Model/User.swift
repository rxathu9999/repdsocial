//
//  User.swift
//  LiteShop
//
//  Created by Thuc Truong on 6/22/15.
//  Copyright (c) 2015 MonaMedia. All rights reserved.
//

import UIKit



class User: NSObject {
    var currentUser : User!
    var user_id: Int = 0
    var email_id: String = ""
    var profile_completed: String = ""
    var mobile: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var token: String = ""
    var home_box_id = 0
    var is_normal : Bool = false
    
    class var sharedInstance: User {
        
        struct Static {
            static let instance: User = User()

        }
        return Static.instance
    }
    
    override init() {
        super.init()

    }
    
    func saveToDefault(user: User) {
        currentUser = user
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.setValue(self.userInfor(user), forKey: "userInfo")
        userDefault.synchronize()

    }
    
    //save
    private func userInfor(user: User)  -> [String: AnyObject] {
        var dicInfo = [String: AnyObject]()
        
        dicInfo["user_id"] = user.user_id
        dicInfo["email_id"] = user.email_id
        dicInfo["mobile"] = user.mobile
        dicInfo["first_name"] = user.first_name
        dicInfo["last_name"] = user.last_name
        dicInfo["home_box_id"] = user.home_box_id
        dicInfo["profile_completed"] = user.profile_completed
        dicInfo["token"] = user.token
        dicInfo["is_normal"] = user.is_normal
        
        return dicInfo
    }
     func loadCurrentUser() {
        
        if  currentUser == nil
        {
            currentUser = User()
        }
       
            let userDefault = NSUserDefaults.standardUserDefaults()
            if let dicInfo = userDefault.objectForKey("userInfo") as? [String: AnyObject]
            {
                currentUser.user_id = dicInfo["user_id"] as! Int
                currentUser.email_id = dicInfo["email_id"] as! String
                currentUser.mobile = dicInfo["mobile"] as! String
                currentUser.first_name = dicInfo["first_name"] as! String
                currentUser.last_name = dicInfo["last_name"] as! String
                currentUser.home_box_id = dicInfo["home_box_id"] as! Int
                currentUser.profile_completed = dicInfo["profile_completed"] as! String
                currentUser.mobile = dicInfo["token"] as! String
                currentUser.is_normal = dicInfo["is_normal"] as! Bool
            }

        
    }
     func logOut(){
        currentUser = nil
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.removeObjectForKey("userInfo")
        userDefault.synchronize()
    }
    
}
