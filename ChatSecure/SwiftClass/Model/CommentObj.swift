//
//  CommentObj.swift
//  ChatSecure
//
//  Created by buisinam on 7/6/15.
//  Copyright (c) 2015 Chris Ballinger. All rights reserved.
//

import UIKit

class CommentObj: NSObject {
    var comment_id: Int = 0
    var owner_id: Int = 0
    var owner_image: String = ""
    var owner: String = ""
    var inserted_date: Int = 0
    var tagged_users : [[NSObject : AnyObject]] = []
}
